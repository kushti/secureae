import com.typesafe.sbt.SbtStartScript

lazy val commonSettings = Seq(
  organization := "com.smartcontract",
  scalaVersion := "2.11.7"
)

def module(id: String): Project = Project(id = id, base = new File(s"$id"))

lazy val commons = module("commons").settings(commonSettings: _*)

lazy val sae = module("sae")
  .aggregate(commons)
  .dependsOn(commons)
  .settings(commonSettings: _*)

lazy val smartcontract = module("smartcontract")
  .aggregate(commons)
  .dependsOn(commons)
  .settings(commonSettings: _*)


lazy val root = Project(id = "scorex", base = file("."))
  .dependsOn(sae, smartcontract)
  .settings(commonSettings: _*)

Seq(SbtStartScript.startScriptForClassesSettings: _*)