Nxt 1.7.0
-----------

New feature since 1.5.x

 * 1.7:

 Coin Shuffling, Account Control for phased transactions,
  immediate release of phased transactions on approval, forging algo update,
  account properties, singleton assets, unique resource allocation transactions throttling,
  prunable plain and prunable encrypted message attachments both allowed in the
   same transaction,

   !sending assets to Genesis isn't allowed anymore, "isText" field
   in message API is renamed to "messageIsText" / "encryptedMessageIsText".

   ! maximum non-prunable message size is 160 bytes

   ! 1 Nxt fee per 32 bytes


 * 1.6:

 Account ledger, asset deletion



Nxt & Secureae.com
------------------

Backend API calls(via Java API currently):

 * GetAllAssets

 * GetAsset (could be avoided)


Listeners:

 * Listener for new blocks to extract clauses / dividends(probably not needed anymore)

 * Listener for new trades



**With Java API usage, SecureAE will work in the same way, but please note,
as time goes by functionality is going backward. E.g. SecureAE
will not have singleton assets / assets deletion support after 1.7 release**


