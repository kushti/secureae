import sbt._

object Dependencies {
  lazy val akka = Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.13",
    "com.typesafe.akka" %% "akka-testkit" % "2.3.13" % "test"
  )

  lazy val spray = Seq(
    "io.spray" %% "spray-routing" % "1.3.3",
    "io.spray" %% "spray-caching" % "1.3.3",
    "io.spray" %% "spray-can" % "1.3.3",
    "io.spray" %% "spray-http" % "1.3.3",
    "io.spray" %% "spray-httpx" % "1.3.3",
    "io.spray" %% "spray-util" % "1.3.3",
    "io.spray" %% "spray-client" % "1.3.3"
  )

  lazy val crypto = Seq(
    "org.bouncycastle" % "bcpkix-jdk15on" % "1.52"
  )

  lazy val db = Seq(
    "org.mapdb" % "mapdb" % "1.0.8",
    "com.typesafe.slick" %% "slick" % "3.0.3",
    "org.postgresql" % "postgresql" % "9.3-1103-jdbc41",
    "com.zaxxer" % "HikariCP" % "2.3.9",
    "com.github.tototoshi" %% "slick-joda-mapper" % "2.0.0"
  )

  lazy val json = Seq(
    "com.typesafe.play" %% "play-json" % "2.3.10"
  )
}