package test.functional

import org.joda.time.DateTime
import org.scalatest.FunSuite
import smartcontract.model.{QueryContext, GoogleRankQuery}
import scala.concurrent.Await
import scala.concurrent.duration._

class GoogleRankQuerySpecification extends FunSuite with TestingCommons {

  test("simple lookup - google.com for google", org.scalatest.tagobjects.Network) {
    val qc = QueryContext("2266", new DateTime().getMillis, "googleterm1")
    val f = GoogleRankQuery("US", "google", "google.com", qc).query()
    val t = Await.result(f, 2.minutes)
    assert(t.isSuccess)
    assert(t.get.value < 5)
  }

  test("simple lookup - unknown.com for google", org.scalatest.tagobjects.Network) {
    val qc = QueryContext("2277", new DateTime().getMillis, "googleterm2")
    val f = GoogleRankQuery("US", "google", "unknown.com", qc).query()
    val t = Await.result(f, 2.minutes)
    assert(t.isSuccess)
    assert(t.get.value > 100)
  }
}