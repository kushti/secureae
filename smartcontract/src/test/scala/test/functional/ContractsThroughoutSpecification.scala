package test.functional

import org.scalatest.{Matchers, FunSuite}
import play.api.libs.json.{JsObject, Json}
import smartcontract.api.http.SmartContractHttpRoute
import spray.testkit.ScalatestRouteTest
import spray.http.StatusCodes._
import test.NxtTest
import scala.util.Random


class ContractsThroughoutSpecification extends FunSuite
  with Matchers
  with ScalatestRouteTest
  with SmartContractHttpRoute
  with TestingCommons {

  private def testContract(js:JsObject, id:String) = {
    val testingParams = Json.obj(
      "term" -> "1",
      "success" -> true,
      "publish-chain" -> false,
      "publish-frontend" -> false
    )

    val toPost = (testingParams ++ js).toString()

    Post("/smartcontracts/utils/checkContractExecution", toPost) ~> smartContractRoute ~> check {
      assert(status === OK)
      val respBody = responseAs[String]

      val respJson = Json.parse(respBody) \ "status_update"

      assert((respJson \ "contract").as[String] === id)
      assert((respJson \ "term").as[String] === "1")
    }
  }

  ignore("bitcoin contract execution", NxtTest){
    val id = Random.nextLong().toString
    val sk = Random.nextLong().toString

    val js = Json.parse(simpleBitcoinContractText(id, sk)).as[JsObject]

    testContract(js, id)
  }

  ignore("seo contract execution", NxtTest){
    val id = Random.nextLong().toString
    val sk = Random.nextLong().toString

    val js = Json.parse(simpleSeoContractText(id, sk)).as[JsObject]

    testContract(js, id)
  }

}