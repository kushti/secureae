package test.functional

import org.scalatest.FunSuite
import smartcontract.api.btcusdprice.BtcUsdTickerClientCascade

import scala.concurrent.Await
import scala.concurrent.duration._


class BtcUsdTickerClientCascadeSpecification extends FunSuite with TestingCommons {

  test("simple rate getting with cascade",  org.scalatest.tagobjects.Network) {
    val priceTryFuture = BtcUsdTickerClientCascade.fetchLastPrice()
    val priceTry = Await.result(priceTryFuture, 1.minute)
    assert(priceTry.isSuccess)
    assert(priceTry.get > 1)
  }
}