package test.functional

import org.joda.time.DateTime
import org.scalatest.FunSuite
import smartcontract.scrapers.CriisComScraper

import scala.concurrent.Await
import scala.concurrent.duration._

class CriisComScraperSpecification extends FunSuite with TestingCommons {

  test("scrapeDeeds - simple", org.scalatest.tagobjects.Network) {
    val ft = CriisComScraper.scrapeDeeds("0766", "001", new DateTime().minusYears(50).getMillis)
    val res = Await.result(ft, 1.minute)
    assert(res.isSuccess)
    assert(res.get._1.nonEmpty)
    assert(res.get._2.nonEmpty)
  }

  test("scrapeDeeds - timefilter", org.scalatest.tagobjects.Network) {
    val ft0 = CriisComScraper.scrapeDeeds("0766", "001", new DateTime().getMillis)
    Thread.sleep(4000)
    val ft = CriisComScraper.scrapeDeeds("0766", "001", new DateTime().minusYears(30).getMillis)
    val res0 = Await.result(ft0, 1.minute)
    val res = Await.result(ft, 1.minute)

    assert(res0.isSuccess)
    assert(res.isSuccess)
    assert(res0.get._1.size < res.get._1.size)
    assert(res0.get._2.size < res.get._1.size)
  }
}