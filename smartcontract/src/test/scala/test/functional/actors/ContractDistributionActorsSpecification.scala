package test.functional.actors

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.testkit.{TestProbe, TestActorRef}
import akka.util.ByteString
import nxt.NxtFunctions
import org.scalatest.{FunSuite, Matchers}
import smartcontract.actors.IncomingContractHandler
import smartcontract.actors.p2p.message.{MessageProducer, Message}
import smartcontract.actors.p2p.{ContractDistributionEventType, PeerData, PeerDatabase}
import smartcontract.db.implementation.postgresql.{ContractDistributionEventStorageTest, ContractStorageTest}
import smartcontract.model.SmartContractTrackingState
import test.functional.{NxtTesting, TestingCommons}
import test.{DatabaseTest, NxtTest}
import scala.concurrent.duration._


class ContractDistributionActorsSpecification extends FunSuite
  with TestingCommons with NxtTesting with Matchers {
  private lazy val evStorage = ContractDistributionEventStorageTest
  private lazy val cStorage = ContractStorageTest

  private implicit lazy val testActorSystem = ActorSystem("test-system")

  //Slave actor under test
  private val slave = TestActorRef(new Actor with IncomingContractHandler {
    override def receive = slaveReceive
  })


  private def generatePeerDb(actor: ActorRef): PeerDatabase = {
    val peerData = PeerData(None, new InetSocketAddress(9999), actor)

    new PeerDatabase {
      override def allConnectedPeers(): Seq[PeerData] = Seq(peerData)

      override def addConnectedPeer(pubKey: Option[Array[Byte]], peer: InetSocketAddress, handler: ActorRef): Unit = ???

      override def actorForPubKey(pubKey: Array[Byte]): Option[ActorRef] =
        Some(peerData.handler)

      override def removeConnectedPeer(pubKey: Array[Byte]): Unit = ???

      override def removeConnectedPeer(peer: InetSocketAddress): Unit = ???

      override def addKnownPeer(pubKey: Array[Byte], peer: InetSocketAddress): Unit = ???

      override def knownPeers(): Map[Array[Byte], InetSocketAddress] = ???
    }
  }


  //Master actor under test
  private def generateMasterRef(slaveRefOpt: Option[ActorRef]) = {

    TestActorRef(new Actor with IncomingContractHandler {
      override val AckWaitingTime = 2.seconds

      val masterRef = self

      val slaveRef = slaveRefOpt match {
        case Some(sr) => sr
        case None => TestActorRef(new Actor with IncomingContractHandler {
          override def receive = slaveReceive orElse{
            case bs:ByteString => self ! Message.parse(bs.toArray).get.concrete().get
          }

          override lazy val peerDb = generatePeerDb(masterRef)
        })
      }

      override def receive = ({
        case ct: String => handleIncomingContract(ct, master = true)
      }: Receive) orElse masterReceive

      override lazy val peerDb = generatePeerDb(slaveRef)
    })
  }

  test("slave writes correct messages into the database", org.scalatest.tagobjects.Slow, DatabaseTest) {
    evStorage.clear()
    cStorage.clear()

    //wrong contract - no events to dump
    slave ! MessageProducer.contract("tt")
    Thread.sleep(2.seconds.toMillis)
    evStorage.size() should be(0)
    cStorage.size() should be(0)

    //valid contract - SlaveGotAContract msg
    val id = "testid"
    val sk = "signingkey"

    val contractText = simpleBitcoinContractText(id, sk)
    slave ! MessageProducer.contract(contractText)
    Thread.sleep(2.seconds.toMillis)
    evStorage.size() should be(1)
    cStorage.size() should be(1)
    cStorage.activeSize() should be(0)
    cStorage.getContract(id).get.trackingState should be(SmartContractTrackingState.PENDING)

    val evGot = ContractDistributionEventType.SlaveGotAContract.id.toByte
    evStorage.eventsByContractAndEventType(id, evGot).size should be(1)

    //wrong abort message
    slave ! MessageProducer.contractAbort(s"$id-wrong")
    Thread.sleep(2.seconds.toMillis)
    cStorage.size() should be(1)
    cStorage.activeSize() should be(0)
    cStorage.getContract(id).get.trackingState should be(SmartContractTrackingState.PENDING)
    evStorage.size() should be(1)

    //valid abort message
    slave ! MessageProducer.contractAbort(id)
    Thread.sleep(2.seconds.toMillis)

    cStorage.size() should be(2)
    cStorage.activeSize() should be(0)
    cStorage.getContract(id).get.trackingState should be(SmartContractTrackingState.ABORTED)

    evStorage.size() should be(2)
    val evAbt = ContractDistributionEventType.SlaveAbort.id.toByte
    evStorage.eventsByContractAndEventType(id, evAbt).size should be(1)
  }

  test("master got no ack", org.scalatest.tagobjects.Slow, DatabaseTest) {
    evStorage.clear()
    cStorage.clear()

    val id = "testid"
    val sk = "signingkey"

    val contractText = simpleBitcoinContractText(id, sk)

    val slaveProbe = TestProbe()
    val master = generateMasterRef(Some(slaveProbe.ref))
    master ! contractText

    slaveProbe.expectMsgAllOf(5.seconds)
    (ByteString(MessageProducer.contract(contractText).bytes)
      , ByteString(MessageProducer.contractAbort(id).bytes))

    Thread.sleep(5.seconds.toMillis)

    evStorage.size() should be(2)
    cStorage.size() should be(2)
    cStorage.getContract(id).get.trackingState should be(SmartContractTrackingState.ABORTED)
  }

  test("master got an ack", DatabaseTest, NxtTest) {
    evStorage.clear()
    cStorage.clear()

    val id = "testid"
    val sk = "signingkey"

    val contractText = simpleBitcoinContractText(id, sk)

    val master = generateMasterRef(None)

    master ! contractText
    Thread.sleep(5000)

    NxtFunctions.generateBlocks(forgerSecretPhrase, 5)

    //as no listener started
    evStorage.size() should be(4)
    cStorage.size() should be(3)
    cStorage.getContract(id).get.trackingState should be(SmartContractTrackingState.TRACKING)
  }
}
