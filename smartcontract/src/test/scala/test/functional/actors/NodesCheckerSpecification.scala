package test.functional.actors

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestProbe}
import org.scalatest.FunSuite
import smartcontract.actors.monitoring.{CheckNodesForList, NodesChecker}
import test.functional.TestingCommons

import scala.concurrent.duration._

class NodesCheckerSpecification extends FunSuite with TestingCommons {
  implicit lazy val testActorSystem = ActorSystem("test-system")

  test("failed node") {
    val monitoringProbe = TestProbe()
    val actorRef = TestActorRef(new NodesChecker(monitoringProbe.ref))
    actorRef ! CheckNodesForList(Set("http://google.com"))
    monitoringProbe.expectMsgType[smartcontract.actors.monitoring.InternalError](30.seconds)
  }
}
