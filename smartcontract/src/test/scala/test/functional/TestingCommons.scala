package test.functional

import org.apache.commons.lang3.RandomStringUtils
import smartcontract.model.ContractParameters

import scala.concurrent.duration._
import scala.util.{Random, Try}

trait TestingCommons {
  implicit val atMost = 5.minutes

  protected def realBitcoinContract(address: String, satoshisValue: Long): Try[ContractParameters] = {
    val id = RandomStringUtils.randomAlphanumeric(20)
    val description = RandomStringUtils.randomAlphabetic(Random.nextInt(1998) + 1)
    val name = RandomStringUtils.randomAlphabetic(Random.nextInt(98) + 1)
    val st = System.currentTimeMillis()
    val deadline = System.currentTimeMillis() + 1000 * 86400 * 7 //one week from now

    val value = 1

    val text =
      s"""
         {"contract": {"description": "$description","signature_deadline": "$deadline","escrows": [],"id": "$id","name": "$name","signers": [{"signature_key": "99a51e4c39d3f5d3","nxt_id": "NXT-D632-HRRY-HBP5-9QSTE"}],"terms": [{"success": {"actions": [{"type": "payment","to": [{"signature_key": "99a51e4c39d3f5d3","nxt_id": "NXT-D632-HRRY-HBP5-9QSTE","address": "1FBSEn1PaEmPackE31xxXx8wtasx5be2NW"}]}]},"failure": {"actions": [{"type": "payment","to": [{"signature_key": "99a51e4c39d3f5d3","nxt_id": "NXT-D632-HRRY-HBP5-9QSTE","address": "1FBSEn1PaEmPackE31xxXx8wtasx5be2NW"}]}]},"name": "1","type": "payment","expected": {"amount": "$value","currency": "BTC","deadline": "$deadline","to": {"name": "Steve Ellis","address": "$address","destination": "NXT-D632-HRRY-HBP5-9QSTE","signature_key": "99a51e4c39d3f5d3","nxt_id": "NXT-D632-HRRY-HBP5-9QSTE"},"type": "payment","amount-currency": "USD"}}],"type": "payment","attachments": []},"outcomes": {},"privacy": "public","source": "smartcontract.com","start-time": "$st"}
        """.trim
    ContractParameters.parse(text)
  }

  protected def simpleBitcoinContractText(id: String,
                                   sk: String): String = {
    val startTime = System.currentTimeMillis() / 1000

    s"""
      {"start-time":$startTime,
       "outcomes" : {},
      "privacy" : "public",
      "contract":{"description": "auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract. auto-test contract.","signature_deadline": "1423069620","escrows": [],"id": "$id","name": "chain.com test","signers": [{"signature_key": "$sk","nxt_id": "NXT-AAJE-3Q9R-Q3U9-5ZPJJ"}],"terms": [{"success": {"actions": []},"failure": {"actions": []},"name": "1","type": "payment","expected": {"amount": "400","currency": "BTC","deadline": "1424279820","to": {"address": "1GThJNvdqZsQT2iozhhiYwi9HTnTWGg9WE"},"type": "payment","amount-currency": "USD"}}],"type": "payment","source": "smartcontract.com","attachments": []}}
     """.trim
  }

  protected def simpleSeoContractText(id: String, sk: String): String =
    s"""{"contract":{"description":"testing SEO","signature_deadline":"1431490956","escrows":[{"deposit_address":"1KiSWBum8uQANb7tWdvRfm78yhraUCoQJu","amount":"93300","currency":"BTC","name":"1"}],"id":"$id","name":"testing SEO","signers":[{"signature_key":"$sk","nxt_id":"NXT-QMY7-K73R-88EJ-2SN6V"}],"terms":[{"success":{"actions":[{"type":"payment","to":[{"signature_key":"b97b6515039f5520","nxt_id":"NXT-QMY7-K73R-88EJ-2SN6V","address":null}]}]},"failure":{"actions":[{"type":"payment","to":[{"signature_key":"b97b6515039f5520","nxt_id":"NXT-QMY7-K73R-88EJ-2SN6V","address":null}]}]},"name":"1","type":"seo","expected":{"min":"1","max":"10","provider":"semrush.com","deadline":"1432009320","result":"cheese.com","query":{"phrase":"cheese","source":"google","locale":"us"},"type":"seo"},"escrow":"1"}],"type":"seo","attachments":[{"url":"https://smartcontract.s3.amazonaws.com/uploads/991a97825b98618b5f69afb9ec4ca7a9/Screen%20Shot%202015-05-11%20at%204.26.30%20PM.png","sha":"cd98c78f12afcf97a158276800a87ef721a6eb6db828ee1a728315bfa2f02c87"}]},"source":"smartcontract.com","start-time":"1431407895"}"""
}



trait NxtTesting {
  val forgerSecretPhrase = "aSykrgKGZNlSVOMDxkZZgbTvQqJPGtsBggb"
}