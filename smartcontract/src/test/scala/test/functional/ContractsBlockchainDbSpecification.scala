package test.functional

import nxt.{NxtFunctions, TransactionQueryBuilder}
import org.scalatest.FunSuite
import smartcontract.model.ContractParameters
import smartcontract.model.blockchain.ContractsBlockchainDb
import smartcontract.utils.EncodingUtils
import test.NxtTest

import scala.util.{Failure, Random, Success}


class ContractsBlockchainDbSpecification extends FunSuite
  with TestingCommons with NxtTesting {
  private val blocksToInclude = 8

  test("ContractsBlockchainDb.publishText & ContractsBlockchainDb.fetchTexts",
    org.scalatest.tagobjects.Slow, NxtTest) {
    val id = Random.nextLong().toString
    val sk = Random.nextLong().toString

    val contractText = simpleBitcoinContractText(id, sk)
    val idTry = ContractsBlockchainDb.publishText(contractText)

    idTry.recover { case t: Throwable =>
      t.printStackTrace()
      fail("No success with publishing contract")
    }

    NxtFunctions.generateBlocks(forgerSecretPhrase, blocksToInclude)
    val st = new TransactionQueryBuilder().withId(idTry.get).query()
    assert(st.isSuccess && st.get.nonEmpty)

    val since = NxtFunctions.currentHeight - 500
    ContractsBlockchainDb.fetchTexts(since) match {
      case Success(texts) =>
        println("texts: " + texts.mkString("\n\n"))
        assert(texts.exists { text =>
          text.contains(id) && text.contains(sk)
        })
      case Failure(e) =>
        e.printStackTrace()
        throw e
    }
  }

  test("private contract publishing",
    org.scalatest.tagobjects.Slow,
    NxtTest) {

    val id = Random.nextLong().toString
    val sk = Random.nextLong().toString

    val passBytes = Array.fill(64)(Random.nextInt(256).toByte)
    val pass64 = EncodingUtils.bytes2base64(passBytes)

    val contractText = simpleBitcoinContractText(id, sk).replace("public\"", s"""private", "password":"$pass64" """.trim)

    val params = ContractParameters.parse(contractText).get

    val idTry = ContractsBlockchainDb.publishContractText(id, params)

    NxtFunctions.generateBlocks(forgerSecretPhrase, blocksToInclude)
    val st = new TransactionQueryBuilder().withId(idTry.get).query()
    assert(st.isSuccess && st.get.nonEmpty)

    val blockchainMsgsTry = ContractsBlockchainDb.fetchTexts(NxtFunctions.currentHeight - 1000)
    assert(blockchainMsgsTry.isSuccess)
    val msgs = blockchainMsgsTry.get
    assert(msgs.exists(_.contains(params.hashHex)))
  }

  ignore("ContractsBlockchainDb.publishText & ContractsBlockchainDb.fetchContracts",
    org.scalatest.tagobjects.Slow,
    NxtTest) {

    val id = Random.nextLong().toString
    val sk = Random.nextLong().toString

    val contractText = simpleBitcoinContractText(id, sk)
    val idTry = ContractsBlockchainDb.publishText(contractText)
    NxtFunctions.generateBlocks(forgerSecretPhrase, blocksToInclude)
    val st = new TransactionQueryBuilder().withId(idTry.get).query()
    assert(st.isSuccess && st.get.nonEmpty)

    val contractsMap = ContractsBlockchainDb.fetchContracts()
    assert(contractsMap.get(id).isDefined)
  }
}