package test.functional

import org.joda.time.DateTime
import org.scalatest.FunSuite
import smartcontract.model.{QueryContext, CalifornianRealEstateQuery}
import scala.concurrent.Await
import scala.concurrent.duration._

class CalifornianRealEstateQuerySpecification extends FunSuite with TestingCommons {

  test("0766-001",  org.scalatest.tagobjects.Network) {
    val qc1 = QueryContext("2255", new DateTime().minusYears(30).getMillis, "tt1")
    val f1 = CalifornianRealEstateQuery("0766", "001", qc1).query()
    val t1 = Await.result(f1, 30.seconds)
    assert(t1.isSuccess)
    println(t1.get)
    assert(t1.get.value._1.nonEmpty)
    assert(t1.get.value._2.nonEmpty)

    val qc2 = QueryContext("2255", new DateTime().getMillis, "tt2")
    val f2 = CalifornianRealEstateQuery("0766", "001", qc2).query()
    val t2 = Await.result(f2, 30.seconds)
    assert(t2.isSuccess)
    assert(t2.get.value._1.isEmpty)
    assert(t2.get.value._2.isEmpty)
  }
}