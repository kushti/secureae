package test.functional

import akka.actor.{Actor, ActorSystem}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FunSuite, Matchers}
import play.api.libs.json.Json
import smartcontract.api.http.SmartContractHttpRoute
import smartcontract.AppRegistry.StoragesImpl
import spray.http.StatusCodes._
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest
import test.NxtTest
import scala.util.Random


class SmartContractHttpRouteSpecification extends FunSuite
  with Matchers
  with ScalatestRouteTest
  with SmartContractHttpRoute
  with TestingCommons
  with MockFactory {

  def actorRefFactory = system

  private def jsonContract() = {
    val id = Random.nextLong().toString
    val sk = Random.nextLong().toString
    simpleBitcoinContractText(id, sk)
  }

  test("startcontract valid 1", NxtTest) {
    Post("/smartcontracts/startcontract", jsonContract()) ~> smartContractRoute ~> check {
      status should be (OK)
      val respBody = responseAs[String]
      assert(respBody.contains("received"))
      val respJs = Json.parse(respBody)
      assert((respJs \ "status").as[String] === "received")
      val id = (respJs \ "details").as[String]

      val txTable = StoragesImpl.transactions

      /*

      todo: produce transaction signable by key

      assert(txTable.getTransaction(ActionId(GlobalTermId(id, "1"), success = true)).isDefined)
      assert(txTable.getTransaction(ActionId(GlobalTermId(id, "1"), success = false)).isDefined)

      assert(txTable.getTransaction(ActionId(GlobalTermId(id, "2"), success = true)).isEmpty)
      assert(txTable.getTransaction(ActionId(GlobalTermId(id, "2"), success = false)).isEmpty)*/
    }
  }

  test("decryptFrom invalid 1") {
    Post("/smartcontracts/utils/decryptFrom", "") ~> smartContractRoute ~> check(assert(status !== OK))
  }

  test("decryptFrom invalid 2") {
    val body = Json.obj("publicKey" -> "", "data" -> "", "nonce" -> "").toString()
    Post("/smartcontracts/utils/decryptFrom", body) ~> smartContractRoute ~> check(assert(status !== OK))
  }

  test("decryptFrom valid 1") {
    val body = Json.obj(
      "publicKey" -> "8ae07010aff62658a227c520a9e1ec026fd2b90d4c591364f871fa5cdb9aca0f",
      "nonce" -> "f8e827f8585dbf89c53d29f215da2bb6f715b2fbc7a8a1a36508f6b8d98dbe55",
      "data" -> "c8a747c5dc2d278b9fb2f429797fe6a8ae1c9b96a9db003cad8bbbf8eeedcf48e3410406bde4b7649acb945fa0e77476cecbe519ffe75a0173bef4eadb5826dd",
      "secretPhrase" -> "please note",
      "uncompressDecryptedMessage" -> "true"
    ).toString()
    Post("/smartcontracts/utils/decryptFrom", body) ~> smartContractRoute ~> check {
      status should be (OK)
      (Json.parse(responseAs[String]) \ "decryptedMessage").as[String] should be ("test message")
    }
  }

  test("decryptFrom valid 2") {
    val body = Json.obj(
      "publicKey" -> "200270cd97db004cb397843f632de0f6b8388a560f0ccc9fb55296b5a7900c60",
      "nonce" -> "b74f9762b43dbe07867fa7a6646ad32107491b393f4fa4fa496946d35524b2e1",
      "data" -> "8c88d4d61b93ee28df22fa98a3381d1cbecd3aa2ebe0f4ef96addb01860b37d2f63605670375c1f743a879c64daf29d8806f83456fd0ab9a722d9e8f3df40b3bb0fad2062d5f451e93f6deb52eb73a5dd5e7fcb001ac973c1a36eec33629615c",
      "secretPhrase" -> "SAE messagingsane girlfriend tap heart however grew muscle society practice aunt poison flood",
      "uncompressDecryptedMessage" -> "true"
    ).toString()
    Post("/smartcontracts/utils/decryptFrom", body) ~> smartContractRoute ~> check {
      status should be (OK)
      (Json.parse(responseAs[String]) \ "decryptedMessage").as[String] should be ("{\"contract\":\"3af4088cfce1c9a2c546742e4396ce89\",\"term\":1}")
    }
  }

  test("smartcontracts/number/total") {
    Get("/smartcontracts/number/total") ~> smartContractRoute ~> check {
      status should be (OK)
      assert((Json.parse(responseAs[String]) \ "contracts-total").as[Int] >= 0)
    }
  }

  test("smartcontracts/number/active") {
    Get("/smartcontracts/number/active") ~> smartContractRoute ~> check {
      status should be (OK)
      assert((Json.parse(responseAs[String]) \ "contracts-active").as[Int] >= 0)
    }
  }
}