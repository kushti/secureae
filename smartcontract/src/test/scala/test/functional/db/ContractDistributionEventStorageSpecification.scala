package test.functional.db

import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}
import smartcontract.actors.p2p.{ContractDistributionEvent, ContractDistributionEventType}
import smartcontract.db.implementation.postgresql.ContractDistributionEventStorageTest
import test.DatabaseTest

class ContractDistributionEventStorageSpecification extends FunSuite with BeforeAndAfter with Matchers {
  private lazy val storage = ContractDistributionEventStorageTest

  test("roundtrip w. eventsByContract", DatabaseTest) {
    storage.clear()
    val eventType = ContractDistributionEventType.MasterGotAContract
    val contractId = "ididid3"
    val ownPK = Array.fill(32)(55: Byte)

    storage.addEvent(ContractDistributionEvent(contractId,
      ownPK,
      eventType,
      System.currentTimeMillis()
    ))

    val evts = storage.eventsByContract(contractId)

    evts.size should be(1)

    val evt = evts.head

    evt.contractId should be(contractId)
    evt.eventType should be(eventType)
    evt.nodePubkey should be(ownPK)
  }

  test("roundtrip w. eventsByContractAndEventType", DatabaseTest) {
    storage.clear()
    val eventType = ContractDistributionEventType.MasterGotAContract
    val contractId = "ididid3"
    val ownPK = Array.fill(32)(55: Byte)

    storage.addEvent(ContractDistributionEvent(contractId,
      ownPK,
      eventType,
      System.currentTimeMillis()
    ))

    val evts = storage.eventsByContractAndEventType(contractId, eventType.id.toByte)

    evts.size should be(1)

    val evt = evts.head

    evt.contractId should be(contractId)
    evt.eventType should be(eventType)
    evt.nodePubkey should be(ownPK)
  }
}
