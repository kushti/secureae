package test.functional.db

import org.bitcoinj.core.Transaction
import org.bitcoinj.params.MainNetParams
import org.scalatest.{BeforeAndAfter, FunSuite}
import smartcontract.db.implementation.postgresql.BitcoinTransactionsStorageTest
import smartcontract.db.interfaces.{ActionId, GlobalTermId}
import smartcontract.utils.EncodingUtils
import test.DatabaseTest


class BitcoinTransactionsStorageSpecification extends FunSuite with BeforeAndAfter {
  private val storage = BitcoinTransactionsStorageTest

  private val txBytes = EncodingUtils.hex2bytes("0100000001098c8385386ebfcf757d0381a4f88f314704a54a738cc342d9a73c1d2956952b010000006b48304502210087e2ca29b20ac7c24c34ce0d670bfca217b1c3284850691843b887110fd779be0220595ffbd36fcee29ac5e332561f1c22e56b07de181170aa9d79ed11c917c6b8dd01210349c4bdf334569a7f026dd7d5196c6da46f9497e9b84aef34958d6151afb12b1effffffff0247f41000000000001976a914c8c0a28cd9c8fa169a30a4276b6a7ef63623802188ac69086c00000000001976a9141533152d7e1b364f91e8af462d83a6954a8b222e88ac00000000")

  private val tx = new Transaction(MainNetParams.get(), txBytes)

  val actionId = ActionId(GlobalTermId("cId", "tId"), success = true)

  test("put & get", DatabaseTest) {
    storage.clear()
    storage.putTransaction(actionId, tx)
    val tx2Opt = storage.getTransaction(actionId)
    assert(tx2Opt.isDefined)
    assert(tx.equals(tx2Opt.get))
  }

  test("put & getContractTransactions", DatabaseTest) {
    storage.clear()
    storage.putTransaction(actionId, tx)
    val tx2Opt = storage.getContractTransactions("cId").headOption.map(_._2)
    assert(tx2Opt.isDefined)
    assert(tx.equals(tx2Opt.get))
  }
}