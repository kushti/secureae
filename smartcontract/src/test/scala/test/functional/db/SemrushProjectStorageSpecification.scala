package test.functional.db

import org.scalatest.{BeforeAndAfter, FunSuite}
import smartcontract.db.implementation.postgresql.SemrushProjectStorageTest
import smartcontract.db.interfaces.SemrushProject
import test.DatabaseTest


class SemrushProjectStorageSpecification extends FunSuite with BeforeAndAfter {
  private val storage = SemrushProjectStorageTest

  test("roundtrip", DatabaseTest) {
    val prj = SemrushProject("prjid", "cid", "key")
    storage.addSemrushProject(prj)
    val prj2 = storage.projectsByContractId("cid").head

    assert(prj.contractId == prj2.contractId)
    assert(prj.projectId == prj2.projectId)
    assert(prj.key == prj2.key)
  }
}
