package test.functional.db

import org.scalatest.{Matchers, BeforeAndAfter, FunSuite}
import play.api.libs.json.{JsObject, Json}
import smartcontract.model._
import smartcontract.db.implementation.postgresql.ContractStorageTest
import test.DatabaseTest

class ContractsStorageSpecification extends FunSuite with BeforeAndAfter with Matchers {
  private lazy val storage = ContractStorageTest

  private lazy val startTime = System.currentTimeMillis()
  private lazy val body = "{\"description\": \"chain.com test contract description\",\"signature_deadline\": \"1423069620\",\"escrows\": [],\"id\": \"43190a908aab5386f6d8d6d9beb42390\",\"name\": \"chain.com test\",\"signers\": [{\"signature_key\": \"f63f15766260c453\",\"nxt_id\": \"NXT-AAJE-3Q9R-Q3U9-5ZPJJ\"}],\"terms\": [{\"success\": {\"actions\": []},\"failure\": {\"actions\": []},\"name\": \"1\",\"type\": \"payment\",\"expected\": {\"amount\": \"400\",\"currency\": \"BTC\",\"deadline\": \"1424279820\",\"to\": {\"address\": \"1GThJNvdqZsQT2iozhhiYwi9HTnTWGg9WE\"},\"type\": \"payment\",\"amount-currency\": \"USD\"}}],\"type\": \"payment\",\"source\": \"smartcontract.com\",\"attachments\": []}"
  private lazy val js = Json.parse(body).as[JsObject]
  private lazy val params = ContractParameters(startTime / 1000, js, None, ContractPrivacyOptions.Public, None)
  private lazy val contract = JsonToContract.apply(js, params, SmartContractTrackingState.PENDING).get

  test("roundtrip", DatabaseTest) {
    storage.clear()

    storage.addContract(contract)

    val fetchedContract = storage.getContract(contract.id)
    assert(fetchedContract.isDefined)
    assert(fetchedContract.get.lastUpdate == contract.lastUpdate)
    assert(fetchedContract.get.startTime == contract.startTime)
    assert(fetchedContract.get.sequences.size == contract.sequences.size)
    assert(fetchedContract.get == contract)
  }

  test("update & versions & activeContracts() test", DatabaseTest) {
    storage.clear()

    storage.addContract(contract)

    assert(storage.getAllVersions(contract.id).size == 1)

    val seq = contract.sequences.head._1
    val newState = new FinishTermState("completed")
    val updateTime = System.currentTimeMillis()
    val updContract = contract.copy(sequences = contract.sequences.updated(seq, newState), lastUpdate = updateTime)

    storage.updateContract(updContract)

    val lastContract = storage.getContract(contract.id).get
    assert(lastContract.sequences.head._2 == newState)
    assert(lastContract.lastUpdate == updateTime)
    assert(lastContract.startTime != lastContract.lastUpdate)

    assert(storage.getAllVersions(contract.id).size == 2)
    assert(storage.getAllVersions(contract.id).head._1 == updateTime)
    assert(storage.getAllVersions(contract.id).tail.head._1 != updateTime)
  }

  //todo: test for trackingState update
}