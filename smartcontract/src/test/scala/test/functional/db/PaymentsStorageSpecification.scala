package test.functional.db

import org.scalatest.{BeforeAndAfter, FunSuite}
import smartcontract.db.implementation.postgresql.PaymentsStorageTest
import smartcontract.db.interfaces.{GlobalTermId, PaymentDetails}
import smartcontract.model.{BtcImpl, UsdImpl}
import test.DatabaseTest


class PaymentsStorageSpecification extends FunSuite with BeforeAndAfter {
  private val storage = PaymentsStorageTest

  test("roundtrip", DatabaseTest) {
    val time = System.currentTimeMillis()
    val details = PaymentDetails(BtcImpl, UsdImpl, 5L, 0.1, 0.5, time)

    val gt1 = GlobalTermId("contract1", "term1")
    storage.addPayment(gt1, "paymentId1", details)

    assert(storage.getPayments(gt1).size == 1)
    assert(storage.getContractPayments(gt1.contractId).size == 1)

    val firstResult = storage.getPayments(gt1).head
    assert(firstResult._1 == "paymentId1")
    assert(firstResult._2.targetCurrency.id == UsdImpl.id)
    assert(firstResult._2.targetAmount > 0.49)
    assert(firstResult._2.targetAmount < 0.51)

    storage.clear()
  }

}