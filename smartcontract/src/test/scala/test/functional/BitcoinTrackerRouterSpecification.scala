package test.functional

import org.joda.time.DateTime
import org.scalatest.FunSuite
import smartcontract.api.bitcoin.BitcoinTrackerRouter

import scala.concurrent.duration._

class BitcoinTrackerRouterSpecification extends FunSuite with TestingCommons {
  test("lastBlockHeight", org.scalatest.tagobjects.Network) {
    BitcoinTrackerRouter.lastBlockHeight()
    Thread.sleep(20.seconds.toMillis)
    assert(BitcoinTrackerRouter.lastBlockHeight() > 300000)
  }

  test("received",  org.scalatest.tagobjects.Network) {
    val start0 = new DateTime(2014, 2, 3, 0, 0)
    val res0 = BitcoinTrackerRouter.received("1KhEieoC2X4CPGNocqkYBwPSaEZjhqDMhk", None, start0.getMillis, 2)
    assert(res0.isSuccess)
    val size0 = res0.get.size
    assert(size0 >= 1)

    val start = start0.minusYears(1)
    val res1 = BitcoinTrackerRouter.received("1KhEieoC2X4CPGNocqkYBwPSaEZjhqDMhk", None, start.getMillis, 2)
    assert(res1.isSuccess)
    val size1 = res1.get.size
    assert(size1 > size0)
  }
}
