package test.functional

import org.scalatest.FunSuite
import smartcontract.api.btcusdprice.BitstampTickerClient

import scala.concurrent.Await
import scala.concurrent.duration._


class BitstampTickerClientSpecification extends FunSuite with TestingCommons {

  test("simple rate getting with bitstamp", org.scalatest.tagobjects.Network) {
    val priceTryFuture = BitstampTickerClient.fetchLastPrice()
    val priceTry = Await.result(priceTryFuture, 2.minutes)
    assert(priceTry.isSuccess)
    assert(priceTry.get > 1)
  }
}
