package test.functional

import org.bitcoinj.core.{ECKey, Transaction}
import org.scalatest.FunSuite
import smartcontract.bitcoin.{BitcoinNodeMode, BitcoinSigningUtils}
import smartcontract.utils.EncodingUtils

import scala.util.Try


class MultisigSpecification extends FunSuite {

  test("multisig with injected outputs") {
    val mode = BitcoinNodeMode.SPV_MAINNET_TEST

    val txBytes = EncodingUtils.hex2bytes("0100000001ce126a9ca4c6e4b5aff36f04514821160dfa41990957e9eea67f61e5c74d325700000000920048304502203fe74dc5dbd36c330ee30acf872a637c0301755fa83ed080a4b34944fd50ff78022100c86ff8dced84ef516d207890bbbfb63246696ee7e9c4c76c37ee829bca8102220147522103297b5f8e34a96c6252079e5ef5301d190dc76ef5077fc5a169ea1ca55682276721037ea9c6a6fcb5828ef1fd13221afecd130193425f32a0dc13f3624f6745f1cdd352aeffffffff01204e0000000000001976a9149b8afce10b876dd27972d04c0cfd04cd4d073bc588ac00000000")
    val keyBytes = EncodingUtils.hex2bytes("1e930296492554bfc621ccabc511709e2bf9bab524bdeb356d2da33ee35d7849")
    val key = ECKey.fromPrivate(keyBytes).ensuring(_ != null)
    val (params, _, _) = BitcoinNodeMode.config(mode, key, startP2P = false, None)

    val outTxBytes = EncodingUtils.hex2bytes("01000000016cc63d8cd19822d31b9b3d232248e0f572d3551a06cefe1b760e0048a793592d000000008b483045022100dac4075951381c97170241bf563968df5020b07f2b210b5c5def1ffb92aa51aa02202c0c5683c58eb81dcbe2b03278a21db2d85d759a99387721f6cefb8d2c7aa91b014104b808685cce694ec8ad1df865546c8620420207baa0ec3a6af44f78451eb74dfd176a3932a6aaf960a0e3465b35339ec317ac8b3b87a6c3cc1f7507a24ba12d95ffffffff01307500000000000017a914fb8f1dc334d970413a0ffda652a063d792de7f998700000000")
    val out = new Transaction(params, outTxBytes).getOutput(0)

    val tx = new Transaction(params, txBytes)

    val signedTxTry = BitcoinSigningUtils.signTransaction(Seq(key), tx, Seq(out.getScriptPubKey))
    assert(signedTxTry.isSuccess)
    assert(!signedTxTry.get.sameElements(txBytes))

    val signedTx = new Transaction(params, signedTxTry.get)
    assert(Try(signedTx.verify()).isSuccess)
  }

  //todo: un-ingore after utxo set downloading/fixing with 3rd party
  test("multisig - chain") {
    val txBytes = EncodingUtils.hex2bytes("010000000187ac6379d2ac6568989ce81d9ea2ee74f8cf00116faac19b0c10df1bf81e5a0700000000490047522103297b5f8e34a96c6252079e5ef5301d190dc76ef5077fc5a169ea1ca55682276721037ea9c6a6fcb5828ef1fd13221afecd130193425f32a0dc13f3624f6745f1cdd352aeffffffff01204e0000000000001976a9149b8afce10b876dd27972d04c0cfd04cd4d073bc588ac00000000")

    val signingUtils = new BitcoinSigningUtils(BitcoinNodeMode.SPV_MAINNET, startP2P = false)
    val tx = new Transaction(signingUtils.networkParameters, txBytes)

    val signedTxBytes = signingUtils.signTransaction(tx)
      .ensuring(_.isSuccess, "Tx signing failed")
      .get
    assert(!signedTxBytes.sameElements(txBytes))

    val signedTx = new Transaction(signingUtils.networkParameters, signedTxBytes)
    signedTx.verify() //throws exception if not ok, doesn't check script though
    //and script can't be checked, as 2nd signature is needed
  }
}
