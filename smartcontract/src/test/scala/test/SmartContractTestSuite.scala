package test

import nxt.NxtFunctions._
import nxt.{Generator, LaunchingFunctions, NxtFunctions}
import org.scalatest.{BeforeAndAfterAll, Suites}
import smartcontract.bootstrap.{SmartContractBootstrap, VerificationServerEnvironment}
import test.functional._
import test.functional.actors.{ContractDistributionActorsSpecification, NodesCheckerSpecification}
import test.functional.db._
import test.unit._

class SmartContractTestSuite extends Suites(
  //unit
  new MessageSpecification
  , new PredicatesSpecification
  , new unit.BtcUsdTickerClientCascadeSpecification
  , new JsonToContractSpecification
  , new SmartContractSpecification
  , new CryptoSpecification
  , new EncryptionUtilsSpecification

  // actors
  , new NodesCheckerSpecification
  , new ContractDistributionActorsSpecification
  , new ContractsThroughoutSpecification

  //functional - db
  , new ContractDistributionEventStorageSpecification
  , new ContractsStorageSpecification
  , new PaymentsStorageSpecification
  , new BitcoinTransactionsStorageSpecification
  , new SemrushProjectStorageSpecification

  //functional
  , new SmartContractHttpRouteSpecification
  , new BitcoinTrackerRouterSpecification
  , new CriisComScraperSpecification
  , new functional.BtcUsdTickerClientCascadeSpecification
  , new CalifornianRealEstateQuerySpecification
  , new GoogleRankQuerySpecification
  , new BitstampTickerClientSpecification
  , new functional.MultisigSpecification
  , new ContractsBlockchainDbSpecification
) with BeforeAndAfterAll {

  var startingHeight = 0

  override def beforeAll() = {
    SmartContractBootstrap.loadConfig(VerificationServerEnvironment.LocalTestnet)
    LaunchingFunctions.launch()
    startingHeight = NxtFunctions.currentHeight

    println("Starting with height: " + startingHeight)
    val phrase = "aSykrgKGZNlSVOMDxkZZgbTvQqJPGtsBggb"
    Generator.startForging(phrase)
    //.ensuring(_ => NxtFunctions.currentHeight >= Constants.VOTING_SYSTEM_BLOCK)
  }

  override def afterAll() =
    forgetLastBlocks(currentHeight - startingHeight + 1,
      removeUnconfirmedTransactions = true)
}