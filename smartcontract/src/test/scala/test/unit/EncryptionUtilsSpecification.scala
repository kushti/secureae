package test.unit

import org.scalatest.FunSuite

import scala.util.Random

class EncryptionUtilsSpecification extends FunSuite {

  import smartcontract.utils.EncodingUtils._

  test("hexencoding roundtrip") {
    val bytes = Random.alphanumeric.take(50).map(_.toByte).toArray
    assert(hex2bytes(bytes2hex(bytes)).sameElements(bytes))
  }

  test("base64 roundtrip") {
    val bytes = Random.alphanumeric.take(50).map(_.toByte).toArray
    assert(base64bytes(bytes2base64(bytes)).sameElements(bytes))
    assert(!base64bytes(bytes2base64(bytes)).sameElements(bytes.tail))
  }
}
