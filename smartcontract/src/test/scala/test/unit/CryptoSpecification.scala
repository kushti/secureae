package test.unit

import org.scalatest.FunSuite
import smartcontract.utils.crypto.{CryptographicHash, PasswordBasedCrypto}
import PasswordBasedCrypto._
import CryptographicHash._
import smartcontract.utils.EncodingUtils._

class CryptoSpecification extends FunSuite {
  val testText = "hello world"
  val testBytes = "hello world".getBytes
  val password = generateEncodedPass()

  test("doublesha test") {
    assert(doubleSha256(testBytes).sameElements(sha256(sha256(testBytes))))
  }

  test("encrypt/decrypt roundtrip") {
    assert(!encrypt(testText, bytes2base64(password)).get.sameElements(testBytes))
    assert(decodeDecrypt(encryptEncode(testText, bytes2base64(password)).get, bytes2base64(password)).get === testText)
    assert(decodeDecrypt(encryptEncode(testText, bytes2base64(password)).get, bytes2base64(password)+"0").isFailure)
  }

  test("hash comparing with externally computed value") {
    //checking sha256 result with http://www.xorbin.com/tools/sha256-hash-calculator
    assert(bytes2hex(sha256(testBytes)) == "b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9")
  }
}