package test.unit

import org.scalatest.FunSuite
import play.api.libs.json.{JsObject, Json}
import smartcontract.model.JsonToContract


class JsonToContractSpecification extends FunSuite {

  test("outcomes parsing") {
    val outcomesText =
      """{"outcomes" : {
          "1" : {
            "success" : ["0100000001098c8385386ebfcf757d0381a4f88f314704a54a738cc342d9a73c1d2956952b010000006b48304502210087e2ca29b20ac7c24c34ce0d670bfca217b1c3284850691843b887110fd779be0220595ffbd36fcee29ac5e332561f1c22e56b07de181170aa9d79ed11c917c6b8dd01210349c4bdf334569a7f026dd7d5196c6da46f9497e9b84aef34958d6151afb12b1effffffff0247f41000000000001976a914c8c0a28cd9c8fa169a30a4276b6a7ef63623802188ac69086c00000000001976a9141533152d7e1b364f91e8af462d83a6954a8b222e88ac00000000"],
            "failure" : ["0100000001098c8385386ebfcf757d0381a4f88f314704a54a738cc342d9a73c1d2956952b010000006b48304502210087e2ca29b20ac7c24c34ce0d670bfca217b1c3284850691843b887110fd779be0220595ffbd36fcee29ac5e332561f1c22e56b07de181170aa9d79ed11c917c6b8dd01210349c4bdf334569a7f026dd7d5196c6da46f9497e9b84aef34958d6151afb12b1effffffff0247f41000000000001976a914c8c0a28cd9c8fa169a30a4276b6a7ef63623802188ac69086c00000000001976a9141533152d7e1b364f91e8af462d83a6954a8b222e88ac00000000"]
          },
          "2" : {
                    "success" : ["0100000001098c8385386ebfcf757d0381a4f88f314704a54a738cc342d9a73c1d2956952b010000006b48304502210087e2ca29b20ac7c24c34ce0d670bfca217b1c3284850691843b887110fd779be0220595ffbd36fcee29ac5e332561f1c22e56b07de181170aa9d79ed11c917c6b8dd01210349c4bdf334569a7f026dd7d5196c6da46f9497e9b84aef34958d6151afb12b1effffffff0247f41000000000001976a914c8c0a28cd9c8fa169a30a4276b6a7ef63623802188ac69086c00000000001976a9141533152d7e1b364f91e8af462d83a6954a8b222e88ac00000000"]
          }
         }}
      """
    val json = Json.parse(outcomesText)

    val outcomesTry = JsonToContract.parseOutcomes("contractId", Set("1", "2", "3"), (json \ "outcomes").asOpt[JsObject])

    assert(outcomesTry.isSuccess)
    assert(outcomesTry.get.size === 3)
    assert(outcomesTry.get.last._1.termId.contractId === "contractId")
    assert(outcomesTry.get.last._1.termId.termId === "2")
    assert(outcomesTry.get.last._1.success === true)
  }
}