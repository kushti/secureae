package test.unit

import org.scalatest.{BeforeAndAfter, FunSuite}
import play.api.libs.json.{JsObject, Json}
import smartcontract.model._
import smartcontract.utils.crypto.CryptographicHash


class SmartContractSpecification extends FunSuite with BeforeAndAfter {

  private lazy val bitcoinContractText = "{\"description\":\"chain.com test contract description\",\"signature_deadline\":\"1423069620\",\"escrows\":[],\"id\":\"43190a908aab5386f6d8d6d9beb42390\",\"name\":\"chain.com test\",\"signers\":[{\"signature_key\":\"f63f15766260c453\",\"nxt_id\":\"NXT-AAJE-3Q9R-Q3U9-5ZPJJ\"}],\"terms\":[{\"success\":{\"actions\":[]},\"failure\":{\"actions\":[]},\"name\":\"1\",\"type\":\"payment\",\"expected\":{\"amount\":\"400\",\"currency\":\"BTC\",\"deadline\":\"1424279820\",\"to\":{\"address\":\"1GThJNvdqZsQT2iozhhiYwi9HTnTWGg9WE\"},\"type\":\"payment\",\"amount-currency\":\"USD\"}}],\"type\":\"payment\",\"attachments\":[]}"
  private lazy val js = Json.parse(bitcoinContractText).as[JsObject]
  private lazy val ct = ContractParameters(startTime / 1000, js, None, ContractPrivacyOptions.Public, passwordOpt = None)
  private lazy val bitcoinContract = JsonToContract(js, ct, SmartContractTrackingState.PENDING).get
  private val startTime = (System.currentTimeMillis() / 1000) * 1000

  test("serialization / deserialization") {
    assert(bitcoinContract.startTime == startTime)

    val dc = SmartContract.deserialize(SmartContract.serialize(bitcoinContract))
    assert(dc.contractParams.contractText == bitcoinContractText)
    assert(dc.contractParams.hashHex == CryptographicHash.sha256HexEncoded(bitcoinContractText))
    assert(dc.startTime == startTime)
    assert(dc.sequences != null)
    assert(dc.sequences.nonEmpty)
    assert(dc.sequences.head._1.terms.size == 1)
  }

  test("serialization / deserialization - SmartContractDeepSerializer - bitcoin") {
    assert(bitcoinContract.startTime == startTime)

    val js = SmartContractDeepSerializer.serialize(bitcoinContract)
    println(js)
    val dc = SmartContractDeepSerializer.deserialize(js)
    assert(dc.contractParams.contractText == bitcoinContractText)
    assert(dc.contractParams.hashHex == CryptographicHash.sha256HexEncoded(bitcoinContractText))
    assert(dc.startTime == startTime)
    assert(dc.sequences != null)
    assert(dc.sequences.nonEmpty)
    assert(dc.sequences.head._1.terms.size == 1)
  }

  test("serialization / deserialization - SmartContractDeepSerializer - seo") {
    val seoContractText = "{\"description\":\"testing SEO\",\"signature_deadline\":\"1431490956\",\"escrows\":[{\"deposit_address\":\"1KiSWBum8uQANb7tWdvRfm78yhraUCoQJu\",\"amount\":\"93300\",\"currency\":\"BTC\",\"name\":\"1\"}],\"id\":\"eba2dbc064a94d96306891d8a8efb94c\",\"name\":\"testing SEO\",\"signers\":[{\"signature_key\":\"b97b6515039f5520\",\"nxt_id\":\"NXT-QMY7-K73R-88EJ-2SN6V\"}],\"terms\":[{\"success\":{\"actions\":[{\"type\":\"payment\",\"to\":[{\"signature_key\":\"b97b6515039f5520\",\"nxt_id\":\"NXT-QMY7-K73R-88EJ-2SN6V\",\"address\":null}]}]},\"failure\":{\"actions\":[{\"type\":\"payment\",\"to\":[{\"signature_key\":\"b97b6515039f5520\",\"nxt_id\":\"NXT-QMY7-K73R-88EJ-2SN6V\",\"address\":null}]}]},\"name\":\"1\",\"type\":\"seo\",\"expected\":{\"min\":\"1\",\"max\":\"10\",\"provider\":\"semrush.com\",\"deadline\":\"1432009320\",\"result\":\"cheese.com\",\"query\":{\"phrase\":\"cheese\",\"source\":\"google\",\"locale\":\"us\"},\"type\":\"seo\"},\"escrow\":\"1\"}],\"type\":\"seo\",\"attachments\":[{\"url\":\"https://smartcontract.s3.amazonaws.com/uploads/991a97825b98618b5f69afb9ec4ca7a9/Screen%20Shot%202015-05-11%20at%204.26.30%20PM.png\",\"sha\":\"cd98c78f12afcf97a158276800a87ef721a6eb6db828ee1a728315bfa2f02c87\"}]}"
    val cjs = Json.parse(seoContractText).as[JsObject]
    val ct = ContractParameters(startTime / 1000, cjs, None, ContractPrivacyOptions.Public, passwordOpt = None)
    val seoContract = JsonToContract(cjs, ct, SmartContractTrackingState.PENDING).get

    assert(seoContract.startTime == startTime)

    val js = SmartContractDeepSerializer.serialize(seoContract)
    val dc = SmartContractDeepSerializer.deserialize(js)
    assert(dc.contractParams.contractText == seoContractText)
    assert(dc.contractParams.hashHex == CryptographicHash.sha256HexEncoded(seoContractText))
    assert(dc.startTime == startTime)
    assert(dc.sequences != null)
    assert(dc.sequences.nonEmpty)
    assert(dc.sequences.head._1.terms.size == 1)
  }
}
