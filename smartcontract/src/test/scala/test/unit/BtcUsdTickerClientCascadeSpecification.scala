package test.unit

import org.scalatest.{BeforeAndAfter, FunSuite}
import smartcontract.api.btcusdprice.{BtcUsdTickerClient, BtcUsdTickerClientCascadeInterface}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

class BtcUsdTickerClientCascadeSpecification extends FunSuite with BeforeAndAfter {

  object badClient extends BtcUsdTickerClient {
    override val clientName: String = "badClient"

    override def fetchLastPrice(): Future[Try[Double]] = Future(Failure(new Exception("bad behavior")))
  }

  object goodClient extends BtcUsdTickerClient {
    override val clientName: String = "goodClient"

    override def fetchLastPrice(): Future[Try[Double]] = Future(Success(5.0))
  }

  object goodClient2 extends BtcUsdTickerClient {
    override val clientName: String = "goodClient2"

    override def fetchLastPrice(): Future[Try[Double]] = Future(Success(15.0))
  }

  var client3 = false

  object goodClient3 extends BtcUsdTickerClient {
    override val clientName: String = "goodClient2"

    override def fetchLastPrice(): Future[Try[Double]] = {
      client3 = true
      Future(Success(25.0))
    }
  }

  case class BtcUsdTickerClientCascadeStub(override val clients: Seq[BtcUsdTickerClient])
    extends BtcUsdTickerClientCascadeInterface {
    override val clientName: String = "TestCascade"
  }

  test("badClient & caching") {
    Thread.sleep(10000)
    val t = Await.result(BtcUsdTickerClientCascadeStub(Seq(badClient)).fetchLastPrice(), 1.minute)
    assert(t.isFailure)
  }

  test("goodClient") {
    Thread.sleep(6000)
    val t = Await.result(BtcUsdTickerClientCascadeStub(Seq(goodClient)).fetchLastPrice(), 2.minutes)
    assert(t.isSuccess)

    val t2 = Await.result(BtcUsdTickerClientCascadeStub(Seq(goodClient, goodClient2)).fetchLastPrice(), 2.minutes)
    assert(t2.get < 10)

    BtcUsdTickerClientCascadeStub(Seq(goodClient, goodClient2))
    assert(!client3)
  }

  test("badClient & goodClient") {
    Thread.sleep(6000)

    val t = Await.result(BtcUsdTickerClientCascadeStub(Seq(badClient, goodClient)).fetchLastPrice(), 1.minute)
    assert(t.isSuccess)

    val t2 = Await.result(BtcUsdTickerClientCascadeStub(Seq(badClient, goodClient, goodClient2)).fetchLastPrice(), 1.minute)
    assert(t2.get < 10)
  }
}