package test.unit

import org.scalatest.{BeforeAndAfter, FunSuite}
import smartcontract.model._

class PredicatesSpecification extends FunSuite with BeforeAndAfter {

  test("StringPartOf") {
    assert(StringPartOf("foo").matchPattern(StringValue("foobar")))
    assert(StringPartOf("oob").matchPattern(StringValue("foobar")))
    assert(!StringPartOf("fooz").matchPattern(StringValue("foobar")))
  }

  test("StringEq") {
    assert(StringEq("foo").matchPattern(StringValue("foo")))
    assert(!StringEq("foo").matchPattern(StringValue("foobar")))
  }

  test("NotLessThan") {
    assert(NotLessThan(5).matchPattern(LongValue(5)))
    assert(NotLessThan(4).matchPattern(LongValue(5)))
    assert(!NotLessThan(6).matchPattern(LongValue(5)))
    assert(NotLessThan(-5).matchPattern(LongValue(-5)))
  }

  test("WithinRange") {
    assert(WithinRange(1, 10).matchPattern(LongValue(1)))
    assert(WithinRange(1, 10).matchPattern(LongValue(10)))
    assert(WithinRange(1, 10).matchPattern(LongValue(9)))
    assert(!WithinRange(1, 10).matchPattern(LongValue(11)))
    assert(!WithinRange(1, 10).matchPattern(LongValue(0)))
  }

  test("StringSeqsPartOf") {
    assert(StringSeqsPartOf(Seq("a") -> Seq("c"))
      .matchPattern(StringSeqsValue(Seq("a", "b") -> Seq("b", "c"))))

    assert(!StringSeqsPartOf(Seq("c") -> Seq("c"))
      .matchPattern(StringSeqsValue(Seq("a", "b") -> Seq("b", "c"))))

    assert(!StringSeqsPartOf(Seq("c") -> Seq("a"))
      .matchPattern(StringSeqsValue(Seq("a", "b") -> Seq("b", "c"))))
  }
}
