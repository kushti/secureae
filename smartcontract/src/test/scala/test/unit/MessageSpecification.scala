package test.unit

import nxt.Account
import org.scalatest.FunSuite
import smartcontract.actors.p2p.message.{MessageProducer, Message, ContractAbortMessage, ContractAckMessage}
import smartcontract.utils.crypto.{SigningFunctions, CryptographicHash}
import test.functional.TestingCommons

class MessageSpecification extends FunSuite with TestingCommons {
  test("ping message producing & verification"){
    val msg = MessageProducer.ping()
    assert(SigningFunctions.ownPublicKey.sameElements(msg.senderPubkey))
    assert(msg.signedMsg.sameElements(CryptographicHash.doubleSha256(Array.fill(5)(0))))
    assert(SigningFunctions.sign(msg.signedMsg).sameElements(msg.signature))
    assert(msg.verify())
  }

  test("ping message round-trip") {
    val msg = MessageProducer.ping()
    val msg2 = Message.parse(msg.bytes).get
    assert(msg2.verify())
    assert(msg.signature.sameElements(msg2.signature))
  }

  test("contract message producing & verification"){
    val ct = simpleBitcoinContractText("5","6")
    val msg = MessageProducer.contract(ct)
    assert(SigningFunctions.ownPublicKey.sameElements(msg.senderPubkey))
    assert(SigningFunctions.sign(msg.signedMsg).sameElements(msg.signature))
    assert(msg.verify())
  }

  test("contract message round-trip") {
    val ct = simpleBitcoinContractText("5","6")
    val msg = MessageProducer.contract(ct)
    val msg2 = Message.parse(msg.bytes).get
    assert(msg2.verify())
    assert(msg.signature.sameElements(msg2.signature))
  }

  test("contractack message round-trip & verification") {
    val msg = MessageProducer.contractAck("abc")
    val msg2 = Message.parse(msg.bytes).get
    assert(msg2.verify())
    assert(msg.signature.sameElements(msg2.signature))

    val cam = msg2.concrete().get.asInstanceOf[ContractAckMessage]
    assert(cam.contractId == "abc")
  }

  test("contractabort message round-trip & verification") {
    val msg = MessageProducer.contractAbort("abcd")
    val msg2 = Message.parse(msg.bytes).get
    assert(msg2.verify())
    assert(msg.signature.sameElements(msg2.signature))

    val cam = msg2.concrete().get.asInstanceOf[ContractAbortMessage]
    assert(cam.contractId == "abcd")
  }
}