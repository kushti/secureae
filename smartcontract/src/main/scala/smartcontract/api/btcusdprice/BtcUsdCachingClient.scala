/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.btcusdprice

import java.util.concurrent.atomic.AtomicLong


import smartcontract.bootstrap.VerificationServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}


object BtcUsdCachingClient extends BtcUsdTickerClient {
  private var cachedPrice = 0.0
  private val cachedTime = new AtomicLong(0)

  private lazy val interval = VerificationServerConfig.getDuration("btc-usd-caching-interval").toMillis

  override val clientName: String = "BTC/USD cache"

  override def fetchLastPrice(): Future[Try[Double]] = {
    val current = System.currentTimeMillis()
    if (cachedPrice < 0.001 || System.currentTimeMillis() - cachedTime.getAndSet(current) > interval) {
      Future(Failure(new Exception()))
    } else Future(Success(cachedPrice))
  }

  def setCache(price: Double) {
    cachedPrice = price
    cachedTime.set(System.currentTimeMillis())
  }
}