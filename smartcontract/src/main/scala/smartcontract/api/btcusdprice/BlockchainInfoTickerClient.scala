/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.btcusdprice

import play.api.libs.json.Json
import smartcontract.AppRegistry
import smartcontract.actors.monitoring.ExternalError
import smartcontract.api.HttpClient
import smartcontract.bootstrap.VerificationServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try


object BlockchainInfoTickerClient extends BtcUsdTickerClient {
  lazy val url = "https://blockchain.info/ticker"
  lazy val webClient = new HttpClient()(AppRegistry.system, AppRegistry.monitoring)
  lazy val timeout = VerificationServerConfig.getDuration("usd-req-timeout")
  lazy val interval = VerificationServerConfig.getDuration("usd-req-interval")
  override val clientName: String = "Blockchain.Info BTC/USD Ticker"

  override def fetchLastPrice(): Future[Try[Double]] =
    webClient.get(url).map { bodyTry =>
      val resTry = bodyTry.flatMap { js => Try((Json.parse(js) \ "USD" \ "last").as[Double]) }
      resTry.recover { case e: Throwable =>
        e.printStackTrace()
        val monitoringMsg = s"Problems with blockchain.info markets data(USD/BTC rate): ${e.getMessage}"
        AppRegistry.monitoring ! ExternalError(monitoringMsg)
      }
      resTry
    }
}



