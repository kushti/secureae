/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.btcusdprice

import smartcontract.AppRegistry
import smartcontract.actors.monitoring.ExternalError

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

trait BtcUsdTickerClientCascadeInterface extends BtcUsdTickerClient {
  val clients: Seq[BtcUsdTickerClient]

  override def fetchLastPrice(): Future[Try[Double]] = {
    val init: Future[Try[Double]] = BtcUsdCachingClient.fetchLastPrice()

    clients.foldLeft(init) { case (prevTryFuture, client) =>
      prevTryFuture.map { prevTry =>
        prevTry match {
          case Success(price) =>
            Future(Success(price))

          case Failure(e) =>
            client.fetchLastPrice().map(_ match {
              case Success(price) =>
                BtcUsdCachingClient.setCache(price)
                Success(price)
              case Failure(ee) =>
                val extErr = ExternalError(s"$clientName returned error " + ee.getMessage)
                AppRegistry.monitoring ! extErr
                Failure(ee)
            })
        }
      }.flatMap(x => x)
    }
  }
}
