/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.http

import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout
import nxt.crypto.{Crypto, EncryptedData}
import nxt.util.Convert
import org.bitcoinj.core.{ECKey, Transaction}
import org.bitcoinj.params.MainNetParams
import play.api.libs.json.Json
import smartcontract.AppRegistry
import smartcontract.actors.{UpdatePublisher, IncomingContractHandler, ContractExecutionAgentStub, TrackContract}
import smartcontract.actors.monitoring.ExternalError
import smartcontract.bitcoin.BitcoinSigningUtils
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.model.{SmartContractTrackingState, SmartContract, JsonToContract, ContractParameters}
import smartcontract.model.blockchain.ContractsBlockchainDb
import spray.http.MediaTypes._
import spray.routing.HttpService._
import smartcontract.utils.{Logging, EncodingUtils}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}


trait SmartContractHttpRoute extends IncomingContractHandler with Logging {

  lazy val smartContractRoute =
    pathPrefix("smartcontracts") {
      path("utils" / "decryptFrom") {
        post {
          respondWithMediaType(`application/json`) {
            entity(as[String]) { body =>
              val js = Json.parse(body)
              val pk = Convert.parseHexString((js \ "publicKey").as[String])
              val data = Convert.parseHexString((js \ "data").as[String])
              val nonce = Convert.parseHexString((js \ "nonce").as[String])
              val sp = (js \ "secretPhrase").as[String]
              val uncompress = (js \ "uncompressDecryptedMessage").asOpt[Boolean].getOrElse(true)

              val decrypted = new EncryptedData(data, nonce).decrypt(Crypto.getPrivateKey(sp), pk)

              val dmBytes = if (uncompress && decrypted.nonEmpty) Convert.uncompress(decrypted) else decrypted

              val dm = Convert.toString(dmBytes)

              complete(Json.obj("decryptedMessage" -> dm).toString())
            }
          }
        }
      } ~ path("utils" / "checkContract") {
        post {
          respondWithMediaType(`application/json`) {
            entity(as[String]) { body =>
              val v = ContractParameters.parse(body)
                .flatMap(params => JsonToContract(params.contractJson, params, SmartContractTrackingState.PENDING))
                .isSuccess
              complete(Json.obj("valid" -> v).toString())
            }
          }
        }
      } ~ path("utils" / "checkContractExecution") {
        post {
          respondWithMediaType(`application/json`) {
            entity(as[String]) { body =>
              complete(
                ContractParameters.parse(body)
                  .flatMap(params => JsonToContract(params.contractJson, params, SmartContractTrackingState.PENDING))
                  .map[String] { contract =>

                  val json = Json.parse(body)
                  val termName = (json \ "term").as[String]
                  val success = (json \ "success").as[Boolean]
                  val chainPublishing = (json \ "publish-chain").as[Boolean]
                  val frontendNotifying = (json \ "publish-frontend").as[Boolean]

                  implicit val asys = AppRegistry.system

                  val paProps = Props(UpdatePublisher(chainPublishing, frontendNotifying, notifyActor = true))
                  val publishingAgent = asys.actorOf(paProps)

                  val eaProps = Props(new ContractExecutionAgentStub(publishingAgent, termName, success))
                  val executingAgent = asys.actorOf(eaProps)

                  implicit val timeout = Timeout(1.minute)
                  Await.result((executingAgent ? TrackContract(contract)).mapTo[String], 1.minute)
                }.recover { case t =>
                  t.printStackTrace()
                  t.getMessage
                }.getOrElse[String]("wrong contract"))
            }
          }
        }
      } ~ path("utils" / "smartcontract/bitcoin" / "unconfirmedIncome") {
        get {
          complete {
            val bitcoinUtils = AppRegistry.bitcoinSigner
            (bitcoinUtils.unconfirmedIncome() match {
              case Some(unconfirmedIncome) =>
                Json.obj("status" -> "ok",
                  "address" -> unconfirmedIncome._1.toString,
                  "satoshis" -> unconfirmedIncome._2)
              case None =>
                Json.obj("status" -> "error", "details" -> "No transactions")
            }).toString()
          }
        }
      } ~ path("utils" / "smartcontract/bitcoin" / "sign") {
        post {
          respondWithMediaType(`application/json`) {
            entity(as[String]) { body =>
              complete {
                val js = Json.parse(body)
                val txBytes = Convert.parseHexString((js \ "transaction").as[String])
                val keyBytes = Convert.parseHexString((js \ "key").as[String])

                val tx = new Transaction(MainNetParams.get, txBytes)
                val key = ECKey.fromPrivate(keyBytes)

                (BitcoinSigningUtils.signatures(key, tx) match {
                  case Success(sigs) =>
                    val sigsHex = sigs.map(EncodingUtils.bytes2hex)
                    Json.obj("status" -> "ok", "signatures" -> sigsHex)
                  case Failure(t) =>
                    Json.obj("status" -> "error", "details" -> t.getMessage)
                }).toString()
              }
            }
          }
        }
      } ~ path("startcontract") {
        post {
          respondWithMediaType(`application/json`) {
            entity(as[String]) { contractText =>
              log.info(s"Got startcontract input: $contractText")

              handleIncomingContract(contractText, master = true) match {
                case Success(contract) =>
                  val params = contract.contractParams

                  complete {
                    Json.obj("status" -> "received",
                      "details" -> contract.id,
                      "hash" -> params.hashHex,
                      "text-hashed" -> params.contractText
                    ).toString()
                  }

                case Failure(t) => reportContractProblem(contractText, t)
              }
            }
          }
        }
      } ~ path("number" / "total") {
        get {
          respondWithMediaType(`application/json`) {
            complete(Json.obj("contracts-total" -> StoragesImpl.contracts.size()).toString())
          }
        }
      } ~ path("number" / "active") {
        get {
          respondWithMediaType(`application/json`) {
            complete(Json.obj("contracts-active" -> StoragesImpl.contracts.activeSize()).toString())
          }
        }
      } ~ path("json" / Segment) { case id =>
        get {
          respondWithMediaType(`application/json`) {
            complete(SmartContract.serialize(StoragesImpl.contracts.getContract(id).get).toString())
          }
        }
      } ~ path("ids" / "publish-check") {
        get {
          respondWithMediaType(`application/json`) {
            complete {
              ContractsBlockchainDb.contractIdsBeingCheckedForPublishing().mkString(",")
            }
          }
        }
      }
    }

  private def reportContractProblem(contractText: String, t: Throwable) = {
    val id = extractIdRoughly(contractText).getOrElse("unknown")
    val msg = s"Wrong contract json posted, contact devs to check logs. Contract ID: $id"
    AppRegistry.monitoring ! ExternalError(msg)
    log.error(s"Failed contract body: $contractText", t)
    complete(Json.obj("status" -> "error", "body" -> contractText, "details" -> t.getMessage).toString())
  }

  private def extractIdRoughly(contractText: String): Option[String] = Try {
    val ct = contractText.replaceAll(" ", "")

    val prefix = "\"id\":\""
    val ctLeftTrimmed = ct.substring(ct.indexOf(prefix) + prefix.length)
    ctLeftTrimmed.substring(0, ctLeftTrimmed.indexOf('"'))
  }.toOption
}