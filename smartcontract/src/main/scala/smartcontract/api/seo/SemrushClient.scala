/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.seo

import java.net.URLEncoder

import com.google.common.net.InternetDomainName
import play.api.libs.json.{JsArray, JsValue, Json}
import smartcontract.AppRegistry
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.actors.monitoring.ExternalError
import smartcontract.api.HttpClient
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.db.interfaces.SemrushProject
import smartcontract.utils.Logging

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}


object SemrushClient extends Logging {
  private val ProjectNameLength = 20

  private type ProjectId = String

  private lazy val ApiKey = VerificationServerConfig.getString("semrush-key")

  private lazy val httpClient = new HttpClient()(AppRegistry.system, AppRegistry.monitoring)

  private lazy val projectsStorage = StoragesImpl.semrushProjects

  private lazy val reqTimeout = VerificationServerConfig.getDuration("semrush-timeout")

  private def projects(): Seq[SemrushProject] = {
    val url = s"http://api.semrush.com/management/v1/projects?key=$ApiKey"

    Try(Await.result(httpClient.get(url), reqTimeout)).flatten.map { body =>
      Json.parse(body).as[JsArray].value.map { js =>
        val kObj = (js \ "keywords").as[JsArray].value.head

        val contractId = (kObj \ "tags").as[JsArray].value.head.as[String]
        val projectId = (js \ "project_id").as[Long].toString
        val key = (kObj \ "keyword").as[String]
        SemrushProject(projectId, contractId, key)
      }
    } match {
      case Success(seq) => seq
      case Failure(t) =>
        log.error("Error during getting projects list ", t)
        AppRegistry.monitoring ! ExternalError(s"Error during getting projects list: ${t.getMessage}")
        Seq()
    }
  }

  private def deleteProject(projectId: ProjectId): Try[Unit] = {
    log.debug(s"Deleting project $projectId")
    val url = s"http://api.semrush.com/management/v1/projects/$projectId?key=$ApiKey"

    Try(Await.result(httpClient.delete(url), reqTimeout)).flatten.map(_ => Unit)
  }

  /**
   * Remove projects for finished contracts from SemRush & internal database
   */
  def removeOutdatedProjects(): Unit = {
    log.info("removeOutdatedProjects() being called")

    val contractsStorage = StoragesImpl.contracts

    projects().foreach { case sp =>
      val isToDelete = contractsStorage.getContract(sp.contractId).map(_.finished).getOrElse(true)
      if (isToDelete) {
        val pid = sp.projectId
        log.info(s"Going to delete project $pid")
        deleteProject(pid) match {
          case Success(_) =>
            projectsStorage.removeSemrushProject(sp)
          case Failure(t) =>
            log.error("Can't delete SemRush project ", t)
            AppRegistry.monitoring ! ExternalError(s"Can't delete SemRush project: ${t.getMessage}")
        }
      }
    }
  }

  private def phraseOrganic(domain: String, keyPhrase: String, geo: String): Try[Option[Long]] = {
    val phraseEnc = URLEncoder.encode(keyPhrase, "UTF-8")
    val url = s"http://api.semrush.com/?type=phrase_organic&key=$ApiKey&display_limit=10&export_columns=Dn&phrase=$phraseEnc&database=$geo"

    Try(Await.result(httpClient.get(url), reqTimeout)).flatten match {
      case Success(resp) =>
        if (resp.contains("ERROR") && resp.contains("::")) {
          if (resp.contains("NOTHING FOUND")) {
            Success(None)
          } else {
            AppRegistry.monitoring ! ExternalError(s"SemRush error for $keyPhrase: $resp")
            Failure(new Exception(s"SemRush error for $keyPhrase: $resp"))
          }
        } else {
          val res = resp
            .split("\n")
            .map(_.trim)
            .zipWithIndex
            .find { case (s, i) => s == domain }
            .map(_._2)
            .getOrElse(Int.MaxValue)
            .toLong
          Success(Some(res))
        }
      case Failure(e) => Failure(e) //not more idiomatic "f:Failure => f" to avoid typechecker errors
    }
  }

  private def getBaseDomain(url: String) =
    InternetDomainName.from(url).topPrivateDomain().toString


  private def makeProject(contractId: String, domain: String, keyPhrase: String): Try[ProjectId] = {
    val url = s"http://api.semrush.com/management/v1/projects?key=$ApiKey"
    val req = Json.obj(
      "project_name" -> contractId.take(ProjectNameLength),
      "url" -> getBaseDomain(domain),
      "key" -> ApiKey,
      "keywords" -> Json.arr(
        Json.obj(
          "keyword" -> keyPhrase,
          "tags" -> Json.arr(contractId)
        )
      )
    ).toString()

    Try(Await.result(httpClient.post(url, req), reqTimeout)).flatten.flatMap { case respBody =>
      (Json.parse(respBody) \ "project_id").asOpt[Long] match {
        case Some(pid) => Success(pid.toString)
        case None => Failure(new Exception(s"No project id in result: $respBody"))
      }
    }
  }

  private def enableTracking(projectId: ProjectId, domain: String, geo: String): Try[Unit] = {
    (geo match {
      case g: String if g == "us" => Success("2840")
      case g: String if g == "uk" => Success("2826")
      case g: String if g == "ca" => Success("2124")
      case g: String if g == "ru" => Success("2643")
      case g: String if g == "de" => Success("2276")
      case g: String if g == "fr" => Success("2250")
      case g: String if g == "es" => Success("2724")
      case g: String if g == "it" => Success("2380")
      case g: String if g == "br" => Success("2076")
      case g: String if g == "au" => Success("2036")
      case g: String if g == "ar" => Success("2032")
      case g: String if g == "be" => Success("2056")
      case g: String if g == "ch" => Success("2756")
      case g: String if g == "dk" => Success("2208")
      case g: String if g == "fi" => Success("2246")
      case g: String if g == "hk" => Success("2344")
      case g: String if g == "il" => Success("2376")
      case g: String if g == "mx" => Success("2484")
      case g: String if g == "nl" => Success("2528")
      case g: String if g == "no" => Success("2578")
      case g: String if g == "pl" => Success("2616")
      case g: String if g == "se" => Success("2752")
      case g: String if g == "sg" => Success("2702")
      case g: String if g == "tr" => Success("2792")
      case g: String if g == "jp" => Success("2392")
      case g: String if g == "ie" => Success("2372")
      case g: String if g == "in" => Success("2356")

      case _ => Failure(new Exception(s"Unsupported geo parameter for enableTracking() : $geo"))
    }).flatMap { countryId =>

      val trackingUrlType = getBaseDomain(domain) == domain match {
        case true => "rootdomain"
        case false => "subdomain"
      }

      val url = s"http://api.semrush.com/management/v1/projects/$projectId/tracking/enable?key=$ApiKey"
      val req = Json.obj(
        "tracking_url_type" -> trackingUrlType,
        "tracking_url" -> domain,
        "country_id" -> countryId,
        "weekly_notification" -> "0"
      ).toString()
      Try(Await.result(httpClient.post(url, req), reqTimeout)).flatten.flatMap { case respBody =>
        (Json.parse(respBody) \ "message").asOpt[String] match {
          case Some(errMsg) => Failure(new Exception(s"Got the error from the SemRush server: $errMsg"))
          case None => Success(Unit)
        }
      }
    }
  }

  private def projectKeyTracking(projectId: ProjectId, domain: String, keyPhrase: String): Try[Option[Long]] = {
    log.debug(s"Tracking positions for project $projectId (domain: $domain, key: '$keyPhrase')")
    val maskedDomain = s"*.$domain/*"
    val phraseEnc = URLEncoder.encode(keyPhrase, "UTF-8")
    val url = s"http://api.semrush.com/reports/v1/projects/$projectId/tracking/?key=$ApiKey&action=report&type=tracking_position_organic&display_limit=10&display_offset=0&display_sort=20150721_asc&display_filter=%2B|Ph|Co|$phraseEnc&url=$maskedDomain"
    Try(Await.result(httpClient.get(url), reqTimeout)).flatten.flatMap { case respBody =>
      val respJson = Json.parse(respBody)
      if ((respJson \ "data").asOpt[JsValue].isEmpty) {
        Success(None)
      } else {
        val fi = respJson \ "data" \ "0" \ "Fi"
        fi.asOpt[String] match {
          case Some(_) => Success(None)
          case None =>
            (fi \ maskedDomain).asOpt[String] match {
              case Some(posStr) =>
                val position = posStr.toLong
                log.info(s"Found position for project $projectId : $position")
                if (position > 0)
                  Success(Some(position))
                else
                  Success(None)
              case None =>
                log.error(s"No data\\0\\Fi\\$maskedDomain value in the Semrush server response: $respBody")
                Failure(new Exception(s"No data\\0\\Fi\\$maskedDomain value in the Semrush server response: $respBody"))
            }
        }
      }
    }
  }


  /**
   * Make simple phrase_organic request, and if it's failed with NOTHING_FOUND
   * (which is Success(None) result of a client function) then start a new project
   */
  def positionCascade(contractId: String, domain: String, keyPhrase: String, geo: String): Try[Option[Long]] = {
    phraseOrganic(domain, keyPhrase, geo) match {
      case Success(s: Some[Long]) => Success(s)

      case Success(None) =>
        projectsStorage.projectsByContractId(contractId).find(_.key == keyPhrase) match {
          case Some(sp) =>
            projectKeyTracking(sp.projectId, domain, keyPhrase) match {
              case Success(opt: Option[Long]) => Success(opt)
              case Failure(t) =>
                val msg = s"SemRush API - An Error during project tracking(project ${sp.projectId}: "
                log.error(msg, t)
                AppRegistry.monitoring ! ExternalError(s"$msg ${t.getMessage}")
                Failure(t)
            }

          case None =>
            makeProject(contractId, domain, keyPhrase).flatMap { case prjId =>
              enableTracking(prjId, domain, geo)
                .map(_ => prjId)
                .recoverWith { case t =>
                  deleteProject(prjId)
                  Failure(t)
                }
            } match {
              case Success(prjId) =>
                log.info(s"Semrush project $prjId created successfully")
                val sp = SemrushProject(prjId, contractId, keyPhrase)
                projectsStorage.addSemrushProject(sp)
                log.info(s"Semrush project $prjId stored into the database")
                Success(None)
              case Failure(t) =>
                val msg = "Cant' create project with SemRush API"
                log.error(msg, t)
                AppRegistry.monitoring ! ExternalError(s"$msg: ${t.getMessage}")
                Failure(t)
            }
        }

      case Failure(t) =>
        log.error("Semrush error ", t)
        AppRegistry.monitoring ! ExternalError(s"Semrush error: ${t.getMessage}")
        Failure(t)
    }
  }
}