/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.bitcoin

import org.joda.time.DateTime
import play.api.libs.json.{JsArray, Json}
import smartcontract.bootstrap.VerificationServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.util.Try


protected[bitcoin] object ChainComBitcoinTracker extends BitcoinTracker with ChainAuthData {

  private val lastBlockUrl = s"$baseUrl/blocks/latest?$apiKey"

  private def transactionsUrl(address: String) = s"$baseUrl/addresses/$address/transactions?limit=200&$apiKey"

  override protected[bitcoin] def fetchLastBlockHeight(): Future[Try[Int]] =
    httpClient.get(lastBlockUrl).map(_.flatMap { response =>
      Try((Json.parse(response) \ "height").as[Int])
    })

  override protected[bitcoin] def fetchReceived(receiver: String,
                                                senderOpt: Option[String],
                                                startTime: Long,
                                                confirmationsNeeded: Int): Try[Map[BitcoinTransactionId, Long]] = {
    val url = transactionsUrl(receiver)

    val futRes = httpClient.get(url).map(_.flatMap { response =>
      Try {
        val transactions = Json.parse(response).as[JsArray].value

        val confirmedTransactions = transactions.flatMap { js =>
          (js \ "block_time").asOpt[String].map(_ => js)
        }

        confirmedTransactions.flatMap { txJs =>
          val cs = (txJs \ "confirmations").as[Int]
          val blockTime = DateTime.parse((txJs \ "block_time").as[String]).getMillis
          val hash = (txJs \ "hash").as[String]

          cs >= confirmationsNeeded && blockTime >= startTime match {
            case true =>
              val outputs = txJs \ "outputs"
              val inputs = txJs \ "inputs"


              val zippedOutputs = (outputs \\ "value").map(_.as[Long]).zip((outputs \\ "addresses").map(_.as[Seq[String]]))
              val receiverOutputs = zippedOutputs.filter { o =>
                val outputCheck = o._2.size == 1 && o._2.head == receiver
                if (outputCheck && senderOpt.isDefined) {
                  (inputs \\ "addresses").flatMap(_.as[Seq[String]]).contains(senderOpt.get)
                } else outputCheck
              }

              val zippedInputs = (inputs \\ "value").map(_.as[Long]).zip((inputs \\ "addresses").map(_.as[Seq[String]]))
              val receiverInputs = zippedInputs.filter(_._2.contains(receiver))

              println("tx: "+txJs)
              println("receiverInputs: "+receiverInputs+" receiverOuputs: "+receiverOutputs)
              //a transaction is counted only if an address is listed in outputs and not listed in inputs
              (receiverInputs.isEmpty, receiverOutputs.isEmpty) match {
                case (true, false) =>
                  Some(hash -> receiverOutputs.map(_._1).sum)
                case _ => None
              }
            case false => None
          }
        }.toMap
      }
    })
    Await.result(futRes, VerificationServerConfig.getDuration("chain-dot-com-timeout"))
  }
}
