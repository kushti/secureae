/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.bitcoin

import smartcontract.AppRegistry
import smartcontract.bootstrap.VerificationServerConfig
import VerificationServerConfig.{isChainComEnabled, isToshiEnabled}
import smartcontract.actors.monitoring.InternalError
import smartcontract.utils.Logging

import scala.concurrent.Future
import scala.util.{Failure, Random, Success, Try}


object BitcoinTrackerRouter extends BitcoinTracker with Logging {

  lazy val tracker = (isChainComEnabled, isToshiEnabled) match {
    case (true, true) =>
      if (Random.nextBoolean()) ToshiBitcoinTracker else ChainComBitcoinTracker
    case (true, false) =>
      ChainComBitcoinTracker
    case (false, true) =>
      ToshiBitcoinTracker
    case (false, false) =>
      AppRegistry.monitoring ! InternalError("Wrong config: both chain.com & toshi disabled")
      throw new RuntimeException("Wrong config: both chain.com & toshi disabled")
  }


  override protected[bitcoin] def fetchLastBlockHeight(): Future[Try[Int]] = tracker.fetchLastBlockHeight()

  override protected[bitcoin] def fetchReceived(receiver: String,
                                                senderOpt: Option[String],
                                                startTime: Long,
                                                confirmationsNeeded: Int): Try[Map[BitcoinTransactionId, Long]] = {
    val res = tracker.fetchReceived(receiver, senderOpt, startTime, confirmationsNeeded)
    res match {
      case Success(mp) => log.debug(s"Got successful response from bitcoin tracker: $mp")
      case Failure(e) => log.warn(s"Got error from bitcoin tracker: $e")
    }
    res
  }
}