/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.bitcoin

import org.joda.time.DateTime
import play.api.libs.json.{JsArray, Json}
import smartcontract.bootstrap.VerificationServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.util.{Random, Try}


protected[bitcoin] object ToshiBitcoinTracker extends BitcoinTracker {
  lazy val toshiNodes = VerificationServerConfig.toshiNodes.toList
  lazy val toshiNodesCount = toshiNodes.size

  lazy val addressesSuffix = "smartcontract/api/v0/addresses/"
  lazy val latestBlockSuffix = "smartcontract/api/v0/blocks/latest"

  def nodeUrl() = {
    val idx = Random.nextInt(toshiNodesCount)
    toshiNodes(idx)
  }

  def transactionsUrl(hash: String) = s"${nodeUrl()}$addressesSuffix$hash/transactions"

  def lastBlockUrl() = s"${nodeUrl()}$latestBlockSuffix"

  override protected[bitcoin] def fetchLastBlockHeight(): Future[Try[Int]] =
    httpClient.get(lastBlockUrl()).map(_.flatMap { response =>
      Try {
        val js = Json.parse(response)
        (js \ "height").as[Int]
      }
    })

  override protected[bitcoin] def fetchReceived(receiver: String,
                                                senderOpt: Option[String],
                                                startTime: Long,
                                                confirmationsNeeded: Int): Try[Map[BitcoinTransactionId, Long]] = {
    val url = transactionsUrl(receiver)

    val fres = httpClient.get(url).map(_.flatMap { response =>
      Try {
        val js = Json.parse(response)
        val transactions = (js \ "transactions").as[JsArray].value

        transactions.flatMap { txJs =>
          val cs = (txJs \ "confirmations").as[Int]
          val blockTime = DateTime.parse((txJs \ "block_time").as[String]).getMillis
          val hash = (txJs \ "hash").as[String]

          cs >= confirmationsNeeded && blockTime >= startTime match {
            case true =>
              val outputs = txJs \ "outputs"
              val inputs = txJs \ "inputs"
              val zippedOutputs = (outputs \\ "amount").map(_.as[Long]).zip((outputs \\ "addresses").map(_.as[Seq[String]]))
              val filteredOutputs = zippedOutputs.filter { o =>
                val outputCheck = o._2.size == 1 && o._2.head == receiver
                if (outputCheck && senderOpt.isDefined) {
                  (inputs \\ "addresses").flatMap(_.as[Seq[String]]).contains(senderOpt.get)
                } else outputCheck
              }

              filteredOutputs.isEmpty match {
                case true => None
                case false => Some(hash -> filteredOutputs.map(_._1).sum)
              }
            case false => None
          }
        }.toMap
      }
    })
    Await.result(fres, VerificationServerConfig.getDuration("toshi-node-timeout"))
  }
}