/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.bitcoin

import smartcontract.AppRegistry
import smartcontract.api.HttpClient
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.utils.Logging

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

trait BitcoinTracker extends Logging {
  type BitcoinAddress = String
  type BitcoinTransactionId = String
  type TimeStamp = Long

  private implicit val asys = AppRegistry.system
  private implicit val monitoringActor = AppRegistry.monitoring

  lazy protected val httpClient = new HttpClient()

  private var cachedLastHeight = 0
  private var heightCachingTime: Long = 0

  private val CachedQueriesMaxSize = 5000
  private lazy val cachedQueries = TrieMap[String, Map[BitcoinTransactionId, Long]]()

  def lastBlockHeight(): Int = {
    val interval = VerificationServerConfig.getDuration("btc-height-check-interval").toMillis
    if (System.currentTimeMillis() - heightCachingTime > interval) {
      this.synchronized {
        heightCachingTime = System.currentTimeMillis()
        fetchLastBlockHeight().map { heightTry =>
          heightTry match {
            case Success(height) =>
              cachedLastHeight = height
              log.debug(s"Bitcoin chain height updated: $cachedLastHeight")
            case Failure(e) =>
              log.warn(s"Bitcoin chain height updating failed", new Exception(e))
          }
        }
      }
    }
    cachedLastHeight
  }

  def received(receiver: BitcoinAddress,
               senderOpt: Option[BitcoinAddress],
               startTime: TimeStamp,
               confirmationsNeeded: Int): Try[Map[BitcoinTransactionId, Long]] = {

    val key = lastBlockHeight().toString + "-" + receiver + "-" + startTime + "-" + confirmationsNeeded
    cachedQueries.get(key) match {
      case Some(res) =>
        log.debug(s"Got Bitcoin data for $receiver from cache $res")
        Success(res)
      case None =>
        Try(fetchReceived(receiver, senderOpt, startTime, confirmationsNeeded)).flatten.map { res =>
          log.debug(s"Got Bitcoin data for $receiver from remote node $res")
          if (cachedQueries.size >= CachedQueriesMaxSize) {
            cachedQueries.remove(cachedQueries.head.toString())
          }
          cachedQueries.put(key, res)
          log.debug(s"Put Bitcoin data for $receiver into cache $res")
          res
        }.recoverWith{case t =>
            log.error(s"Error while fetching Bitcoin data for $receiver: ", t)
            Failure(t)
        }
    }
  }

  protected[bitcoin] def fetchLastBlockHeight(): Future[Try[Int]]

  protected[bitcoin] def fetchReceived(receiver: String,
                                       senderOpt: Option[String],
                                       startTime: Long,
                                       confirmationsNeeded: Int): Try[Map[String, Long]]
}
