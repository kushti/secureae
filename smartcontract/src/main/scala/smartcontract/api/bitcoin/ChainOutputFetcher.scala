/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api.bitcoin

import org.bitcoinj.core.{Utils, Sha256Hash}
import org.bitcoinj.script.Script
import play.api.libs.json.{JsArray, Json}
import smartcontract.AppRegistry
import smartcontract.api.HttpClient

import scala.concurrent.Await
import scala.util.Try
import scala.concurrent.duration._


object ChainOutputFetcher extends ChainAuthData {

  lazy implicit val system = AppRegistry.system
  lazy implicit val monitoring = AppRegistry.monitoring
  lazy protected val httpClient = new HttpClient()

  def getOutScript(txHash: Sha256Hash, outputIdx: Long): Try[Script] = Try {
    val txUrl = s"$baseUrl/transactions/$txHash?$apiKey"
    Await.result(httpClient.get(txUrl), 30.seconds).map { respBody =>
      val outputsJs = (Json.parse(respBody) \ "outputs").as[JsArray]
      val scriptHex = (outputsJs \\ "output_index").zip(outputsJs \\ "script_hex")
        .find(_._1.as[Long] == outputIdx)
        .get
        ._2
        .as[String]

      new Script(Utils.HEX.decode(scriptHex))
    }
  }.flatten

  def main(args: Array[String]): Unit = {
    val out = getOutScript(Sha256Hash.wrap("0f40015ddbb8a05e26bbacfb70b6074daa1990b813ba9bc70b7ac5b0b6ee2c45"), 0)
    println(out)
  }
}
