/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract

import akka.actor.{ActorSystem, Props}
import smartcontract.actors.{UpdatePublisher, ContractsTracker, ContractExecutionAgent}
import smartcontract.actors.p2p.NetworkController
import smartcontract.bitcoin.{BitcoinNodeMode, BitcoinSigningUtils}
import smartcontract.bootstrap.{GlobalActors, VerificationServerEnvironment}
import smartcontract.db.implementation.postgresql._


object AppRegistry extends GlobalActors {
  //System-wide actor refs
  lazy override implicit val system = ActorSystem("SaeActorSystem")

  lazy val networkController = system.actorOf(Props[NetworkController])

  private lazy val executingAgent = system.actorOf(Props(classOf[ContractExecutionAgent], publishingAgent))
  lazy val contractsTracker = system.actorOf(Props(classOf[ContractsTracker], executingAgent))

  private lazy val upProps = Props(UpdatePublisher(chainPublishing = true,
    frontendNotifying = true,
    notifyActor = false))
  private lazy val publishingAgent = system.actorOf(upProps)

  //Bitcoin signer
  lazy val bitcoinSigner = BitcoinSigningUtils(BitcoinNodeMode.SPV_MAINNET, startP2P = true)

  //storages
  object StoragesImpl {
    private lazy val test = VerificationServerEnvironment.chosenEnvironment() == VerificationServerEnvironment.LocalTestnet

    val contracts = if (test) ContractStorageTest else ContractStorageProduction
    val payments = if (test) PaymentsStorageTest else PaymentsStorageProduction
    val transactions = if (test) BitcoinTransactionsStorageTest else BitcoinTransactionsStorageProduction
    val updates = if (test) ContractUpdateStorageTest else ContractUpdateStorageProduction
    val semrushProjects = if (test) SemrushProjectStorageTest else SemrushProjectStorageProduction
    val contractDistribEvents = if (test) ContractDistributionEventStorageTest else ContractDistributionEventStorageProduction
  }
}