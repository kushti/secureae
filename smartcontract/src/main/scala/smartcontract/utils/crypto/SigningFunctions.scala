/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.utils.crypto

import nxt.crypto.Crypto
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.utils.Logging
import scala.util.{Success, Failure, Try}


object SigningFunctions extends Logging {
  lazy val phrase = VerificationServerConfig.blockchainDbWorkerPhrase
  lazy val ownPublicKey:Array[Byte] = Crypto.getPublicKey(phrase)
  val SignatureSize = 64

  def sign(message:Array[Byte]):Array[Byte] = Crypto.sign(message, phrase)

  def verify(signature: Array[Byte], message: Array[Byte]):Boolean =
    verify(signature, message, ownPublicKey)

  def verify(signature: Array[Byte], message: Array[Byte], publicKey:Array[Byte]):Boolean = Try {
    require(signature.length == SignatureSize)
    Crypto.verify(signature, message, publicKey, false)
  } match {
    case Success(res) => res
    case Failure(e) => log.warn("Error while verifying signature",e); false
  }
}