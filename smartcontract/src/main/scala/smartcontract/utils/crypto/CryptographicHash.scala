/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.utils.crypto

import java.security.MessageDigest
import smartcontract.utils.EncodingUtils
import EncodingUtils._


object CryptographicHash {
  def sha256(input: Array[Byte]): Array[Byte] = MessageDigest.getInstance("SHA-256").digest(input)

  def sha256HexEncoded(input: String): String = bytes2hex(sha256(input.getBytes))

  def doubleSha256(input: Array[Byte]): Array[Byte] = sha256(sha256(input))

  def doubleSha256HexEncoded(input: String): String = bytes2hex(doubleSha256(input.getBytes))
}
