/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.utils.crypto

import org.bouncycastle.crypto.digests.SHA256Digest
import org.bouncycastle.crypto.engines.AESEngine
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator
import org.bouncycastle.crypto.modes.CBCBlockCipher
import org.bouncycastle.crypto.paddings.{PKCS7Padding, PaddedBufferedBlockCipher}
import org.bouncycastle.crypto.params.ParametersWithIV
import smartcontract.utils.EncodingUtils
import scala.util.{Random, Try}


object PasswordBasedCrypto {

  import EncodingUtils._

  val PassLength = 32
  val SaltLength = 16
  val IvLength = 16

  val totalEncodedPassLength = PassLength + SaltLength + IvLength
  private val IterationsCount = 20000


  private def cipherData(cipher: PaddedBufferedBlockCipher, data: Array[Byte]): Array[Byte] = {
    val minSize = cipher.getOutputSize(data.length)
    val outBuf = new Array[Byte](minSize)
    val length1 = cipher.processBytes(data, 0, data.length, outBuf, 0)
    val length2 = cipher.doFinal(outBuf, length1)
    val actualLength = length1 + length2
    val result = new Array[Byte](actualLength)
    System.arraycopy(outBuf, 0, result, 0, result.length)
    result
  }

  def splitBase64Password(base64: String): (Array[Byte], Array[Byte], Array[Byte]) = {
    val bytes = base64bytes(base64).ensuring(_.length == totalEncodedPassLength)
    
    (bytes.take(PassLength),
      bytes.takeRight(SaltLength + IvLength).take(SaltLength),
      bytes.takeRight(SaltLength + IvLength).takeRight(IvLength))
  }

  def generateEncodedPass(): Array[Byte] =
    new Random().alphanumeric.take(PassLength + SaltLength + IvLength).map(_.toByte).toArray

  def generatePassSaltIv(): (Array[Byte], Array[Byte], Array[Byte]) = 
    splitBase64Password(bytes2base64(generateEncodedPass()))

  private def cipher(encrypt: Boolean,
                     password: Array[Byte],
                     salt: Array[Byte],
                     iv: Array[Byte],
                     iterationCount: Int): PaddedBufferedBlockCipher = {
    val pGen = new PKCS5S2ParametersGenerator(new SHA256Digest())

    pGen.init(password, salt, iterationCount)
    val aesCBC = new CBCBlockCipher(new AESEngine())
    val aesCBCParams = pGen.generateDerivedParameters(256)

    val paramsiv = new ParametersWithIV(aesCBCParams, iv)

    aesCBC.init(encrypt, paramsiv)

    val cipher = new PaddedBufferedBlockCipher(aesCBC, new PKCS7Padding())
    cipher.init(encrypt, paramsiv)
    cipher
  }


  def encrypt(msg: Array[Byte],
              password: Array[Byte],
              salt: Array[Byte],
              iv: Array[Byte],
              iterationCount: Int) =
    cipherData(cipher(encrypt = true, password, salt, iv, iterationCount), msg)

  def decrypt(encrypted: Array[Byte],
              password: Array[Byte],
              salt: Array[Byte],
              iv: Array[Byte],
              iterationCount: Int) =
    cipherData(cipher(encrypt = false, password, salt, iv, iterationCount), encrypted)

  def encrypt(plainText: String, base64Password: String): Try[Array[Byte]] = Try {
    val (pass, salt, iv) = splitBase64Password(base64Password)

    require(pass.length == PassLength)
    require(salt.length == SaltLength)
    require(iv.length == IvLength)

    encrypt(plainText.getBytes("UTF-8"), pass, salt, iv, IterationsCount)
  }

  def encryptEncode(plainText: String, password: String): Try[String] =
    encrypt(plainText, password).map(EncodingUtils.bytes2base64)

  def decodeDecrypt(hexEncText: String, password: String): Try[String] = Try {
    val (pass, salt, iv) = splitBase64Password(password)

    require(pass.length == PassLength)
    require(salt.length == SaltLength)
    require(iv.length == IvLength)

    val msg = EncodingUtils.base64bytes(hexEncText)
    new String(decrypt(msg, pass, salt, iv, IterationsCount))
  }
}