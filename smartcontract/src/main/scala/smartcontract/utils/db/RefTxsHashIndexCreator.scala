/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.utils.db

import nxt.Db
import nxt.db.DbUtils
import smartcontract.utils.Logging
import scala.util.{Failure, Success, Try}

object RefTxsHashIndexCreator extends Logging {
  def applyUpdate(): Unit = {
    /*
      from h2 documentation:
      "Please note that using an index on randomly generated data will result on poor performance
      once there are millions of rows in a table. The reason is that the cache behavior is very bad
      with randomly distributed data. This is a problem for any database system."

      So it's better in future to get transaction id from hash (if possible) and build fast
       key -> value (transaction_id -> message_text) cache with MapDb

      But current approach is okay with < 100K or even 1M contracts
    */

    apply("CREATE INDEX IF NOT EXISTS ref_tx_hash on transaction(referenced_transaction_full_hash)")
  }

  private def apply(sql: String): Unit = {
    val db = Db.db

    val csTry = Try {
      val con = db.getConnection
      val stmt = con.createStatement
      (con, stmt)
    }

    csTry match {
      case Success((con, stmt)) =>
        try {
          if (sql != null) {
            log.debug("Will apply sql:\n" + sql)
            stmt.executeUpdate(sql)
          }
          con.commit()
        } catch {
          case e: Exception =>
            DbUtils.rollback(con)
            throw e
        }
        DbUtils.close(stmt, con)

      case Failure(e) =>
        log.error("Database error executing " + sql, new Exception(e))
        DbUtils.close(null, null)
    }
  }
}