/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors

import akka.actor.{Actor, ActorRef}
import play.api.libs.json.{JsObject, Json}
import smartcontract.AppRegistry
import smartcontract.actors.monitoring.InternalError
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.db.implementation.postgresql.BitcoinTransactionsStorageProduction
import smartcontract.db.interfaces.{ActionId, GlobalTermId}
import smartcontract.model.{SmartContract, SmartContractUpdaterImpl, SmartTermsSequence, TermState}
import smartcontract.utils.{EncodingUtils, Logging}

import scala.util.{Failure, Success}


case class TrackContract(contract: SmartContract)

case class PublishUpdate(actorToNotify: ActorRef,
                         json: JsObject,
                         contract: SmartContract,
                         sq: SmartTermsSequence,
                         st: TermState)


abstract class AbstractContractExecutionAgent(publisher: ActorRef)
  extends Actor with Logging {

  import AppRegistry.bitcoinSigner

  protected def trackTerms(contract: SmartContract): Option[(SmartTermsSequence, TermState)]

  override def receive = {
    case TrackContract(contract) =>
      val id = contract.id
      log.debug(s"Tracking terms for contract: $id")
      val updatedTermOpt = trackTerms(contract)

      log.debug("Updated term:" + updatedTermOpt + " for contract  " + id)
      if (updatedTermOpt.isEmpty) {
        log.debug("State remains the same for all terms of contract: " + id)
      } else {
        log.debug("State changed for a term of the contract: " + id)

        updatedTermOpt.foreach { case (sq, st) =>

          val success = (sq.isSuccess(st), sq.isFailed(st)) match {
            case (true, false) => true
            case (false, true) => false
            case _ =>
              log.error("Intermediate states are not supported atm")
              throw new NotImplementedError("Intermediate states are not supported atm")
          }

          //json construction to send to the frontend
          val actionId = ActionId(GlobalTermId(contract.id, sq.name), success)

          val jsWithoutTx = Json.obj("term" -> sq.name, "status" -> st.name)

          val statusJsObj = BitcoinTransactionsStorageProduction.getTransaction(actionId).map { tx =>
            bitcoinSigner.signatures(tx) match {
              case Success(sigs) =>
                val sigsHex = sigs.map(EncodingUtils.bytes2hex)
                jsWithoutTx ++ Json.obj("signatures" -> sigsHex)

              case Failure(t) =>
                AppRegistry.monitoring ! InternalError("Cant' sign transaction, check logs: " + t.getMessage)
                log.error("Cant' sign transaction: ", t)
                jsWithoutTx
            }
          }.getOrElse(jsWithoutTx)

          val updatedContract = SmartContractUpdaterImpl.termUpdate(contract, sq.name, st, System.currentTimeMillis())

          val js = Json.obj("source" -> "smartcontract.com",
            "type" -> "statechange",
            "node_id" -> VerificationServerConfig.nodeId,
            "contract" -> id,
            "completeness" -> updatedContract.completeness) ++ statusJsObj

          publisher ! PublishUpdate(sender(), js, updatedContract, sq, st)
        }
      }
  }
}


class ContractExecutionAgent(publisher: ActorRef)
  extends AbstractContractExecutionAgent(publisher) {

  override protected def trackTerms(contract: SmartContract): Option[(SmartTermsSequence, TermState)] =
    SmartContractUpdaterImpl.trackTerms(contract)
}

class ContractExecutionAgentStub(publisher: ActorRef, termName: String, success: Boolean)
  extends AbstractContractExecutionAgent(publisher) {

  override def trackTerms(contract: SmartContract): Option[(SmartTermsSequence, TermState)] =
    contract.sequences.find(_._1.name == termName).map { case (sq, _) =>
      sq -> SmartTermsSequence.stateFromBoolean(success)
    }
}