/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors

import akka.actor.Actor.Receive
import akka.util.ByteString
import play.api.libs.json.JsObject
import smartcontract.AppRegistry
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.actors.monitoring.InternalError
import smartcontract.actors.p2p._
import smartcontract.actors.p2p.message._
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.model.blockchain.ContractsBlockchainDb
import smartcontract.model.{ContractParameters, JsonToContract, SmartContract, SmartContractTrackingState}
import smartcontract.utils.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}


/**
 * Helper trait to be mixed to a class implementing an incoming contract handling
 * functionality
 */

trait IncomingContractHandler extends Logging {

  val AckWaitingTime = 2.minutes

  lazy val ownPK: Array[Byte] = VerificationServerConfig.blockchainDbWorker.getPublicKey

  protected lazy val peerDb: PeerDatabase = PeerDatabaseImpl

  private lazy val contractStorage = StoragesImpl.contracts
  private lazy val eventStorage = StoragesImpl.contractDistribEvents

  /**
   * A contract could be passed to the node in one of two possible ways:
   *
   * 1. It could comes from API call, so being passed by an user into the system. In this case
   * the node becomes Master in contract distribution process
   *
   * 2. It also could comes from P2P network. In this case we are the node is mentioned further
   * as Slave
   *
   * An incoming contract handling algo:
   *
   * 1. Parse json coming as string, check mandatory fields presence,
   * check whether incoming Bitcoin transactions are signable
   *
   * 2. Set PENDING state to contract. Write a contract to a database
   *
   * If a node is a master:
   *
   * 3a. Send out a contract to other peers. For now master node sends out a
   * contract to all the peers connected.
   *
   * TODO: include nodes to run a contract into a contract JSON, implement corresponding checks
   * TODO: make UI to choose the nodes running a contract
   *
   * 3b. Listen for incoming Contract Ack messages. Write "Master Got An Ack"(with a contract Id &
   * sender's node ID) event for an Ack coming into the database.
   *
   * 3c. Two minutes after sending out a contract, if enough number of acks being received,
   * try to publish a contract into the blockchain. If publishing transaction is successful,
   * set TRACKING state to a contract and start it's tracking. If any failure happens for any of both cases, send out Contract Abort message and
   * set contract's state to ABORT.
   *
   * TODO: quorum params after 3a TODO implementation. For now one vote is enough to start contract tracking
   *
   * If a node is a slave:
   *
   * 3a. Send out contract acknowledgement to master
   * 3b. Listen to blockhain, when contract is there, set contract state to TRACKING and start it's tracking
   * 3c. Otherwise, if a Contract Abort message has been received, set ABORT state to a contract.
   *
   *
   * @param contractText - contract JSON in a form of plain UTF-8 string
   * @param master - whether the node is Master or Slave(external context determines that)
   *
   * @return a contract wrapped into Try monad, so whether Success(contract) or Failure(exception)
   */

  def handleIncomingContract(contractText: String, master: Boolean): Try[SmartContract] = Try {
    ContractParameters
      .parse(contractText)
      .flatMap(params => JsonToContract(params.contractJson, params, SmartContractTrackingState.PENDING))
      .flatMap { contract =>

        val contractId = contract.id

        val termNames = contract.sequences.flatMap(_._1.terms).map(_.name).toSet
        val outcomesJsOpt = contract.contractParams.outcomesJsonOpt.map(_.as[JsObject])
        val outcomes = JsonToContract.parseOutcomes(contractId, termNames, outcomesJsOpt).getOrElse(Map())

        val txsSignable = outcomes.forall { case (_, tx) => AppRegistry.bitcoinSigner.isSignable(tx) }

        if (txsSignable) {
          outcomes.foreach(StoragesImpl.transactions.putTransaction)

          assert(contract.trackingState == SmartContractTrackingState.PENDING,
            s"The contract isn't in a pending state though it should be so: $contractId")

          log.info(s"Writing a contract into internal db: $contractId")
          StoragesImpl.contracts.addContract(contract)

          if (master) {
            dumpEvent(contractId, ContractDistributionEventType.MasterGotAContract)
            sendContractOut(contractId, contractText)

            AppRegistry.system.scheduler.scheduleOnce(AckWaitingTime) {
              val contractParams = contract.contractParams
              val ackTypeId = ContractDistributionEventType.MasterGotAnAck.id.toByte
              val acksGot = eventStorage.eventsByContractAndEventType(contractId, ackTypeId).size
              acksGot >= VerificationServerConfig.acksToStart match {
                case true =>
                  //publish the contract into the blockchain
                  log.info(s"Going to publish a contract into blockchain: ${contract.name} \n $contract")
                  ContractsBlockchainDb.publishContractText(contract.id, contractParams) match {
                    case Success(_) =>
                      dumpEvent(contractId, ContractDistributionEventType.MasterPublished)
                      contractStorage.updateContract(contract.copy(trackingState = SmartContractTrackingState.TRACKING))
                      log.info("contract written to the blockchain. Going to track it: " + contract.id)

                    case Failure(e) =>
                      abortContractMaster(contract)
                      val msg = s"Can't write message into the blockchain: '${contractParams.blockchainJson}', error is ${e.getMessage}"
                      AppRegistry.monitoring ! InternalError(msg)
                      log.warn("Can't write message into the blockchain", new Exception(e))
                  }

                case false =>
                  abortContractMaster(contract)
              }
            }
          } else {
            dumpEvent(contractId, ContractDistributionEventType.SlaveGotAContract)
          }

          //todo: frontend notification ? (or in case of failure?)
          Success(contract)
        } else {
          Failure(new Exception("A Bitcoin transaction is not signable"))
        }
      }
  }.flatten

  /**
   * Incoming messages handling for a slave peer of the contract distribution process
   * @return partial function of Receive type to be incorporated into an actor's `receive`
   *         partial function
   */
  def slaveReceive: Receive = {
    case cm: ContractMessage =>
      handleIncomingContract(cm.contractText, master = false) match {
        case Success(contract) =>
          log.info(s"Got the contract(id: ${contract.id}}) via P2P layer!")

          peerDb.actorForPubKey(cm.senderPubkey) match {
            case Some(actor) =>
              log.info(s"Sending ACK back to ${cm.senderPubkey.mkString}")
              actor ! MessageProducer.contractAck(contract.id)
            case None =>
              log.error(s"Cant' send Ack back to ${cm.senderPubkey.mkString}")
            //todo: implement handling
          }

        case Failure(t) =>
          log.error("Got wrong contract via P2P layer: ", t)
      }

    case cam: ContractAbortMessage =>
      contractStorage.getContract(cam.contractId) match {
        case Some(contract) =>
          dumpEvent(cam.contractId, ContractDistributionEventType.SlaveAbort)
          val updContract = contract.copy(trackingState = SmartContractTrackingState.ABORTED)
          contractStorage.updateContract(updContract)
        case None =>
          val msg = s"Got abort req for the contract not living in the database ${cam.contractId}"
          AppRegistry.monitoring ! InternalError(msg)
          log.error(msg)
      }
  }

  /**
   * Incoming messages handling for a master peer of the contract distribution process
   * @return partial function of Receive type to be incorporated into an actor's `receive`
   *         partial function
   */
  def masterReceive: Receive = {
    case ack: ContractAckMessage =>
      dumpEvent(ack.contractId, ContractDistributionEventType.MasterGotAnAck)
  }

  def sendOut(msg: Message): Unit = {
    val connectedPeers = peerDb.allConnectedPeers()
    connectedPeers.foreach { case pd =>
      pd.handler ! ByteString(msg.bytes)
    }
  }

  def sendContractOut(contractId: String, contractText: String): Unit = {
    log.info(s"SendContractOut($contractId) is called")

    //todo: not all the connected peers should got a contract
    sendOut(MessageProducer.contract(contractText))
  }

  def sendContractAbortOut(contractId: String): Unit = {
    log.info(s"SendContractAbortOut($contractId) is called")

    //todo: not all the connected peers should got a contract abort
    sendOut(MessageProducer.contractAbort(contractId))
  }


  private def abortContractMaster(contract: SmartContract): Unit = {
    dumpEvent(contract.id, ContractDistributionEventType.MasterAbort)
    contractStorage.updateContract(contract.copy(trackingState = SmartContractTrackingState.ABORTED))
    sendContractAbortOut(contract.id)
  }

  private def dumpEvent(contractId: SmartContract.ContractId,
                        eventType: ContractDistributionEventType.Value) =
    eventStorage.addEvent(ContractDistributionEvent(contractId,
      ownPK,
      eventType,
      System.currentTimeMillis()
    ))
}