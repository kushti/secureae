/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors

import akka.actor.Actor
import play.api.libs.json.{JsObject, Json}
import smartcontract.AppRegistry
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.actors.monitoring.InternalError
import smartcontract.api.HttpClient
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.model.blockchain.ContractsBlockchainDb
import smartcontract.model.{ContractPrivacyOptions, SmartContract, SmartTermsSequence, TermState}
import smartcontract.utils.Logging
import smartcontract.utils.crypto.{CryptographicHash, PasswordBasedCrypto}

import scala.util.{Failure, Success, Try}

case class UpdatePublisher(chainPublishing: Boolean,
                           frontendNotifying: Boolean,
                           notifyActor: Boolean)
  extends Actor with Logging {

  implicit val system = context.system
  implicit val monitoringActor = AppRegistry.monitoring

  private lazy val webClient = new HttpClient()

  override def receive = {
    case PublishUpdate(actorToNotify, js, updatedContract, sq, st) =>
      log.info(s"Going to publish status update: $js")
      publishToChain(js, updatedContract) match {
        case Success(_) =>

          writeUpdatesToDb(updatedContract.id, (sq, st))

          //saving updated contract to the database
          StoragesImpl.contracts.updateContract(updatedContract)

          //sending status update to the frontend
          val fm = Json.obj("status_update" -> js).toString()
          log.info(s"Message to frontend: $fm")

          if (frontendNotifying) {
            webClient.post(VerificationServerConfig.frontAppPath + "api/contracts", fm)
          }

          if (notifyActor) {
            actorToNotify ! fm
          }

        case Failure(e) =>
          val msg = s"Can't write update into the blockchain '$js', error is ${e.getMessage}"
          AppRegistry.monitoring ! InternalError(msg)
          e.printStackTrace()
      }
  }

  private def publishToChain(updateJs: JsObject, contract: SmartContract): Try[Long] =
    chainPublishing match {
      case true => realPublishingToChain(updateJs, contract)
      case false => Success(0)
    }

  private def realPublishingToChain(updateJs: JsObject, contract: SmartContract): Try[Long] = Try {
    val updateText = updateJs.toString()

    val statusUpdate = contract.contractParams.privacy match {
      case ContractPrivacyOptions.Public =>
        updateText

      case ContractPrivacyOptions.Private =>
        val password = contract.contractParams.passwordOpt.get
        val encrypted = PasswordBasedCrypto.encryptEncode(updateText, password).get
        val idHash = CryptographicHash.sha256HexEncoded(contract.id)

        Json.obj("source" -> "smartcontract.com",
          "contract-id-hash" -> idHash,
          "update-encrypted" -> encrypted).toString()
    }

    log.info(s"Going to write status update: $statusUpdate for contract: ${contract.id}")
    ContractsBlockchainDb.publishText(statusUpdate)
  }.flatten

  private def writeUpdatesToDb(contractId: String, updatedTerm: (SmartTermsSequence, TermState)) = {
    log.info("Writing to db:" + updatedTerm + " for contract  " + contractId)
    val (sq, st) = (updatedTerm._1, updatedTerm._2)
    //saving update into the SQL database
    StoragesImpl.updates.putStatusUpdate(contractId, sq.name, st.name, System.currentTimeMillis())
  }
}