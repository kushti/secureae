/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors

import akka.actor.{Actor, ActorRef}
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.model.{Query, SmartContract, TermState, Value}
import smartcontract.utils.Logging

import scala.concurrent.ExecutionContext.Implicits.global

case class ChangeState(id: Long, contract: SmartContract, newState: TermState)

case class UpdateContract(updatedContract: SmartContract)

case object CheckContracts


class ContractsTracker(executingAgent: ActorRef) extends Actor with Logging {

  override def receive = {
    case CheckContracts =>
      log.info("Going to check contracts, total amount to check: " + StoragesImpl.contracts.activeSize())
      StoragesImpl.contracts.activeContracts().foreach { contract =>
        log.info("Tracking contract: " + contract.id)
        executingAgent ! TrackContract(contract)
      }
  }
}

case class QueryResult[V <: Value, Q <: Query[V]](contractId: String, termName: String, qr: Q, res: V)