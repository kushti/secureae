/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors

import nxt.utils.TxSeqUtils
import nxt.{Block, BlockchainListener}
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.actors.p2p.{ContractDistributionEvent, ContractDistributionEventType}
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.model.SmartContractTrackingState
import smartcontract.utils.Logging

import scala.collection.JavaConversions._

/**
 * Listener for a node having the slave role
 */

//todo: it just looks for a message of a contract containing id!
//todo: definitely not a 100% correct solution!

object SmartContractSlaveListener extends BlockchainListener with Logging {

  lazy val ownPK: Array[Byte] = VerificationServerConfig.blockchainDbWorker.getPublicKey

  override def notify(b: Block): Unit = {
    val pendingIds = StoragesImpl.contracts.pendingContractIds()
    val blockTransactions = b.getTransactions

    TxSeqUtils.withTextMessage(blockTransactions).foreach { tx =>
      val text = tx.getMessage.toString
      pendingIds.find(pendingId => text.contains()).foreach{pendingId =>

        //slave starts tracking
        StoragesImpl.contractDistribEvents.addEvent(ContractDistributionEvent(pendingId,
          ownPK,
          ContractDistributionEventType.SlaveTracking,
          System.currentTimeMillis()
        ))

        //todo: what if not found
        val contractStorage = StoragesImpl.contracts
        contractStorage.getContract(pendingId).foreach{contract =>
          contractStorage.updateContract(contract.copy(trackingState = SmartContractTrackingState.TRACKING))
        }
      }
    }
  }
}