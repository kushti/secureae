/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.p2p

import java.net.InetSocketAddress

import akka.actor.{Actor, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import nxt.Account
import smartcontract.actors.IncomingContractHandler
import smartcontract.actors.p2p.NetworkController.{PeerDisconnected, ShutdownNetwork}
import smartcontract.actors.p2p.SmartContractsNetworkLogic.CheckPeers
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.utils.Logging

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


/**
 * App-specific logic
 * To be mixed with common application-context free logic
 */

trait SmartContractsNetworkLogic extends Actor with IncomingContractHandler with Logging {
  protected val connectingPeers = TrieMap[Array[Byte], InetSocketAddress]() //todo: shared by both classes, fix

  private implicit val system = context.system

  def init(): Unit = {
    VerificationServerConfig.peers.foreach { case (pk, addr) =>
      log.info("Adding peer on network controller init: " + addr)
      PeerDatabaseImpl.addKnownPeer(pk, addr)
    }

    context.system.scheduler.schedule(3.seconds, 15.seconds)(self ! CheckPeers)
  }

  def appHandler(): Receive = {
    case CheckPeers =>
      log.debug("CheckPeers is called")
      val connectedPeers = PeerDatabaseImpl.allConnectedPeers()
      PeerDatabaseImpl.knownPeers().find { case (pk, _) =>
        !connectedPeers.exists(_.pubKey.getOrElse(Array.fill(32)(0)).sameElements(pk)) &&
          !connectingPeers.keys.exists(_.sameElements(pk))
      }.foreach { case (pk, peer) =>
        log.info("Connecting to: " + peer)
        connectingPeers += pk -> peer
        IO(Tcp) ! Connect(peer) //todo: connection timeout
      }
  }
}

object SmartContractsNetworkLogic {

  case object CheckPeers

}


/**
 * Common network-related logic
 * An instance must be a singleton!
 */

class NetworkController extends Actor with SmartContractsNetworkLogic with Logging {

  lazy val port = VerificationServerConfig.peerPort
  private lazy val bindAddress = VerificationServerConfig.bindAddress
  private implicit val system = context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(bindAddress, port))

  //app-related init
  init()

  override def receive = ({
    case b@Bound(localAddress) =>
      log.info("Successfully bound to the port " + port)

    case CommandFailed(_: Bind) =>
      log.error("Network port " + port + " already in use or other network error!")
      context stop self
    //todo: send a signal out

    case c@Connected(remote, local) =>
      log.info(s"Connected to $remote")

      val connection = sender()
      val props = Props(classOf[PeerConnectionHandler], self, connection, remote)
      val handler = context.actorOf(props)
      connection ! Register(handler)

      val pkOpt = connectingPeers.find { case (_, r) => r == remote }
        .flatMap { case (nxtId, _) => Option(Account.getAccount(nxtId)).map(_.getPublicKey) }
      PeerDatabaseImpl.addConnectedPeer(pkOpt, remote, handler)

    case CommandFailed(c: Connect) =>
      log.info("Failed to connect to : " + c.remoteAddress)
      PeerDatabaseImpl.removeConnectedPeer(c.remoteAddress)

    case CommandFailed(cmd: Tcp.Command) =>
      log.info("Failed to execute command : " + cmd)

    case ShutdownNetwork =>
      log.info("Going to shutdown all connections & unbind port")
      PeerDatabaseImpl.allConnectedPeers()
        .foreach(_.handler ! PeerConnectionHandler.CloseConnection)
      self ! Unbind
      context stop self

    case PeerDisconnected(remote) =>
      PeerDatabaseImpl.removeConnectedPeer(remote)
  }: Receive) orElse appHandler orElse ({
    case nonsense: Any => log.warn(s"Strange input: $nonsense")
  }: Receive)
}

object NetworkController {

  case class PeerDisconnected(address: InetSocketAddress)

  case object ShutdownNetwork

}