/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.p2p

import smartcontract.actors.p2p.ContractDistributionEventType.ContractDistributionEventType
import smartcontract.model.SmartContract

/**
 * Log structure:
 *
 * Events:
 * 1. Master got a contract
 * 2. Slave got a contract
 * 3. Master got an Ack message
 *
 * Then the two branches, a and b
 * 4a. Master sends abort 2 minutes after step 1
 * 4b. Slave got abort message
 *
 * 4b. Master publishes contract to the chain
 * 5b. Slave sees contract published into the chain
 *
 * Each event is to be written into the backend persistent storage
 */

object ContractDistributionEventType extends Enumeration {
  type ContractDistributionEventType = Value

  val MasterGotAContract = Value(1, "Master got a contract")
  val SlaveGotAContract = Value(2, "Slave got a contract")
  val MasterGotAnAck = Value(3, "Master got an Ack message")

  val MasterAbort = Value(4, "Master sends abort 2 minutes after step 1")
  val SlaveAbort = Value(5, "Slave got abort message")

  val MasterPublished = Value(6, "Master publishes contract to the chain")
  val SlaveTracking = Value(7, "Slave sees contract published into the chain")
}


case class ContractDistributionEvent(contractId: SmartContract.ContractId,
                                     nodePubkey: Array[Byte],
                                     eventType: ContractDistributionEventType,
                                     timestamp: Long)