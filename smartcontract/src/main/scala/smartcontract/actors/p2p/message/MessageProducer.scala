/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.p2p.message

import smartcontract.bootstrap.VerificationServerConfig

object MessageProducer {
  private lazy val nxtAcc = VerificationServerConfig.blockchainDbWorker
  private lazy val pk = nxtAcc.getPublicKey

  def ping(): PingMessage = PingMessage(pk, None)

  def contract(text: String) = ContractMessage(text, pk, None)

  def contractAck(contractId: String) = ContractAckMessage(contractId, pk, None)

  def contractAbort(contractId: String) = ContractAbortMessage(contractId, pk, None)
}