/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.p2p.message

import com.google.common.primitives.{Bytes, Ints}
import smartcontract.utils.crypto.{CryptographicHash, SigningFunctions}

import scala.util.{Failure, Success, Try}

class Message(val senderPubkey: Array[Byte],
              val messageType: Byte,
              val dataBytes: Array[Byte],
              val signatureOpt: Option[Array[Byte]]) {

  lazy val typeBytes = Array(messageType)

  lazy val signedMsg = {
    val bytesToSign = Bytes.concat(typeBytes, Ints.toByteArray(dataLength), dataBytes)
    CryptographicHash.doubleSha256(bytesToSign)
  }

  lazy val signature: Array[Byte] = signatureOpt.getOrElse(SigningFunctions.sign(signedMsg))

  lazy val bytes: Array[Byte] =
    Bytes.concat(Message.Magic,
      senderPubkey,
      typeBytes,
      Ints.toByteArray(dataLength),
      dataBytes,
      signature)
  lazy val dataLength = dataBytes.length

  def verify(): Boolean =
    Try(SigningFunctions.verify(signature, signedMsg, senderPubkey))
      .getOrElse(false)

  def concrete(): Try[_ <: Message] = messageType match {
    case PingMessage.TypeVal => Success(PingMessage(senderPubkey, signatureOpt))

    case ContractMessage.TypeVal =>
      val contractText = new String(dataBytes)
      Success(ContractMessage(contractText, senderPubkey, signatureOpt))

    case ContractAckMessage.TypeVal =>
      ContractAckMessage.parseDataBytes(dataBytes).map { case contractId =>
        ContractAckMessage(contractId, senderPubkey, signatureOpt)
      }

    case ContractAbortMessage.TypeVal =>
      val contractId = new String(dataBytes)
      Success(ContractAbortMessage(contractId, senderPubkey, signatureOpt))

    case _ => Failure(new Exception("Wrong message type in concrete()"))
  }
}


object Message {
  val Magic = Array(0x16: Byte, 0x6a: Byte)

  val PubKeyLength = 32

  //if databytes.length == 0
  val MinimumLength = Magic.length + 32 + 1 + 4 + SigningFunctions.SignatureSize

  def parse(bytes: Array[Byte]): Try[Message] = Try {
    require(bytes.length >= MinimumLength)

    var position = 0
    if (!bytes.take(Magic.length).sameElements(Magic)) {
      throw new Exception("Wrong magic!")
    }

    position += Magic.length
    val pk = bytes.slice(position, position + PubKeyLength)
    position += PubKeyLength

    //todo: check pubkey

    val msgType = bytes(position)
    //todo: check message types
    position += 1

    val datalen = Ints.fromByteArray(bytes.slice(position, position + 4))
    if (datalen < 0) throw new Exception("Negative data length!")
    position += 4

    val dataBytes = bytes.slice(position, position + datalen)
    position += datalen

    val signature = bytes.slice(position, position + SigningFunctions.SignatureSize)

    val message = new Message(pk, msgType, dataBytes, Some(signature))
    if (!message.verify()) throw new Exception("Signature is invalid")
    message
  }
}