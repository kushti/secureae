/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.p2p

import java.net.InetSocketAddress
import akka.actor.ActorRef
import scala.collection.concurrent.TrieMap


case class PeerData(pubKey: Option[Array[Byte]], peer: InetSocketAddress, handler: ActorRef)

trait PeerDatabase {
  def addConnectedPeer(pubKey: Option[Array[Byte]], peer: InetSocketAddress, handler: ActorRef): Unit

  def removeConnectedPeer(pubKey: Array[Byte]): Unit

  def removeConnectedPeer(peer: InetSocketAddress): Unit

  def allConnectedPeers(): Seq[PeerData]

  def actorForPubKey(pubKey: Array[Byte]): Option[ActorRef]

  def addKnownPeer(pubKey: Array[Byte], peer: InetSocketAddress)

  def knownPeers(): Map[Array[Byte], InetSocketAddress]
}


//todo: testable design
object PeerDatabaseImpl extends PeerDatabase {
  private val whitelist = TrieMap[Array[Byte], InetSocketAddress]()
  private var connected = Seq[PeerData]()

  override def addConnectedPeer(pubKey: Option[Array[Byte]], peer: InetSocketAddress, handler: ActorRef) =
    connected = connected :+ PeerData(pubKey, peer, handler)

  override def removeConnectedPeer(pubKey: Array[Byte]) =
    connected = connected.filter(pd => !pd.pubKey.getOrElse(Array.fill(32)(0)).sameElements(pubKey))

  override def removeConnectedPeer(peer: InetSocketAddress): Unit =
    connected = connected.filter(_.peer != peer)

  override def allConnectedPeers() = connected

  override def actorForPubKey(pubKey: Array[Byte]): Option[ActorRef] =
    connected
      .find(_.pubKey.getOrElse(Array.fill(32)(0)).sameElements(pubKey))
      .map(_.handler)

  override def addKnownPeer(pubKey: Array[Byte], peer: InetSocketAddress): Unit = whitelist += pubKey -> peer

  override def knownPeers(): Map[Array[Byte], InetSocketAddress] = whitelist.toMap
}