/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.p2p

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef}
import akka.io.Tcp._
import akka.util.ByteString
import nxt.Account
import smartcontract.actors.IncomingContractHandler
import smartcontract.actors.p2p.message.Message
import smartcontract.actors.p2p.message._
import smartcontract.utils.Logging
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success}


trait MessageHandler extends Actor with IncomingContractHandler with Logging {
  def handleMessage: Receive = ({
    case pm: PingMessage => log.debug(s"Got ping message: $pm")
  }:Receive) orElse slaveReceive orElse masterReceive
}

class PeerConnectionHandler(networkController: ActorRef,
                            connection: ActorRef,
                            remote: InetSocketAddress) extends Actor with MessageHandler with Logging {

  import PeerConnectionHandler._

  context watch connection

  context.system.scheduler.schedule(2.seconds, 15.seconds)(self ! ByteString(MessageProducer.ping().bytes))

  override def receive: Receive = ({

    case data: ByteString => connection ! Write(data)

    case CommandFailed(w: Write) =>
      log.info(s"Write failed : $w " + remote)
      connection ! Close

    case Received(data) =>
      Message.parse(data.toArray) match {
        case Success(message) =>
          log.info("received message " + message.getClass.getSimpleName + " from " + remote)
          if (message.verify()) {
            val senderPk = message.senderPubkey

            val peers = PeerDatabaseImpl.allConnectedPeers()

            //todo: check there's another peer with the same Nxt id,
            //todo: but how to handle same hostname but different ports?

            //check if there's no Nxt id or different Nxt id is assigned to the peer
            val po = peers.find(_.peer == remote)
            lazy val p = po.get
            if (po.isEmpty || (p.pubKey.isDefined && p.pubKey.get.sameElements(senderPk))) {
              self ! CloseConnection
            } else {
              if (p.pubKey.isEmpty) {
                PeerDatabaseImpl.removeConnectedPeer(remote)
                val acc = Account.getAccount(senderPk)
                PeerDatabaseImpl.addConnectedPeer(Some(acc.getPublicKey), remote, self)
                log.info(s"Address set for $acc: $remote")
              }

              message.concrete() match {
                case Success(concreteMessage) =>
                  self ! concreteMessage

                case Failure(e) =>
                  log.warn("PeerConnectionHandler: got error: ", e)
                  self ! CloseConnection
              }
            }
          } else {
            log.warn(s"Broken message: $message")
            self ! CloseConnection
          }

        case Failure(e) =>
          log.info(s"Corrupted data from: " + remote + " : " + e.getMessage)
          connection ! Close
      }

    case cc: ConnectionClosed =>
      networkController ! NetworkController.PeerDisconnected(remote)
      log.info("Connection closed to : " + remote + ": " + cc.getErrorCause)

    case CloseConnection =>
      log.info(s"Enforced to abort communication with: " + remote)
      connection ! Close

  }: Receive) orElse handleMessage orElse ({
    case nonsense: Any => log.warn(s"Strange input: $nonsense")
  }: Receive)
}

object PeerConnectionHandler {
  case object CloseConnection
}