/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bitcoin

import org.bitcoinj.core._
import smartcontract.utils.Logging

import scala.collection.mutable

object LastTransactionsStorage extends AbstractPeerEventListener with Logging {
  private val Size = 100
  private val Storage = mutable.Buffer[Transaction]()

  override def onBlocksDownloaded(peer: Peer, block: Block, filteredBlock: FilteredBlock, blocksLeft: Int) = {
    log.debug(s"Block downloaded from a peer with best height: ${peer.getBestHeight}, blocks left $blocksLeft")
  }

  override def onTransaction(peer: Peer, t: Transaction): Unit = synchronized {
    if(Storage.size == Size) Storage -= Storage.head
    Storage += t
    log.debug("Storage size: " + Storage.size)
  }

  def lastTransaction(): Option[Transaction] =
    Storage.reverseIterator.find(tx => !tx.isCoinBase && tx.isPending)
}
