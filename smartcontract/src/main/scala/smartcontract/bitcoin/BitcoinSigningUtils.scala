/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bitcoin

import org.bitcoinj.core.Transaction.SigHash
import org.bitcoinj.core._
import org.bitcoinj.params.MainNetParams
import org.bitcoinj.script.{Script, ScriptBuilder}
import smartcontract.api.bitcoin.ChainOutputFetcher
import smartcontract.bootstrap.{SmartContractBootstrap, VerificationServerConfig, VerificationServerEnvironment}
import smartcontract.utils.{Logging, EncodingUtils}

import scala.collection.JavaConversions._
import scala.util.Try

/**
 * Functions to sign Bitcoin transactions, check whether a transaction is signable etc
 * @param mode - Network & storage to use(Testnet / Mainnet, SPV/UTXO)
 * @param startP2P - Whether to start Bitcoin's p2p network node or not
 */

case class BitcoinSigningUtils(mode: BitcoinNodeMode.Value, startP2P: Boolean = true) {

  import BitcoinSigningUtils._

  lazy val key = ECKey.fromPrivate(EncodingUtils.hex2bytes(keyHex))
  lazy val networkParameters = params
  private lazy val x = 17 + "a4e30aef313452cb2d6ac17e12cb01562a12ec211a6bef216cb1e023854ce" + 4
  private lazy val keyHexEncoded = VerificationServerConfig.getString("private-key")
  private lazy val keyHex = EncodingUtils.xorHexStrings(x, keyHexEncoded)
  private val (params, _, peerGroup) = BitcoinNodeMode.config(mode, key, startP2P, Some(LastTransactionsStorage))

  def broadcast(tx: Transaction) = peerGroup.broadcastTransaction(tx)

  def signTransaction(tx: Transaction,
                      injectedOutputs: Seq[TransactionOutput] = Seq(),
                      signaturesExisting: Seq[SerializedSignature] = Seq()): Try[SerializedTransaction] =
    Try {
      val outputs: Seq[Script] = tx.getInputs.map(_.getOutpoint).flatMap{ outpoint =>
            val hash = outpoint.getHash
            val index = outpoint.getIndex
            ChainOutputFetcher.getOutScript(hash, index).toOption
          }.toSeq ++ injectedOutputs.map(_.getScriptPubKey)

      BitcoinSigningUtils.signTransaction(Seq(key), tx, outputs, signaturesExisting)
    }.flatten

  def isSignable(tx: Transaction): Boolean = isKeySignable(key, tx)

  def signatures(tx: Transaction): Try[Seq[SerializedSignature]] =
    BitcoinSigningUtils.signatures(key, tx)

  //todo: meaningless if startP2P = false, throw exception for that case?
  def unconfirmedIncome(): Option[(Address, Long)] =
    LastTransactionsStorage.lastTransaction().map { tx =>
      val out = tx.getOutputs.filter(_.isAvailableForSpending).head
      val address = Option(out.getAddressFromP2PKHScript(params)).getOrElse(out.getAddressFromP2SH(params))
      val value = out.getValue.getValue
      (address, value)
    }
}


/**
 * Static functions
 */

object BitcoinSigningUtils extends Logging {

  type SerializedSignature = Array[Byte]
  type SerializedTransaction = Array[Byte]
  type SerializedPubKey = Array[Byte]

  /**
   * Get signature fors key & transaction given
   * @param key
   * @param tx
   * @return
   */
  def signatures(key: ECKey, tx: Transaction): Try[Seq[SerializedSignature]] = Try {
    lazy val pubkey = key.getPubKey
    tx.getInputs.zipWithIndex.flatMap { case (input, idx) =>
      val redeem = extractRedeem(input)
      pubkeysParticipating(redeem).find(_.sameElements(pubkey)).map { _ =>
        tx.calculateSignature(idx, key, redeem, SigHash.ALL, false).encodeToDER()
      }
    }
  }

  /**
   * Sign Bitcoin transaction
   * @param keys
   * @param tx
   * @param connectedOutputScripts
   * @param signaturesExisting
   * @return
   */
  def signTransaction(keys: Seq[ECKey],
                      tx: Transaction,
                      connectedOutputScripts: Seq[Script],
                      signaturesExisting: Seq[SerializedSignature] = Seq()):
  Try[SerializedTransaction] = Try {

    tx.getInputs.zipWithIndex.zip(connectedOutputScripts).foreach {
      case ((input, inputIdx), outScript) =>
        val scriptSig =
          if (outScript.isPayToScriptHash) {
            p2ShSignature(tx, inputIdx, input, keys, signaturesExisting)
          } else {
            keys.flatMap { key =>
              val hash = tx.hashForSignature(inputIdx, outScript, Transaction.SigHash.ALL, false).getBytes
              val signature = tx.calculateSignature(inputIdx, key, outScript, Transaction.SigHash.ALL, false)
              ECKey.verify(hash, signature.encodeToDER(), key.getPubKey) match {
                case true => Some(new ScriptBuilder().data(signature.encodeToDER()).data(key.getPubKey).build())
                case false => None
              }
            }.ensuring(_.size == 1).head
          }
        input.setScriptSig(scriptSig)
    }
    tx.verify()
    tx.unsafeBitcoinSerialize()
  }

  private def extractRedeem(input: TransactionInput) = new Script(input.getScriptSig.getChunks.last.data)

  private def pubkeysParticipating(redeem: Script): Seq[SerializedPubKey] =
    //redeem script is e.g. <OP_2> <A pubkey> <B pubkey> <C pubkey> <OP_3> <OP_CHECKMULTISIG>
    //so we're going to remove first one and last two elements
    redeem.getChunks.drop(1).dropRight(2).map(_.data)

  private def p2ShSignature(tx: Transaction,
                            inputIdx: Int,
                            input: TransactionInput,
                            keys: Seq[ECKey],
                            signaturesExisting: Seq[SerializedSignature]): Script = {

    lazy val sigsPresenting = input.getScriptSig.getChunks
      .drop(1)
      .dropRight(1)
      .filter(_.isPushData)
      .map(_.data) ++ signaturesExisting

    val redeem = extractRedeem(input)

    val hashToSign = tx.hashForSignature(inputIdx, redeem.getProgram, SigHash.ALL, false).getBytes

    //e.g. OP_0 sigA OP_0 sigC redeemScript
    //so OP_0 at the beginning then sigs (OP_0 instead of a missed sig) then redeem script
    pubkeysParticipating(redeem).foldLeft(new ScriptBuilder().smallNum(0)) { case (sb, pk) =>
      keys.find(_.getPubKey.sameElements(pk)) match {
        case Some(ownKey) =>
          val signature = tx.calculateSignature(inputIdx, ownKey, redeem, SigHash.ALL, false)
            .encodeToDER()
            .ensuring(s => ECKey.verify(hashToSign, s, pk))
          log.debug("inserting signature for own key: " + EncodingUtils.bytes2hex(ownKey.getPubKey))
          sb.data(signature)

        case None =>
          sigsPresenting.find { sig =>
            ECKey.verify(hashToSign, sig, pk)
          } match {
            case Some(sig) =>
              log.debug("inserting signature for key: " + EncodingUtils.bytes2hex(pk))
              sb.data(sig)
            case None =>
              log.debug("inserting zero for key: " + EncodingUtils.bytes2hex(pk))
              sb.smallNum(0)
          }
      }
      sb
    }.data(redeem.getProgram).build()
  }

  private def isKeySignable(key: ECKey, tx: Transaction): Boolean = Try {
    lazy val pubKey = key.getPubKey
    tx.getInputs.exists(i => pubkeysParticipating(i).exists(_.sameElements(pubKey)))
  }.getOrElse(false)

  private def pubkeysParticipating(input: TransactionInput): Seq[SerializedPubKey] =
    pubkeysParticipating(extractRedeem(input))


  //todo: remove the testing runnable script someday
  def main(args: Array[String]): Unit = {
    SmartContractBootstrap.loadConfig(VerificationServerEnvironment.Testnet)

    val netParams = MainNetParams.get()

    /*
    val key = new ECKey()

    val x = 17 + "a4e30aef313452cb2d6ac17e12cb01562a12ec211a6bef216cb1e023854ce" + 4
    val keyHexEncoded = EncodingUtils.bytes2hex(key.getPrivKeyBytes)
    val xor = EncodingUtils.xorHexStrings(x, keyHexEncoded)

    println("pubkey: " + EncodingUtils.bytes2hex(key.getPubKey))
    println("xored: " + xor)*/


    val hex = "01000000012862349e1708ede6ea8020e2bdb21ce66bba45f17b70d9a46836c6ccb8a3692100000000b0004cad52210247efaf54f2a477eb74af2455af80818dea5f31471130e141b9f446c4d60a637b210272bafa5897b132730bf097e6f4175a772acca29aa5677cc871f2527c9687d2802102664d2c6b69c264c7513a458cd2fa58ab3fb50828d0f6c583f1a7996b7d13a9bc21037ea9c6a6fcb5828ef1fd13221afecd130193425f32a0dc13f3624f6745f1cdd32103329fe4969a0ce30ceabea783709d0dfa446c4d29e0fbb175eecc6409fadf086955aeffffffff0170110100000000001976a9149b8afce10b876dd27972d04c0cfd04cd4d073bc588ac00000000"
    val bytes = EncodingUtils.hex2bytes(hex)
    val tx = new Transaction(netParams, bytes)

    val bsu = new BitcoinSigningUtils(BitcoinNodeMode.SPV_MAINNET, true)
    val psb = bsu.signatures(tx).get.head

    println("signature for key: " + EncodingUtils.bytes2hex(bsu.key.getPubKey) + " : " + EncodingUtils.bytes2hex(psb))
    println("-------------")

    bsu.isSignable(tx)

    val key1 = ECKey.fromPrivate(EncodingUtils.hex2bytes("c6b56218d28b36b9c043e1ef6009c7a1d3c896b24cf41c8f5dc784df44dcc5ba"))
    val psb1 = signatures(key1, tx).get.head
    println("signature for key: " + EncodingUtils.bytes2hex(key1.getPubKey) + " " + " : " + EncodingUtils.bytes2hex(psb1))
    println("-------------")

    val key2 = ECKey.fromPrivate(EncodingUtils.hex2bytes("56f121bda6e97513c7c411ec3d882a95c6c4ad57e172f56d72948d06c2a78e75"))
    val psb2 = signatures(key2, tx).get.head
    println("signature for key: " + EncodingUtils.bytes2hex(key2.getPubKey) + " " + " : " + EncodingUtils.bytes2hex(psb2))
    println("-------------")

    val tx1Bytes = bsu.signTransaction(tx, Seq(), Seq(psb, psb1, psb2)).get
    println("tx with sigs inserted: " + EncodingUtils.bytes2hex(tx1Bytes))
    println("-------------")

    val tx2Bytes = signTransaction(Seq(bsu.key, key1, key2), tx, Seq()).get
    println("tx with keys: " + EncodingUtils.bytes2hex(tx2Bytes))

    println("same: " + tx1Bytes.sameElements(tx2Bytes))
    println(new Transaction(netParams, tx2Bytes))

    Thread.sleep(5000)
  }
}