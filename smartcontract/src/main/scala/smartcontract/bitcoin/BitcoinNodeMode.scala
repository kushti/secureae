/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bitcoin

import org.bitcoinj.core._
import org.bitcoinj.net.discovery.{SeedPeers, DnsDiscovery}
import org.bitcoinj.params.{TestNet3Params, MainNetParams}
import org.bitcoinj.store.{H2FullPrunedBlockStore, SPVBlockStore}
import org.joda.time.DateTime
import smartcontract.utils.Logging


object BitcoinNodeMode extends Enumeration with Logging {

  val SPV_TESTNET = Value(1)
  val SPV_MAINNET = Value(2)
  val SPV_MAINNET_TEST = Value(3)
  val UTXO_MAINNET = Value(4)

  def config(mode: BitcoinNodeMode.Value,
             key: ECKey,
             startP2P: Boolean,
             peerEventListenerOpt: Option[PeerEventListener]):
                (NetworkParameters, AbstractBlockChain, PeerGroup) = {
    lazy val mainParams = MainNetParams.get()
    lazy val testParams = TestNet3Params.get()

    def configureWallet(params: NetworkParameters) = {
      val wallet = new Wallet(params)
      wallet.importKey(key)
      wallet.addWatchedAddress(key.toAddress(params))
      wallet
    }

    val (params: NetworkParameters, chain: AbstractBlockChain, walletOpt: Option[Wallet]) = mode match {
      case SPV_TESTNET =>
        val store = new SPVBlockStore(testParams, new java.io.File("/tmp/spv_testnet"))
        val w = configureWallet(testParams)
        (testParams, new BlockChain(testParams, w, store), Some(w))

      case SPV_MAINNET =>
        val store = new SPVBlockStore(mainParams, new java.io.File("/tmp/spv"))
        val w = configureWallet(mainParams)
        (mainParams, new BlockChain(mainParams, w, store), Some(w))

      case SPV_MAINNET_TEST =>
        val store = new SPVBlockStore(mainParams, new java.io.File("/tmp/spv-test"))
        val w = configureWallet(mainParams)
        (mainParams, new BlockChain(mainParams, w, store), Some(w))

      case UTXO_MAINNET =>
        val store = new H2FullPrunedBlockStore(mainParams, "utxo/utxo", 2000)
        val fpc = new FullPrunedBlockChain(mainParams, store)
        (mainParams, fpc, None)
    }

    val peerGroup = new PeerGroup(params, chain)
    peerGroup.setUserAgent("SmartContract.com", "0.1")
    peerGroup.addPeerDiscovery(new DnsDiscovery(params))
    walletOpt.foreach(wallet => peerGroup.addWallet(wallet))

    if (!chain.isInstanceOf[FullPrunedBlockChain]) {
      peerGroup.setFastCatchupTimeSecs(new DateTime().minusWeeks(2).getMillis / 1000)
    }

    peerGroup.addPeerDiscovery(new SeedPeers(params))

    if (startP2P) {
      peerEventListenerOpt.foreach(peerGroup.addEventListener)
      peerGroup.startAsync()
      if(chain.isInstanceOf[FullPrunedBlockChain]) {
        log.debug("Bitcoin blockchain downloading has been started...")
        peerGroup.downloadBlockChain()
        log.debug("Bitcoin blockchain downloading is done")
      }
    }

    (params, chain, peerGroup)
  }
}