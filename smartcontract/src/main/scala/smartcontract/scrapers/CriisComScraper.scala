/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.scrapers

import akka.actor.ActorSystem
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import smartcontract.AppRegistry
import smartcontract.api.HttpClient
import smartcontract.bootstrap.VerificationServerConfig

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}


object CriisComScraper {
  def scrapeDeeds(block: String, lot: String, startTime: Long): Future[Try[(Seq[String], Seq[String])]] = {
    implicit lazy val actorSystem = AppRegistry.system
    implicit lazy val monitoring = AppRegistry.monitoring

    val url = s"http://www.criis.com/cgi-bin/new_get_recorded.cgi/?county=sanfrancisco&SEARCH_TYPE=APN&COUNTY=san%20francisco&YEARSEGMENT=current&ORDER_TYPE=Recorded%20Official&LAST_RECORD=1&SCREENRETURN=doc_search.cgi&SCREEN_RETURN_NAME=Recorded%20Document%20Search&BLOCK=$block&LOT=$lot"

    def reduceFn(t1: (Seq[String], Seq[String]), t2: (Seq[String], Seq[String])) =
      (t1._1 ++ t2._1) -> (t1._2 ++ t2._2)

    def timeFilterFn(tr: Element): Boolean = {
      val text = tr.select("td")(3).text()
      val dt = DateTime.parse(text, DateTimeFormat.forPattern("MM/dd/YYYY"))
      dt.getMillis >= startTime
    }

    val fr = new HttpClient().get(url).map { bodyTry =>
      bodyTry.map { body =>
        val recordsIterator = Jsoup.parse(body).select("table.records").select("tr").iterator()
        val deedRecords = recordsIterator.filter(_.select("td").last().text() == "DEED").toList
        val filteredDeeds = deedRecords.filter(timeFilterFn)
        val hrefs = filteredDeeds.map(_.select("td").head.select("a").attr("href").replaceAll(" ", "+"))
        val deeds = hrefs.map(scrapeDeed)
        deeds
      }
    }

    fr.map(tr => tr.map(_.toSeq)
      .map(s => flatten(s))
      .flatten
      .map(_.foldLeft(Seq[String]() -> Seq[String]())(reduceFn)))
  }

  private def flatten[T](xs: Seq[Try[T]]): Try[Seq[T]] = {
    val (ss: Seq[Success[T]]@unchecked, fs: Seq[Failure[T]]@unchecked) =
      xs.partition(_.isSuccess)

    if (fs.isEmpty) Success(ss map (_.get))
    else Failure[Seq[T]](fs.head.exception) // Only keep the first failure
  }

  private def scrapeDeed(url: String)(implicit actorSystem: ActorSystem): Try[(Seq[String], Seq[String])] = {
    implicit lazy val actorSystem = AppRegistry.system
    implicit lazy val monitoring = AppRegistry.monitoring

    val ftRes = new HttpClient().get("http://www.criis.com" + url).map { bodyTry =>
      bodyTry.map { body =>
        val deedDoc = Jsoup.parse(body)
        val deedTrs = deedDoc.select("table.records").select("tr").iterator().toList

        deedTrs.foldLeft((Seq[String](), Seq[String]())) { case ((seqR, seqE), tr) =>
          val tds = tr.select("td")
          val idx = tds.zipWithIndex.find(_._1.text().contains("GRANTOR")).map(_._2).getOrElse(10)

          val party = tds.last().text().trim
          if (tds.size() > idx) {
            tds.find(e => e.text() == "R" || e.text() == "E").map { e =>
              val tipe = e.text()
              if (tipe == "R") {
                (seqR ++ Seq(party), seqE)
              } else if (tipe == "E") {
                (seqR, seqE ++ Seq(party))
              } else {
                (seqR, seqE)
              }
            }.getOrElse {
              (seqR, seqE)
            }
          } else (seqR, seqE)
        }
      }
    }
    Try(
      Await.result(ftRes, VerificationServerConfig.getDuration("criis-timeout"))
    ).flatten
  }
}
