/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import nxt.util.Convert

sealed trait Currency {
  val id: Byte
}

object Currency {
  def fromId(id: Byte) = id match {
    case UsdImpl.id => UsdImpl
    case BtcImpl.id => BtcImpl
    case NxtImpl.id => NxtImpl
  }

  def fromString(s:String) = s.toLowerCase match {
    case s:String if s == "usd" => UsdImpl
    case s:String if s == "btc" => BtcImpl
    case s:String if s == "nxt" => NxtImpl
  }
  
}


trait Usd extends Currency {
  override val id: Byte = 0
}

object UsdImpl extends Usd

trait Btc extends Currency {
  override val id: Byte = 1
}

object BtcImpl extends Btc

trait Nxt extends Currency {
  override val id: Byte = 2
}

object NxtImpl extends Nxt


abstract class Address

case class BtcAddress(address: String) extends Address with Btc

object BtcAddress {
  def fromString(addr: String): Option[BtcAddress] = {
    lazy val sz = addr.length
    if ((addr.startsWith("1") || addr.startsWith("3")) && sz >= 26 && sz <= 34) Some(new BtcAddress(addr))
    else None
  }
}


case class NxtAddress(address: Long) extends Address with Nxt

object NxtAddress {
  def fromString(addr: String): Option[NxtAddress] =
    Option(Convert.parseAccountId(addr)).map(NxtAddress.apply)
}