/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.utils.Logging

import scala.concurrent.Await
import scala.language.existentials
import scala.util.{Failure, Success, Try}


case class SmartTerm(name: String,
                     expected: QueryAndCheck[_ <: Value],
                     action: Action,
                     deadline: Long)


case class SmartTermsSequence(contractId: String,
                              name: String,
                              startTime: Long,
                              deadline: Long,
                              terms: Seq[SmartTerm],
                              failureAction: Action) extends Logging {
  import SmartTermsSequence._

  type Clause = (TermState, QueryAndCheck[_])
  type Out = (Action, TermState)

  lazy val transitions: Map[Clause, Out] = {
    val mutTr = scala.collection.mutable.Map[Clause, Out]()

    val states = Seq(initState) ++
      terms.dropRight(1).map(_.name + " done").map(s => new TermState(s)) ++
      Seq(done)

    states.sliding(2).toSeq.zip(terms).map { case (statesSeq, term) =>
      val ss = statesSeq.head
      val fs = statesSeq(1)
      mutTr += (ss, term.expected) ->(term.action, fs)

      val qCtx = QueryContext(contractId, startTime, term.name)
      mutTr += (ss, QueryAndCheckBuilder.deadline(deadline, qCtx)) ->(failureAction, failureState)
    }
    mutTr.toMap
  }

  def doStep(currentState: TermState): Option[TermState] =
    if (isFinished(currentState)) None
    else {
      transitions.filterKeys(_._1 == currentState).find { case ((_, qc), _) =>
        Try(Await.result(qc.process(), VerificationServerConfig.termTrackingTimeout)) match {
          case Success(b) => b
          case Failure(e) =>
            log.error(s"${qc.context.contractId}:${qc.context.termName} tracking error, timeout: ", new Exception(e))
            e.printStackTrace()
            false
        }
      } map { case ((state, qc), (action, stateToGo)) =>
        action.process()
        onTransition(qc.predicate, action, stateToGo)
        QueryCache.remove(qc.query, qc.context.termName)
        stateToGo
      }
    }

  def isFinished(currentState: TermState) = isFailed(currentState) || isSuccess(currentState)

  def isFailed(currentState: TermState) = currentState == failureState

  def isSuccess(currentState: TermState) = currentState == done

  protected def onTransition(p: Predicate[_], a: Action, s: TermState) =
    log.debug(s"State changed for term $name to $s")

  override def toString = transitions.map { t =>
    s"${t._1._1} :: ${t._1._2} -> ${t._2._1} :: ${t._2._2}"
  }.mkString("\n", "\n", ";")
}

object SmartTermsSequence {
  lazy val initState: InitTermState = new InitTermState(Initialized)
  lazy val failureState: FinishTermState = new FinishTermState(Failed)
  lazy val done: FinishTermState = new FinishTermState(Completed)
  val Initialized = "initialized"
  val Failed = "failed"
  val Completed = "completed"

  def stateFromString(s:String):TermState = s match {
    case s:String if s == Initialized => initState
    case s:String if s == Failed => failureState
    case s:String if s == Completed => done
  }

  def stateFromBoolean(success:Boolean):FinishTermState = success match {
    case true => done
    case false => failureState
  }
}