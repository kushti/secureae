/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

sealed trait Predicate[V <: Value] {
  self =>
  val pattern: Any

  def matchPattern(value: V): Boolean
}

sealed trait StringPredicate extends Predicate[StringValue]

case class StringPartOf(override val pattern: String) extends StringPredicate {
  override def matchPattern(value: StringValue) = value.value contains pattern

  override def toString = s"contains $pattern"
}

sealed case class StringEq(override val pattern: String) extends StringPredicate {
  override def matchPattern(value: StringValue) = value.value == pattern

  override def toString = s"== $pattern"
}


sealed trait LongPredicate extends Predicate[LongValue]

case class NotLessThan(override val pattern: Long) extends LongPredicate {
  override def matchPattern(value: LongValue) = value.value >= pattern

  override def toString = s">= $pattern"
}

case class WithinRange(override val pattern: (Int, Int)) extends LongPredicate {
  override def matchPattern(value: LongValue) = value.value >= pattern._1 && value.value <= pattern._2

  override def toString = s" (>= ${pattern._1} && <= ${pattern._2})"
}


sealed trait StringSeqsPredicate extends Predicate[StringSeqsValue]

case class StringSeqsPartOf(override val pattern: (Seq[String], Seq[String])) extends StringSeqsPredicate {

  private def contains(s: String, wordsToCheck: Seq[String]): Boolean = {
    val words = s.split("\\s").map(_.toLowerCase)
    wordsToCheck.foldLeft(true) { case (b, w) =>
      b match {
        case true => words.contains(w.toLowerCase)
        case false => false
      }
    }
  }

  override def matchPattern(value: StringSeqsValue) = {
    val tuples = value.value
    tuples._1.foldLeft(false) { case (b, s) =>
      b match {
        case true => true
        case false => contains(s, pattern._1)
      }
    } && tuples._2.foldLeft(false) { case (b, s) =>
      b match {
        case true => true
        case false => contains(s, pattern._2)
      }
    }
  }

  override def toString = s" contains $pattern"
}