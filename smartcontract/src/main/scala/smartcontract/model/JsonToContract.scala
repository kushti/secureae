/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import org.bitcoinj.core.Transaction
import org.bitcoinj.params.MainNetParams
import play.api.libs.json.{JsArray, JsObject, JsValue}
import smartcontract.db.interfaces.{ActionId, GlobalTermId}
import smartcontract.utils.EncodingUtils

import scala.collection.immutable.HashMap
import scala.util.{Failure, Success, Try}


case class Escrow(name: String, currency: String, amount: Long)

case class EscrowPayment(receiver: Address)

case class CommonTermData(escrow: Option[Escrow], success: Seq[EscrowPayment],
                          failure: Seq[EscrowPayment], togo: Option[String])

trait Expected {
  val deadline: Long
}

case class RankingRange(min: Int,
                        max: Int,
                        phrase: String,
                        domain: String,
                        geo: String,
                        searchEngine: String,
                        deadline: Long) extends Expected

case class ExpectedPayment(sender: Option[Address],
                           receiver: Address,
                           amount: Long,
                           amountCurrency: Option[Currency],
                           deadline: Long) extends Expected

case class RealEstate(state: String,
                      county: String,
                      block: String,
                      lot: String,
                      grantorWords: Seq[String],
                      granteeWords: Seq[String],
                      deadline: Long) extends Expected

trait Term[E <: Expected] {
  val commonData: CommonTermData
  val expected: Expected
}

case class PaymentTerm(expected: ExpectedPayment, commonData: CommonTermData) extends Term[ExpectedPayment]

case class RankingTerm(expected: RankingRange, commonData: CommonTermData) extends Term[RankingRange]

case class RealEstateTerm(expected: RealEstate, commonData: CommonTermData) extends Term[RealEstate]


object JsonToContract {

  def parseOutcomes(contractId: String,
                    termNames: Set[String],
                    outcomeJsOpt: Option[JsObject]): Try[Map[ActionId, Transaction]] = Try {
    val outcomeJs = outcomeJsOpt.get

    termNames.flatMap { termName =>
      val termId = GlobalTermId(contractId, termName)

      (outcomeJs \ termName).asOpt[JsValue].map { innerJson =>
        val s: Option[(ActionId, String)] = (innerJson \ "success").asOpt[Seq[String]].map { sseq =>
          ActionId(termId, success = true) -> sseq.head
        }

        val f: Option[(ActionId, String)] = (innerJson \ "failure").asOpt[Seq[String]].map { fseq =>
          ActionId(termId, success = false) -> fseq.head
        }

        Seq(s, f).flatMap(_.map(t => t._1 -> new Transaction(MainNetParams.get(), EncodingUtils.hex2bytes(t._2))))
      }.getOrElse(Seq())
    }.toMap
  }

  def apply(json: JsObject,
            contractParams: ContractParameters,
            trackingState: SmartContractTrackingState.Value): Try[SmartContract] =
    parseCommonContractData(json).flatMap { ccd =>
      Try {
        val termsJA = (json \ "terms").as[JsArray].value
        val terms: Map[String, Term[_]] = termsJA.map { termJs =>
          val termName = (termJs \ "name").as[String]

          val cdTry = parseCommonTermData(termName, termJs, ccd)

          val expectedJs = termJs \ "expected"

          val expType = (expectedJs \ "type").as[String].toLowerCase
          val expTry: Try[Expected] = if (expType == "payment") {
            parseExpectedPayment(expectedJs)
          } else if (expType == "seo") {
            parseRankingRange(expectedJs)
          } else if (expType == "deed") {
            parseRealEstate(expectedJs)
          } else throw new IllegalStateException("Wrong kind of expected event")

          (cdTry, expTry) match {
            case Tuple2(Success(cd), Success(exp: ExpectedPayment)) => termName -> PaymentTerm(exp, cd)
            case Tuple2(Success(cd), Success(exp: RankingRange)) => termName -> RankingTerm(exp, cd)
            case Tuple2(Success(cd), Success(exp: RealEstate)) => termName -> RealEstateTerm(exp, cd)
            case Tuple2(Failure(e), _) =>
              e.printStackTrace()
              throw new Exception(s"There's a problem with term $termName common data parsing: ${e.getMessage}")
            case Tuple2(_, Failure(e)) =>
              e.printStackTrace()
              throw new Exception(s"There's a problem with term $termName expected data parsing: ${e.getMessage}")
            case _ => throw new IllegalStateException()
          }
        }.toMap

        buildContract(ccd, terms, contractParams.unixStartTime * 1000, contractParams, trackingState)
      }
    }

  private def parseCommonContractData(contractJs: JsObject) = Try {
    val id = (contractJs \ "id").as[String]

    val contractName = (contractJs \ "name").as[String]
    val description = (contractJs \ "description").as[String]

    val escrowsJs = (contractJs \ "escrows").as[JsArray]
    val escrows = (escrowsJs \\ "name").map(_.as[String]).zipWithIndex.map { case (escName, ei) =>
      val e = escrowsJs(0)
      Escrow(escName, (e \ "currency").as[String].toLowerCase, (e \ "amount").as[String].toLong)
    }
    CommonContractData(id, contractName, description, escrows)
  }

  private def parseCommonTermData(termName: String, termJs: JsValue, ccd: CommonContractData) = Try {
    def parseActionsJs(escrowCurrency: String, actionsJs: JsValue): Seq[EscrowPayment] = {
      val actions = (actionsJs \ "actions").as[JsArray]

      (actions \\ "to").seq.flatMap(_ \\ "address").seq
        .flatMap(_.asOpt[String])
        .map(stringToAddress(escrowCurrency))
        .map(EscrowPayment.apply)
    }

    val escrowOpt = (termJs \ "escrow").asOpt[String]
      .map(escrowStr => ccd.escrows.find(_.name == escrowStr)
      .getOrElse(throw new IllegalStateException(s"No escrow found for $termName")))
    lazy val escrowCurrencyOpt = escrowOpt.map(_.currency)

    val togo = (termJs \ "success" \ "next").asOpt[String]

    val (successPayments, failPayments) = escrowCurrencyOpt.map { escrowCurrency =>
      val sp = (termJs \ "success" \ "type").asOpt[String] match {
        case Some(t) if t == "payment" => parseActionsJs(escrowCurrency, termJs \ "success")
        case _ => Seq()
      }
      val fp = parseActionsJs(escrowCurrency, termJs \ "failure")
      (sp, fp)
    }.getOrElse(Seq(), Seq())

    CommonTermData(escrowOpt, successPayments, failPayments, togo)
  }

  private def stringToAddress(currencyCode: String)(address: String): Address = {
    val resOpt = if (currencyCode == "nxt") NxtAddress.fromString(address)
    else if (currencyCode == "btc" || currencyCode == "smartcontract/bitcoin") BtcAddress.fromString(address)
    else throw new IllegalStateException("Only Nxt & BTC are supported")

    resOpt match {
      case Some(res) => res
      case None => throw new IllegalStateException(s"Wrong $currencyCode address provided: $address")
    }
  }

  private def buildContract(ccd: CommonContractData,
                            terms: Map[String, Term[_]],
                            startTime: Long,
                            contractParams: ContractParameters,
                            trackingState: SmartContractTrackingState.Value): SmartContract = {

    val stSeqs = terms.map { case (termName, term) =>
      val amtOpt = term.commonData.escrow.map(_.amount)

      val deadline = term.expected.deadline
      val action = term.commonData.success.headOption
        .map(ep => ActionBuilder.payment(ep.receiver, amtOpt.get))
        .getOrElse(NoAction)

      val qCtx = QueryContext(ccd.id, startTime, termName)

      val qc = term.expected match {
        case ep: ExpectedPayment => QueryAndCheckBuilder.withExpectedPayment(ep, qCtx)
        case rr: RankingRange => QueryAndCheckBuilder.withSeoRanking(rr, qCtx)
        case re: RealEstate => QueryAndCheckBuilder.withRealEstate(re, qCtx)
      }

      val st = SmartTerm(termName, qc, action, deadline)
      val fa = term.commonData.failure.headOption
        .map(ep => ActionBuilder.payment(ep.receiver, amtOpt.get))
        .getOrElse(NoAction)
      SmartTermsSequence(ccd.id, termName, startTime, deadline, Seq(st), fa)
    }.toSeq

    val withInitState = HashMap(stSeqs.map(stq => stq -> SmartTermsSequence.initState): _*)

    new SmartContract(ccd.id, ccd.name, ccd.description, withInitState, startTime,
                      contractParams, trackingState, startTime)
  }

  private def parseRealEstate(expectedJs: JsValue): Try[Expected] = Try {
    val block = (expectedJs \ "block").as[String]
    val lot = (expectedJs \ "lot").as[String]
    val county = (expectedJs \ "county").as[String]
    val state = (expectedJs \ "state").as[String].toLowerCase

    if (state != "ca") throw new IllegalStateException("Only CA supported as state value")
    if (county != "san francisco") throw new IllegalStateException("Only 'san francisco' supported as county value")

    val deadline = (expectedJs \ "deadline").as[String].toLong * 1000
    val grantorWords = (expectedJs \ "grantor").as[JsArray].value.map(_.as[String])
    val granteeWords = (expectedJs \ "grantee").as[JsArray].value.map(_.as[String])

    RealEstate(state, county, block, lot, grantorWords, granteeWords, deadline)
  }

  private def parseExpectedPayment(expectedJs: JsValue) = Try {
    val currency = (expectedJs \ "currency").as[String].toLowerCase
    val amount = (expectedJs \ "amount").as[String].toLong

    val fromOptStr = (expectedJs \ "from").asOpt[String]
    val toStr = (expectedJs \ "to" \ "address").as[String]

    val fromOpt = fromOptStr.map(stringToAddress(currency))
    val to = stringToAddress(currency)(toStr)

    val deadline = (expectedJs \ "deadline").as[String].toLong * 1000

    if (amount < 0) throw new IllegalStateException("Amount is negative")

    val amtCurOpt = (expectedJs \ "amount-currency").asOpt[String].map { case str =>
      if (str.toLowerCase == "usd") {
        UsdImpl
      } else {
        throw new IllegalStateException("Only usd is supported as amount-currency value (case doesnt' matter)")
      }
    }

    ExpectedPayment(fromOpt, to, amount, amtCurOpt, deadline)
  }

  private def parseRankingRange(expectedJs: JsValue) = Try {
    val min: Int = toInt(expectedJs \ "min")
    val max: Int = toInt(expectedJs \ "max")

    val provider = (expectedJs \ "provider").as[String]
    if (provider != "semrush.com") {
      throw new IllegalStateException("Wrong or unsupported provider, only 'semrush.com' is supported")
    }
    val deadline = (expectedJs \ "deadline").as[String].toLong * 1000

    val domain = (expectedJs \ "result").as[String]
    val phrase = (expectedJs \ "query" \ "phrase").as[String]
    val geo = (expectedJs \ "query" \ "locale").as[String]
    val engine = (expectedJs \ "query" \ "source").as[String]
    RankingRange(min, max, phrase, domain, geo, engine, deadline)
  }

  private def toInt(jsval: JsValue): Int = {
    jsval.asOpt[Int] match {
      case Some(res) => res
      case None => jsval.as[String].toInt
    }
  }

  case class CommonContractData(id: String, name: String, description: String, escrows: Seq[Escrow])
}
