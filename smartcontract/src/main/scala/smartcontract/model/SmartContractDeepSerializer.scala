/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import play.api.libs.json.{JsArray, JsObject, Json}
import smartcontract.utils.Logging

import scala.util.{Failure, Try}


object SmartContractDeepSerializer extends Logging {

  /*
    ** Serialization functions **
   */

  def serialize(contract: SmartContract): JsObject = {
    Json.obj(
      "id" -> contract.id,
      "name" -> contract.name,
      "description" -> contract.description,
      "start-time" -> contract.startTime,
      "last-update" -> contract.lastUpdate,
      "metadata" -> Json.obj(
        "incoming-json" -> contract.contractParams.contractJson,
        "outcomes" -> contract.contractParams.outcomesJsonOpt,
        "privacy" -> contract.contractParams.privacy.id,
        "password" -> contract.contractParams.passwordOpt
      ),
      "terms-sequences" -> contract.sequences.map { case (sts, st) =>
        Json.obj(
          "name" -> sts.name,
          "state" -> st.name,
          "deadline" -> sts.deadline,
          "failure" -> actionToJson(sts.failureAction),
          "subterms" -> sts.terms.map(smartTermToJson)
        )
      },
      "tracking-state" -> contract.trackingState.toString
    )
  }

  private def smartTermToJson(smartTerm: SmartTerm): JsObject = Json.obj(
    "deadline" -> smartTerm.deadline,
    "action" -> actionToJson(smartTerm.action),
    "name" -> smartTerm.name,
    "expected" -> queryAndCheckToJson(smartTerm.expected)
  )

  private def actionToJson(action: Action): JsObject = action match {
    case NoAction => Json.obj("type" -> "nop")

    case sb: SendBitcoins =>
      Json.obj(
        "type" -> "send-bitcoins",
        "recipient" -> sb.receiver.address,
        "amount-satoshis" -> sb.amountSatoshis
      )
    case _ => ???
  }

  private def queryAndCheckToJson(qac: QueryAndCheck[_]): JsObject = Json.obj(
    "query" -> queryToJson(qac.query),
    "predicate" -> predicateToJson(qac.predicate)
  )

  private def queryToJson(query: Query[_]): JsObject = query match {
    case TimestampQuery(_) =>
      Json.obj("type" -> "timestamp")

    case BitcoinToUsdPaymentQuery(senderOpt, recipient, _) =>
      Json.obj(
        "type" -> "bitcoin-usd-payment",
        "sender" -> senderOpt.map(_.address),
        "recipient" -> recipient.address
      )

    case npq: NxtNoConversionPaymentQuery =>
      Json.obj(
        "type" -> "nxt-payment",
        "sender" -> npq.senderOpt.map(_.address),
        "recipient" -> npq.recipient.address
      )

    case gq: GoogleRankQuery =>
      Json.obj(
        "type" -> "google-rank",
        "geo" -> gq.geo,
        "phrase" -> gq.phrase,
        "domain" -> gq.domain
      )

    case creq: CalifornianRealEstateQuery =>
      Json.obj(
        "type" -> "ca-estate",
        "lot" -> creq.lot,
        "block" -> creq.block
      )

    case bpq: BitcoinNoConversionPaymentQuery =>
      Json.obj(
        "type" -> "bitcoin-btc-payment",
        "sender" -> bpq.senderOpt.map(_.address),
        "recipient" -> bpq.recipient.address
      )

    case _ => ???
  }

  private def predicateToJson(p: Predicate[_]): JsObject = p match {
    case spo: StringPartOf =>
      Json.obj(
        "type" -> "string-part-of",
        "value" -> spo.pattern
      )

    case se: StringEq =>
      Json.obj(
        "type" -> "string-eq",
        "value" -> se.pattern
      )

    case nlt: NotLessThan =>
      Json.obj(
        "type" -> "number-nlt",
        "value" -> nlt.pattern
      )

    case wr: WithinRange =>
      Json.obj(
        "type" -> "number-range",
        "value1" -> wr.pattern._1,
        "value2" -> wr.pattern._2
      )

    case sspo: StringSeqsPartOf =>
      Json.obj(
        "type" -> "string-seq-part-of",
        "value1" -> Json.arr(sspo.pattern._1),
        "value2" -> Json.arr(sspo.pattern._2)
      )

    case _ => ???
  }

  /*
    ** De-serialization functions **
   */

  def deserialize(contractJson: JsObject): SmartContract = Try{
    val id = (contractJson \ "id").as[String]
    val contractName = (contractJson \ "name").as[String]
    val description = (contractJson \ "description").as[String]
    val startTime = (contractJson \ "start-time").as[Long]
    val lastUpdate = (contractJson \ "last-update").as[Long]

    val metadata = contractJson \ "metadata"
    val incomingJson = (metadata \ "incoming-json").as[JsObject]
    val outcomesJsonOpt = (metadata \ "outcomes").asOpt[JsObject]
    val privacyId = (metadata \ "privacy").as[Int]
    val privacy = ContractPrivacyOptions(privacyId)
    val passwordOpt = (metadata \ "password").asOpt[String]

    val trackingState = (contractJson \ "tracking-state").asOpt[String]
      .map(SmartContractTrackingState.withName)
      .getOrElse(SmartContractTrackingState.TRACKING)

    val termsSeqs = (contractJson \ "terms-sequences").as[JsArray].value.map { termJson =>
      val termSeqName = (termJson \ "name").as[String]
      val state = SmartTermsSequence.stateFromString((termJson \ "state").as[String])
      val deadline = (termJson \ "deadline").as[Long]
      val failAction = jsonToAction((termJson \ "failure").as[JsObject])

      val terms = (termJson \ "subterms").as[JsArray].value.map { subtermJson =>
        val stName = (subtermJson \ "name").as[String]
        val stDeadline = (subtermJson \ "deadline").as[Long]
        val stAction = jsonToAction((subtermJson \ "action").as[JsObject])
        val exJs = (subtermJson \ "expected").as[JsObject]

        val qc = QueryContext(id, startTime, stName)
        val stq = jsonToQuery((exJs \ "query").as[JsObject], qc)
        val stp = jsonToPredicate((exJs \ "predicate").as[JsObject], qc)
        val qac = QueryAndCheck(stq.asInstanceOf[Query[Value]], stp.asInstanceOf[Predicate[Value]], qc)
        SmartTerm(stName, qac, stAction, stDeadline)
      }

      SmartTermsSequence(id, termSeqName, startTime, deadline, terms, failAction) -> state
    }.toMap

    val cp = ContractParameters(startTime / 1000, incomingJson, outcomesJsonOpt, privacy, passwordOpt)

    SmartContract(id, contractName, description, termsSeqs, startTime, cp, trackingState, lastUpdate)
  }.recoverWith{case t =>
    log.error("An error during parsing a contract from a db: ", t)
    Failure(t)
  }.get

  private def jsonToAction(js: JsObject): Action = (js \ "type").as[String] match {
    case s: String if s == "nop" => NoAction
    case s: String if s == "send-bitcoins" =>
      val rcp = BtcAddress.fromString((js \ "recipient").as[String]).get //todo: .get ?
    val amt = (js \ "amount-satoshis").as[Long]
      SendBitcoins(rcp, amt)
  }

  private def jsonToQuery(js: JsObject, qc: QueryContext): Query[_ <: Value] = (js \ "type").as[String] match {
    case s: String if s == "timestamp" =>
      TimestampQuery(qc)

    case s: String if s == "bitcoin-btc-payment" =>
      val senderOpt = (js \ "sender").asOpt[String].flatMap(BtcAddress.fromString)
      val rcp = BtcAddress.fromString((js \ "recipient").as[String]).get
      BitcoinNoConversionPaymentQuery(senderOpt, rcp, qc)

    case s: String if s == "bitcoin-usd-payment" =>
      val senderOpt = (js \ "sender").asOpt[String].flatMap(BtcAddress.fromString)
      val rcp = BtcAddress.fromString((js \ "recipient").as[String]).get
      BitcoinToUsdPaymentQuery(senderOpt, rcp, qc)

    case s: String if s == "nxt-payment" =>
      val senderOpt = (js \ "sender").asOpt[String].flatMap(NxtAddress.fromString)
      val rcp = NxtAddress.fromString((js \ "recipient").as[String]).get
      NxtNoConversionPaymentQuery(senderOpt, rcp, qc)

    case s: String if s == "google-rank" =>
      val geo = (js \ "geo").as[String]
      val phrase = (js \ "phrase").as[String]
      val domain = (js \ "domain").as[String]
      GoogleRankQuery(geo, phrase, domain, qc)

    case s: String if s == "ca-estate" =>
      val block = (js \ "block").as[String]
      val lot = (js \ "lot").as[String]
      CalifornianRealEstateQuery(block, lot, qc)
  }

  private def jsonToPredicate(js: JsObject, qc: QueryContext): Predicate[_ <: Value] = (js \ "type").as[String] match {
    case s: String if s == "string-part-of" =>
      StringPartOf((js \ "value").as[String])

    case s: String if s == "string-eq" =>
      StringEq((js \ "value").as[String])

    case s: String if s == "number-nlt" =>
      NotLessThan((js \ "value").as[Int])

    case s: String if s == "number-range" =>
      WithinRange((js \ "value1").as[Int], (js \ "value2").as[Int])

    case s: String if s == "string-seq-part-of" =>
      val seq1 = (js \ "value1").as[JsArray].value.map(_.as[String])
      val seq2 = (js \ "value2").as[JsArray].value.map(_.as[String])
      StringSeqsPartOf(seq1, seq2)
  }
}