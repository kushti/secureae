/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import play.api.libs.json.{JsObject, Json}
import smartcontract.model.SmartContract.ContractId

case class TermState(name: String) {
  override def toString: String = name
}

class InitTermState(name: String) extends TermState(name)

class FinishTermState(name: String) extends TermState(name)

/**
 * Immutable structure to hold contract details:
 * @param id - contract Id(it's string so could contains any chars)
 * @param name - title of a contract
 * @param description - description of a contract
 * @param sequences - terms sequences to be tracked in parallel(while a sequence is to be tracked
 *                  sequentially but for now only 1-element sequences are supported)
 * @param startTime - starting time of whole contract
 * @param contractParams - contract context(incoming JSON for a contract, password,
 *                       bitcoin transactions to sign etc)
 * @param trackingState - whether a contract being tracking or not, possible cases are
 *                      Pending/Tracking/Aborted
 * @param lastUpdate - time of last update(terms' state change / tracking state change)
 */
case class SmartContract(id: ContractId,
                         name: String,
                         description: String,
                         sequences: Map[SmartTermsSequence, TermState],
                         startTime: Long,
                         contractParams: ContractParameters,
                         trackingState: SmartContractTrackingState.Value,
                         lastUpdate: Long) {

  lazy val completeness = sequences.count { case (sq, st) => sq.isSuccess(st) } / sequences.size.toDouble

  lazy val finished = Math.abs(completeness - 1) < 10e-6 //float point numbers equality comparison
}

object SmartContract {
  type ContractId = String

  def serialize(contract: SmartContract): JsObject =
    SmartContractDeepSerializer.serialize(contract)

  def deserialize(contractJson: String): SmartContract =
    deserialize(Json.parse(contractJson).as[JsObject])

  def deserialize(contractJson: JsObject): SmartContract =
    SmartContractDeepSerializer.deserialize(contractJson)
}
