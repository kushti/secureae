/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

sealed trait Value {
  val value: Any

  override def toString = value.toString
}

case class StringValue(override val value: String) extends Value

case class LongValue(override val value: Long) extends Value

case class DoubleValue(override val value: Double) extends Value

case class StringSeqsValue(override val value: (Seq[String], Seq[String])) extends Value

case class LongLongMapValue(override val value: Map[LongValue, LongValue]) extends Value

case class StringLongMapValue(override val value: Map[StringValue, LongValue]) extends Value

object StandardTypesConversions {

  implicit def stringSeq2StringSeqValue(value: (Seq[String], Seq[String])): StringSeqsValue = StringSeqsValue(value)

  implicit def string2StringValue(value: String): StringValue = StringValue(value)

  implicit def stringValue2String(value: StringValue): String = value.value

  implicit def long2LongValue(value: Long): LongValue = LongValue(value)

  implicit def int2LongValue(value: Int): LongValue = LongValue(value)

  implicit def longValue2Long(value: LongValue): Long = value.value

  implicit def double2DoubleValue(value: Long): DoubleValue = DoubleValue(value)
}

