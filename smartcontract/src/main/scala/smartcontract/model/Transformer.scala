/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import play.api.libs.json.{JsArray, JsObject, JsString, Json}
import smartcontract.AppRegistry
import smartcontract.AppRegistry.StoragesImpl
import smartcontract.api.HttpClient
import smartcontract.api.btcusdprice.BtcUsdTickerClientCascade
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.db.interfaces.PaymentDetails

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}


trait Transformer[SourceType <: Value, TargetType <: Value] {
  def transform(value: SourceType): Future[Try[TargetType]]
}

trait NoTransformation[V <: Value] extends Transformer[V, V] {
  override def transform(value: V): Future[Try[V]] = Future(Success(value))
}

trait CurrencyConversion[C1 <: Currency, C2 <: Currency] extends Transformer[LongValue, LongValue] {
  def transform(amount: LongValue): Future[Try[LongValue]]
}

trait BulkCurrencyConversion[C1 <: Currency, C2 <: Currency]
  extends Transformer[LongLongMapValue, LongLongMapValue] {

  def transform(amount: LongLongMapValue): Future[Try[LongLongMapValue]]
}


trait Reducer[V1 <: Value, V2 <: Value] extends Transformer[V1, V2] {
  def transform(value: V1): Future[Try[V2]]
}


trait SumGetter extends Reducer[StringLongMapValue, LongValue] {

  val context: QueryContext

  def fetchRate(): Future[Try[Double]]

  private def conversionHappened(rate: Double): Boolean = rate > 10e-7

  private def generatePaymentUpdate(contractId: String,
                                    termId: String,
                                    rate: Double,
                                    payments: Map[StringValue, LongValue]):JsObject = {
    val txObjs = payments.map { case (txHash, outputsSum) =>
      Json.obj("id" -> txHash.value, "amount" -> outputsSum.value.toString)
    }.toSeq

    val js0 = Json.obj(
      "node_id" -> VerificationServerConfig.nodeId,
      "contract" -> contractId,
      "term" -> termId,
      "transactions" -> JsArray(txObjs),
      "payment_currency" -> "BTC"
    )

    Json.obj("payment_update" -> (if (conversionHappened(rate)) {
      (js0 + ("exchange_rate" -> JsString(rate.toString))).
        +("target_currency" -> JsString("USD"))
    } else js0))
  }

  private def reportToFrontend(contractId: String,
                               termId: String,
                               rate: Double,
                               payments: Map[StringValue, LongValue]): Unit = {
    val reqText = generatePaymentUpdate(contractId, termId, rate, payments).toString()
    val path = VerificationServerConfig.frontAppPath + "api/terms/payments"
    val httpClient = new HttpClient()(AppRegistry.system, AppRegistry.monitoring)
    httpClient.post(path, reqText)
  }

  override def transform(value: StringLongMapValue): Future[Try[LongValue]] = {
    fetchRate().map { exchangeRateTry =>
      exchangeRateTry match {
        case Failure(e) => Failure(e)
        case Success(exchangeRate) =>
          val contractId = context.contractId

          val time = System.currentTimeMillis()
          val termId = context.termName
          val payments = value.value

          if(payments.nonEmpty) {
            reportToFrontend(contractId, termId, exchangeRate, payments)

            val paymentsDone = StoragesImpl.payments.getPayments(contractId, termId)
            val paymentsDoneKeys = paymentsDone.keySet

            val newPayments = payments.filter(t => !paymentsDoneKeys.contains(t._1.value))
              .map { case (paymentId, paymentAmount) =>
              val (tgCurrency: Currency, tgAmount: Double) = conversionHappened(exchangeRate) match {
                case true => (UsdImpl, paymentAmount.value * exchangeRate)
                case false => (BtcImpl, paymentAmount.value.toDouble)
              }
              val pd = PaymentDetails(BtcImpl, tgCurrency, paymentAmount.value, exchangeRate, tgAmount, time)
              StoragesImpl.payments.addPayment(contractId, termId, paymentId.value, pd)
              pd
            }

            val sum = (paymentsDone.values ++ newPayments).map(_.targetAmount).sum
            Success(LongValue(sum.toLong))
          } else Success(LongValue(0))
      }
    }
  }
}

trait BtcSumGetter extends SumGetter {
  override def fetchRate() = Future(Success(1.0))
}

trait UsdSumGetter extends SumGetter {
  override def fetchRate() = BtcUsdTickerClientCascade.fetchLastPrice().map(_.map { price =>
    price * 100.0 / 100000000.0
  })
}