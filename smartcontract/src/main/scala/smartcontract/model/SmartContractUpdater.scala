/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

trait SmartContractUpdater {
  def trackTerms(contract:SmartContract): Option[(SmartTermsSequence, TermState)] =
    contract.sequences.view.flatMap { case (sequence, state) =>
      if (!sequence.isFinished(state))
        sequence.doStep(state).map { newState => (sequence, newState) }
      else None
    }.headOption

  def termUpdate(contract:SmartContract,
                        seqName: String, 
                        newState: TermState, updateTime: Long): SmartContract =
    contract.sequences.find(_._1.name == seqName).map { case (sequence, _) =>
      contract.copy(sequences = contract.sequences.updated(sequence, newState), lastUpdate = updateTime)
    }.get
}

object SmartContractUpdaterImpl extends SmartContractUpdater
