/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import org.joda.time.DateTime
import smartcontract.model.MapDbDatabases.tempDiskCache
import smartcontract.utils.Logging


object QueryCache extends Logging {

  val lastQueries = tempDiskCache.createHashMap("queries").makeOrGet[Long, Long]()

  def get(q: Query[_], term: String): Long =
    Option(lastQueries.get(id(q, term)))
      .map { ts => log.debug(s"Got $q hit from cache time: $ts"); ts }
      .getOrElse(0)

  def put(q: Query[_], term: String) = {
    log.debug(s"Going to put $q into cache")
    lastQueries.put(id(q, term), DateTime.now.getMillis)
  }

  private def id(q: Query[_], term: String) = (q.queryId.toLong << 32) | (term.hashCode.toLong & 0xffffffffL)

  def remove(q: Query[_], term: String) = {
    log.debug(s"Going to remove $q from cache")
    lastQueries.remove(id(q, term))
  }
}