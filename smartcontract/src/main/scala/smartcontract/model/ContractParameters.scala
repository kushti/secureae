/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import play.api.libs.json.{JsObject, Json}
import smartcontract.utils.JsonHelpers._
import smartcontract.utils.crypto.{CryptographicHash, PasswordBasedCrypto}

import scala.util.Try


case class ContractParameters(unixStartTime: Long,
                              contractJson: JsObject,
                              outcomesJsonOpt: Option[JsObject],
                              privacy: ContractPrivacyOptions.Value,
                              passwordOpt: Option[String]) {

  lazy val startTime = unixStartTime * 1000

  lazy val contractText = contractJson.toString()

  lazy val hashHex = CryptographicHash.sha256HexEncoded(contractText)

  lazy val blockchainJson: JsObject = {
    privacy match {
      case ContractPrivacyOptions.Private =>
        Json.obj(
          "contract-hash" -> hashHex,
          "source" -> "smartcontract.com")

      case ContractPrivacyOptions.Public =>
        Json.obj(
          "contract" -> contractJson,
          "source" -> "smartcontract.com",
          "start-time" -> unixStartTime)
    }
  }

  lazy val blockchainText = blockchainJson.toString()
}

object ContractParameters {
  private def passwordCheck(passwordBase64: Option[String]): Boolean = {
    passwordBase64.nonEmpty && {
      val psi = PasswordBasedCrypto.splitBase64Password(passwordBase64.get)
      psi._1.length == PasswordBasedCrypto.PassLength &&
        psi._2.length == PasswordBasedCrypto.SaltLength &&
        psi._3.length == PasswordBasedCrypto.SaltLength
    }
  }

  def parse(outerJsonString: String): Try[ContractParameters] = Try {
    val outerJson = Json.parse(outerJsonString)
    val st = longFromJsValue(outerJson \ "start-time")
    val contractJson = (outerJson \ "contract").as[JsObject]
    val outcomesJson = (outerJson \ "outcomes").asOpt[JsObject]
    val privacy = ContractPrivacyOptions.fromString((outerJson \ "privacy").asOpt[String]).get
    val pass = (outerJson \ "password").asOpt[String]

    if (privacy == ContractPrivacyOptions.Private && !passwordCheck(pass)) {
      val err = s"No password provided for private contract or pass length != ${PasswordBasedCrypto.totalEncodedPassLength}"
      throw new IllegalArgumentException(err)
    }

    new ContractParameters(st, contractJson, outcomesJson, privacy, pass)
  }
}


object ContractPrivacyOptions extends Enumeration {
  val Private = Value(1, "private")
  val Public = Value(2, "public")

  val Default = Public

  def isPrivate(po: ContractPrivacyOptions.Value) = po == Private

  def fromString(option: Option[String]): Option[ContractPrivacyOptions.Value] = Try {
    option.map(withName).getOrElse(Default)
  }.toOption
}
