/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model.blockchain

import nxt.utils.TransactionTemplates.sendNonPrunableMultipartMessage
import nxt.{NxtFunctions, PrunableMessage, Transaction, TransactionQueryBuilder}
import play.api.libs.json.{JsObject, JsValue, Json}
import smartcontract.AppRegistry
import smartcontract.actors.monitoring.InternalError
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.model._
import smartcontract.utils.JsonHelpers._
import smartcontract.utils.Logging

import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.util._

case class StateUpdateInfo(contractId: String, termName: String, state: TermState)


object ContractsBlockchainDb extends Logging {
  private val CACHE_NAME = "contracts_to_write"

  private val cache = MapDbDatabases.persistentCache
    .createHashMap(CACHE_NAME)
    .makeOrGet[String, ContractParameters]()

  def fetchContracts(): Map[String, SmartContract] = fetchContracts(VerificationServerConfig.minHeight)

  def fetchContracts(since: Int): Map[String, SmartContract] = fetchTexts(since: Int) match {
    case Success(texts) =>
      val (contractJses, stateUpdateJses) = texts.flatMap { text =>
        Try(Json.parse(text)).toOption.flatMap { js =>
          (js \ "contract").asOpt[JsValue].map(_ => js)
        }
      }.partition(js => (js \ "start-time").asOpt[JsValue].isDefined)

      val stateUpdates = stateUpdateJses.flatMap { json =>
        val contractId = (json \ "contract").as[String]
        val ja = json \ "statuses"
        (ja \\ "term") zip (ja \\ "status") map { case (tj, sj) =>
          contractId -> StateUpdateInfo(contractId, tj.as[String], new TermState(sj.as[String]))
        }
      }

      val contracts = contractJses.map { contractJs =>
        val unixStartTime = longFromJsValue(contractJs \ "start-time")
        val privacy = ContractPrivacyOptions.Public
        val params = ContractParameters(unixStartTime, contractJs.as[JsObject], None, privacy, None)

        JsonToContract.apply((contractJs \ "contract").as[JsObject], params, SmartContractTrackingState.TRACKING)
      }.flatMap(_.toOption)

      contracts.map { case initContract =>
        val updates = stateUpdates.filter(_._1 == initContract.id).map(_._2)
        initContract.id -> updates.foldLeft(initContract) { case (contract, update) =>
          val sq = contract.sequences.find(_._1.name == update.termName).get._1
          contract.copy(sequences = contract.sequences.updated(sq, update.state))
        }
      }.toMap

    case Failure(e) =>
      AppRegistry.monitoring ! InternalError("fetchTexts failed, contact devs immediately!")
      Map()
  }

  def fetchTexts(since: Int): Try[Seq[String]] = Try {
    val pubId = VerificationServerConfig.blockchainDbWorker.getId

    val pms = PrunableMessage.getPrunableMessages(pubId, 0, Integer.MAX_VALUE).iterator().map { pm =>
      pm.toString
    }.toSeq

    //old format fetching below
    val txs = new TransactionQueryBuilder()
      .withHeightMoreThan(since)
      .withSender(pubId)
      .withType(1, 0)
      .query()
      .get

    val oldMsgs = txs.flatMap { tx =>
      val msg = Option(tx.getMessage).map(_.getMessage).map(ba => new String(ba)).getOrElse("")
      fetchMultiPartMessage(tx, Some(msg))
    }

    oldMsgs ++ pms
  }

  //todo: will be not needed after 1.5.x mainnet release
  def fetchMultiPartMessage(tx: Transaction, msgOpt: Option[String]): Option[String] = {
    def fetchStep(tx: Transaction): Option[String] = {
      val txTry = new TransactionQueryBuilder()
        .withReferenceToTransaction(tx)
        .query()

      val msgTxOpt = txTry match {
        case Success(txAsItr) =>
          txAsItr.find(t => Option(t.getMessage).isDefined)
        case Failure(e) => e.printStackTrace()
          None
      }

      msgTxOpt.map { tx =>
        val s = new String(tx.getMessage.getMessage)
        s + fetchStep(tx).getOrElse("")
      }
    }

    msgOpt.orElse {
      Option(tx.getMessage)
        .map(_.getMessage)
        .map(b => new String(b))
    }.map {
      _ + fetchStep(tx).getOrElse("")
    }
  }

  def publishContractText(id: String, params: ContractParameters): Try[Long] = {
    log.info(s"Going to publish a contract into blockchain: $id \n ${params.blockchainJson}")

    cache.put(id, params)
    MapDbDatabases.persistentCache.commit()
    publishText(params.blockchainText)
  }

  def checkContractsBeingWritten(): Unit = {
    val currentTime = System.currentTimeMillis()

    val since = NxtFunctions.currentHeight - 3000

    val messagesSince = fetchTexts(since).getOrElse(Seq())

    cache.keySet().foreach { contractId =>
      val params = cache.get(contractId)
      val deltaMillis = currentTime - params.startTime
      if (deltaMillis >= 3.hours.toMillis) {
        val toFind = ContractPrivacyOptions.isPrivate(params.privacy) match {
          case false => s"""id":"$contractId"""
          case true => params.hashHex
        }

        val contains = messagesSince.exists (_.contains(toFind))

        if (contains) {
          log.info(s"Removing from cache contract being published: $contractId")
          cache.remove(contractId)
        } else {
          log.warn(s"Republishing the contract failed to be written: $contractId")
          publishText(params.blockchainText)
        }
      }
    }
  }

  def publishText(text: String): Try[Long] = {
    log.debug(s"Going to publish $text into the chain")
    sendNonPrunableMultipartMessage(VerificationServerConfig.blockchainDbWorkerPhrase, text.trim).map(_.getId)
  }

  def contractIdsBeingCheckedForPublishing():Set[String] = cache.keySet().toSet
}