/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import smartcontract.utils.Logging


sealed trait Action {
  def process()
}

case object NoAction extends Action {
  override def process() {}

  override def toString = " nop action "
}

case class SendNxt(receiver: NxtAddress, amountNqt: Long) extends Action {
  def process() = ???
}

case class SendBitcoins(receiver: BtcAddress, amountSatoshis: Long) extends Action with Logging {
  def process() = log.info("sending bitcoins")
}

object ActionBuilder {
  def payment(receiver: Address, amount: Long): Action = receiver match {
    case na: NxtAddress => SendNxt(na, amount)
    case ba: BtcAddress => SendBitcoins(ba, amount)
    case _ => throw new IllegalStateException()
  }
}


