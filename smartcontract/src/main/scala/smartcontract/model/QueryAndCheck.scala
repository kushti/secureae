/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import smartcontract.utils.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

case class QueryContext(contractId: String, contractStartTime: Long, termName: String)


case class QueryAndCheck[V <: Value](query: Query[V],
                                     predicate: Predicate[V],
                                     context: QueryContext) extends Logging {

  def process(): Future[Boolean] = {
    if (QueryCache.get(query, context.termName) + query.interval.toMillis < System.currentTimeMillis) {
      query.query().map { resTry =>
        resTry match {
          case Success(res) =>
            QueryCache.put(query, context.termName)
            predicate.matchPattern(res)
          case Failure(e) =>
            log.error("Query failed because of ", e)
            false
        }
      }
    } else Future(false)
  }

  override def toString = s"$query $predicate"
}


object QueryAndCheckBuilder {

  def withRealEstate(re: RealEstate, qCtx: QueryContext): QueryAndCheck[_ <: Value] = {
    val q = CalifornianRealEstateQuery(re.block, re.lot, qCtx)
    val p = StringSeqsPartOf(re.grantorWords -> re.granteeWords)
    QueryAndCheck(q, p, qCtx)
  }

  def withSeoRanking(rr: RankingRange, qCtx: QueryContext): QueryAndCheck[_ <: Value] =
    QueryAndCheck(GoogleRankQuery(rr.geo, rr.phrase, rr.domain, qCtx), WithinRange(rr.min -> rr.max), qCtx)

  def withExpectedPayment(ep: ExpectedPayment, qCtx: QueryContext): QueryAndCheck[_ <: Value] =
    (ep.sender, ep.receiver) match {
      case (senderOpt: Option[NxtAddress], nr: NxtAddress) =>
        QueryAndCheck(NxtNoConversionPaymentQuery(senderOpt, nr, qCtx), NotLessThan(ep.amount), qCtx)

      case (senderOpt: Option[BtcAddress], br: BtcAddress) =>
        ep.amountCurrency match {
          case Some(u: Usd) =>
            QueryAndCheck(BitcoinToUsdPaymentQuery(senderOpt, br, qCtx),
              NotLessThan(ep.amount),
              qCtx)
          case None =>
            QueryAndCheck(BitcoinNoConversionPaymentQuery(senderOpt, br, qCtx),
              NotLessThan(ep.amount),
              qCtx)
          case _ => throw new IllegalStateException()
        }
      case _ => throw new IllegalStateException()
    }

  def deadline(timestamp: Long, qCtx: QueryContext) =
    QueryAndCheck(TimestampQuery(qCtx), NotLessThan(timestamp), qCtx)
}
