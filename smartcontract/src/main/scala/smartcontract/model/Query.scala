/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.model

import nxt.TransactionQueryBuilder
import smartcontract.AppRegistry
import smartcontract.actors.monitoring.ExternalError
import smartcontract.api.bitcoin.BitcoinTrackerRouter
import smartcontract.api.seo.SemrushClient
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.scrapers.CriisComScraper
import smartcontract.utils.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.util.{Failure, Random, Success, Try}


sealed abstract class Query[V <: Value] {
  lazy val interval: Duration = VerificationServerConfig.getDuration(intervalSettingName)
  val queryId: Int
  protected val context: QueryContext
  protected val intervalSettingName: String

  def query(): Future[Try[V]]
}

case class TimestampQuery(override val context: QueryContext) extends Query[LongValue] {
  import StandardTypesConversions._

  override lazy val queryId = Random.nextInt()
  override protected val intervalSettingName = "contract-deadline-check-interval"

  override def query() = Future(Success(System.currentTimeMillis()))

  override def toString = "Timestamp"
}

trait BlockchainQuery[V1 <: Value, V2 <: Value]
  extends Query[V2] with Transformer[V1, V2]

abstract class BitcoinPaymentQuery(senderOpt: Option[BtcAddress],
                                   recipient: BtcAddress,
                                   override val context: QueryContext)
  extends BlockchainQuery[StringLongMapValue, LongValue]
  with Transformer[StringLongMapValue, LongValue] {

  override lazy val queryId = (context.contractId + context.termName + recipient).hashCode()
  override protected val intervalSettingName = "btc-payment-check-interval"

  override def query(): Future[Try[LongValue]] = {

    val startTime = context.contractStartTime
    val mapTry = BitcoinTrackerRouter.received(recipient.address,
      senderOpt.map(_.address),
      startTime,
      VerificationServerConfig.bitcoinConfirmations)

    val txsTry: Try[StringLongMapValue] =
      mapTry.map(_.map { case (k, v) => StringValue(k) -> LongValue(v) }).map(StringLongMapValue.apply)

    txsTry match {
      case Success(txs) =>
        transform(txs)
      case Failure(e) =>
        e.printStackTrace()
        AppRegistry.monitoring ! ExternalError(s"Problems with Bitcoin tracking: ${e.getMessage}")
        Future(Failure(e))
    }
  }
}

case class BitcoinNoConversionPaymentQuery(senderOpt: Option[BtcAddress],
                                           recipient: BtcAddress,
                                           override val context: QueryContext)
  extends BitcoinPaymentQuery(senderOpt, recipient, context) with BtcSumGetter {

  override def toString = s"Bitcoins paid(from $senderOpt to $recipient)"
}


case class BitcoinToUsdPaymentQuery(senderOpt: Option[BtcAddress],
                                    recipient: BtcAddress,
                                    override val context: QueryContext)
  extends BitcoinPaymentQuery(senderOpt, recipient, context) with UsdSumGetter {
  override def toString = s"USD in form of Bitcoins paid(from $senderOpt to $recipient)"
}

abstract class NxtPaymentQuery(senderOpt: Option[NxtAddress],
                               recipient: NxtAddress,
                               override val context: QueryContext)
  extends BlockchainQuery[LongValue, LongValue] {

  override lazy val queryId = (context.contractId + recipient).hashCode()
  override protected val intervalSettingName = "nxt-payment-check-interval"

  override def query() = transform(LongValue {
    val qb0 = new TransactionQueryBuilder
    senderOpt.map(sender => qb0.withSender(sender.address)).getOrElse(qb0)
      .withRecipient(recipient.address)
      .withType(0, 0).query()
      .map(_.map(_.getAmountNQT).sum).getOrElse(0: Long)
  })

  override def toString = s"Nxts paid(from $senderOpt to $recipient)"
}


case class NxtNoConversionPaymentQuery(senderOpt: Option[NxtAddress],
                                       recipient: NxtAddress,
                                       override val context: QueryContext)
  extends NxtPaymentQuery(senderOpt, recipient, context) with NoTransformation[LongValue]


abstract class SeoQuery[V <: Value] extends Query[V]

case class GoogleRankQuery(geo: String,
                           phrase: String,
                           domain: String,
                           override val context: QueryContext) extends SeoQuery[LongValue] with Logging {
  import StandardTypesConversions._

  override lazy val queryId = (context.contractId + phrase + domain).hashCode
  override protected val intervalSettingName = "ranking-check-interval"

  override def query(): Future[Try[LongValue]] = Future {
    SemrushClient.positionCascade(context.contractId, domain, phrase, geo) match {
      case Success(positionOpt) => Success(positionOpt.getOrElse(Long.MaxValue):Long)
      case Failure(t) => Failure(new Exception("Got no position from SemrushClient.positionCascade call", t))
    }
  }
}

abstract class RealEstateQuery[V <: Value] extends Query[V]

case class CalifornianRealEstateQuery(block: String,
                                      lot: String,
                                      override val context: QueryContext) extends RealEstateQuery[StringSeqsValue] {

  override lazy val queryId = (context.contractId + block + lot).hashCode
  override protected val intervalSettingName = "ciirs-check-interval"

  override def query() = {
    val startTime = context.contractStartTime
    CriisComScraper.scrapeDeeds(block, lot, startTime).map(_.map(StringSeqsValue.apply))
  }
}