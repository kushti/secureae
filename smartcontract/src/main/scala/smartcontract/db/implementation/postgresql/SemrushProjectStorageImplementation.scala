/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import slick.backend.DatabasePublisher
import slick.driver.PostgresDriver.api._
import smartcontract.db.implementation.postgresql.PostgresTableRecords._
import smartcontract.db.interfaces.{SemrushProject, SemrushProjectStorage}
import smartcontract.utils.Logging

import scala.util.Try


abstract class SemrushProjectStorageImplementation extends SemrushProjectStorage
with StorageImplementation[SemrushProjectRecord] with Logging {

  override protected val TableName = "semrush_project"
  override protected val table = TableQuery[SemrushProjectTable]

  override def addSemrushProject(project: SemrushProject): Unit = Try {
    val a = table +=(None, project.projectId, project.contractId, project.key)
    await(db.run(a))
  }.recover { case t: Throwable =>
    log.error("Error while inserting contract", t)
  }

  override def removeSemrushProject(project: SemrushProject): Unit = Try {
    val a = table.filter(_.projectId === project.projectId).delete
    await(db.run(a))
  }.recover { case t: Throwable =>
    log.error("Error while inserting contract", t)
  }

  override def semrushProjects(): DatabasePublisher[SemrushProject] = {
    val q = table.map(r => (r.projectId, r.contractId, r.keyword))
    db.stream(q.result).mapResult(deserialize)
  }

  private def deserialize(tuple3: (String, String, String)): SemrushProject =
    SemrushProject(tuple3._1, tuple3._2, tuple3._3)

  override def projectsByContractId(contractId: String): Seq[SemrushProject] = {
    val a = table
      .filter(_.contractId === contractId)
      .map(r => (r.projectId, r.contractId, r.keyword))
      .result

    await(db.run(a)).map(deserialize)
  }

  protected class SemrushProjectTable(tag: Tag) extends Table[SemrushProjectRecord](tag, TableName) {
    //columns
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def projectId = column[String]("project_id")
    def contractId = column[String]("contract_id")
    def keyword = column[String]("key")

    def * = (id.?, projectId, contractId, keyword)

    //secondary indexes
    def pidx = index("idx_prj_id", projectId, unique = true)
    def cidx = index("idx_sp_contract_id", contractId, unique = false)
  }
}


object SemrushProjectStorageProduction extends SemrushProjectStorageImplementation
with StorageImplementationProduction[SemrushProjectRecord]

object SemrushProjectStorageTest extends SemrushProjectStorageImplementation
with StorageImplementationTest[SemrushProjectRecord]