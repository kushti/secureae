/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import smartcontract.db.implementation.postgresql.PostgresTableRecords._
import smartcontract.db.interfaces.ContractUpdateStorage

import slick.driver.PostgresDriver.api._


abstract class ContractUpdateStorageImplementation extends ContractUpdateStorage
  with StorageImplementation[ContractUpdateTableRecord] {

  override protected val TableName = "contract_update"
  override protected val table = TableQuery[ContractUpdateTable]

  override def putStatusUpdate(contractId: String, termName: String, state:String, time: Long): Unit =
      await(db.run(table += (None, contractId, termName, state, time)))

  override def getContractUpdates(contractId: String): Seq[(String, String, Long)] = {
    val q = table
      .filter(_.contractId === contractId)
      .map(t => (t.termName, t.state, t.time))
    await(db.run(q.result))
  }

  protected class ContractUpdateTable(tag: Tag) extends Table[ContractUpdateTableRecord](tag, TableName) {
    def * = (id, contractId, termName, state, time)

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def contractId = column[String]("contract_id")

    def termName = column[String]("term_name")

    def state = column[String]("state")

    def time = column[Long]("time")
  }
}


object ContractUpdateStorageProduction extends ContractUpdateStorageImplementation
  with StorageImplementationProduction[ContractUpdateTableRecord]

object ContractUpdateStorageTest extends ContractUpdateStorageImplementation
  with StorageImplementationTest[ContractUpdateTableRecord]
