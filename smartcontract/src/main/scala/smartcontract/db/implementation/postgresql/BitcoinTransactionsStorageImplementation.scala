/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import org.bitcoinj.core.Transaction
import org.bitcoinj.params.MainNetParams
import smartcontract.db.implementation.postgresql.PostgresTableRecords._
import smartcontract.db.interfaces.{ActionId, BitcoinTransactionsStorage, GlobalTermId}

import slick.driver.PostgresDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global


abstract class BitcoinTransactionsStorageImplementation extends BitcoinTransactionsStorage with StorageImplementation[TransactionTableRecord] {

  override protected val TableName = "transaction"
  override protected val table = TableQuery[TransactionsTable]

  def putTransaction(idWithTx:(ActionId, Transaction)):Unit = putTransaction(idWithTx._1, idWithTx._2)

  override def putTransaction(transactionId: ActionId, transaction: Transaction): Unit = {
    val compositeTermId = transactionId.termId

    val a = table.filter(_.contractId === compositeTermId.contractId)
      .filter(_.termId === compositeTermId.termId)
      .filter(_.success === transactionId.success)
      .map(_.latest).update(false)

    await(db.run(a))

    val time = System.currentTimeMillis()
    val txBytes = transaction.bitcoinSerialize()
    val t = (None, compositeTermId.contractId, compositeTermId.termId, transactionId.success, time, txBytes, true)
    await(db.run(table += t))
  }

  override def getTransaction(transactionId: ActionId): Option[Transaction] = {

    val a = table.filter(_.contractId === transactionId.termId.contractId)
      .filter(_.termId === transactionId.termId.termId)
      .filter(_.success === transactionId.success)
      .filter(_.latest === true)
      .map(_.bytes)
      .result
      .headOption

    await(db.run(a).map(_.map(deserializeTx)))
  }

  override def getContractTransactions(contractId: String): Map[ActionId, Transaction] = {
    val q = table.filter(_.contractId === contractId)
      .filter(_.latest === true)
      .map(t => (t.contractId, t.termId, t.success, t.bytes))

    val ts = await(db.run(q.result))

    ts.map(t => ActionId(GlobalTermId(t._1, t._2), t._3) -> deserializeTx(t._4)).toMap
  }

  override def getAllVersions(transactionId: ActionId): Map[TimeStamp, Transaction] = {
    val q = table.filter(_.contractId === transactionId.termId.contractId)
      .filter(_.termId === transactionId.termId.termId)
      .filter(_.success === transactionId.success)
      .sortBy(_.id.desc)
      .map(t => t.timestamp -> t.bytes)

    await(db.run(q.result)).map(t => t._1 -> deserializeTx(t._2)).toMap
  }

  private def deserializeTx(bytes: Array[Byte]) = new Transaction(MainNetParams.get(), bytes)

  protected class TransactionsTable(tag: Tag) extends Table[TransactionTableRecord](tag, TableName) {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def contractId = column[String]("contract_id")

    def termId = column[String]("termId_id")

    def success = column[Boolean]("success")

    def timestamp = column[Long]("timestamp")

    def bytes = column[Array[Byte]]("transaction_bytes")

    def latest = column[Boolean]("latest")

    def * = (id, contractId, termId, success, timestamp, bytes, latest)
  }
}


object BitcoinTransactionsStorageProduction extends BitcoinTransactionsStorageImplementation
  with StorageImplementationProduction[TransactionTableRecord]

object BitcoinTransactionsStorageTest extends BitcoinTransactionsStorageImplementation
  with StorageImplementationTest[TransactionTableRecord]