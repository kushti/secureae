/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import slick.driver.PostgresDriver.api._
import slick.lifted.TableQuery
import smartcontract.actors.p2p.{ContractDistributionEventType, ContractDistributionEvent}
import smartcontract.db.implementation.postgresql.PostgresTableRecords._
import smartcontract.db.interfaces.ContractDistributionEventStorage
import smartcontract.model.SmartContract
import SmartContract.ContractId
import smartcontract.utils.Logging

import scala.util.Try


abstract class ContractDistributionEventStorageImplementation
  extends ContractDistributionEventStorage
    with StorageImplementation[ContractDistributionEventTableRecord]
    with Logging {

  override protected val TableName = "contract_distrib_events"
  override protected val table = TableQuery[ContractDistributionEventTable]

  override def addEvent(event: ContractDistributionEvent): Unit = Try {
    val a = table.+=((None, event.contractId, event.eventType.id.toByte, event.nodePubkey, event.timestamp))
    await(db.run(a))
  }.recover { case t: Throwable =>
    log.error("Error while inserting contract", t)
  }


  override def eventsByContract(contractId: ContractId): Seq[ContractDistributionEvent] = {
    val q = table.filter(_.contractId === contractId)
    await(db.run(q.result)).map(deserialize)
  }

  private def deserialize(row:ContractDistributionEventTableRecord):ContractDistributionEvent =
    ContractDistributionEvent(row._2, row._4, ContractDistributionEventType(row._3), row._5)

  override def eventsByContractAndEventType(contractId: ContractId,
                                            eventTypeId:Byte): Seq[ContractDistributionEvent] = {
    val q = table.filter(_.contractId === contractId).filter(_.eventTypeId === eventTypeId)
    await(db.run(q.result)).map(deserialize)
  }

  protected class ContractDistributionEventTable(tag: Tag)
    extends Table[ContractDistributionEventTableRecord](tag, TableName) {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def contractId = column[String]("contract_id")

    def eventTypeId = column[Byte]("event_id")

    //todo: re-consider after "How do we determine node ID" issue solving
    def nodePubkey = column[Array[Byte]]("node_pk")

    def timestamp = column[Long]("timestamp")

    def * = (id.?, contractId, eventTypeId, nodePubkey, timestamp)
  }
}


object ContractDistributionEventStorageProduction extends ContractDistributionEventStorageImplementation
  with StorageImplementationProduction[ContractDistributionEventTableRecord]

object ContractDistributionEventStorageTest extends ContractDistributionEventStorageImplementation
  with StorageImplementationTest[ContractDistributionEventTableRecord]
