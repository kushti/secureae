/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import slick.driver.PostgresDriver
import slick.driver.PostgresDriver.api._
import slick.jdbc.meta.MTable
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.db.interfaces.Storage
import smartcontract.utils.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

trait StorageImplementation[TR] extends Storage with Logging {
  protected val db: PostgresDriver.backend.DatabaseDef
  protected val TableName: String
  protected val table: TableQuery[_ <: Table[TR]]

  def await[T](f: Future[T]): T = Await.result(f, 1.minute) //todo: make configurable

  protected def init() = await {
    log.debug("Initializing table " + TableName)

    db.run(MTable.getTables(TableName).headOption).map(to =>
      if (to.isEmpty) {
        log.debug("Creating table " + TableName)
        db.run(table.schema.create)
      })
  }

  override def size(): Int = await(db.run(table.length.result))
}

trait StorageImplementationProduction[TR] extends StorageImplementation[TR] {
  override val db = Database.forConfig("db.sc.production", VerificationServerConfig.conf)

  log.debug("database path: " + VerificationServerConfig.getString("db.sc.production.url"))

  init()
}

trait StorageImplementationTest[TR] extends StorageImplementation[TR] {
  override val db = Database.forURL("jdbc:postgresql://localhost/smartcontracts_test",
    user = "postgres",
    password = "postgres",
    driver = "org.postgresql.Driver")

  override def init() = {
    super.init()
    clear()
  }

  def clear() = await {
    db.run(MTable.getTables(TableName).headOption).map { to =>
      if (to.nonEmpty) {
        await(db.run(table.schema.drop))
        await(db.run(table.schema.create))
      }
    }
  }

  init()
}