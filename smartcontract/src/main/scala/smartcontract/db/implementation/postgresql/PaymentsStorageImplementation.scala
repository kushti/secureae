/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import slick.driver.PostgresDriver.api._
import smartcontract.db.implementation.postgresql.PostgresTableRecords.PaymentTableRecord
import smartcontract.db.interfaces.{PaymentDetails, PaymentsStorage}
import smartcontract.model.Currency

import scala.concurrent.ExecutionContext.Implicits.global

abstract class PaymentsStorageImplementation extends PaymentsStorage
  with StorageImplementation[PaymentTableRecord] {

  override protected val TableName = "payment"
  override protected val table = TableQuery[PaymentsTable]

  override def addPayment(contractId: String,
                          termId: String,
                          paymentId: String,
                          paymentDetails: PaymentDetails): Unit = {
    val record = (None, paymentId, contractId, termId, paymentDetails.currency.id,
      paymentDetails.targetCurrency.id, paymentDetails.amount, paymentDetails.conversionRate,
      paymentDetails.targetAmount, paymentDetails.paymentTime)
    await(db.run(table += record))
  }

  override def getPayments(contractId: String, termId: String): Map[PaymentId, PaymentDetails] = {
    val a = table.filter(ps => ps.contractId === contractId && ps.termId === termId).result
    await(db.run(a).map(_.map(t=> t._2 -> extractPaymentDetails(t)))).toMap
  }

  override def getContractPayments(contractId: String): Map[(TermId, PaymentId), PaymentDetails] = {
    val a = table.filter(ps => ps.contractId === contractId).result
    await(db.run(a).map(_.map(t=> (t._2, t._4) -> extractPaymentDetails(t)))).toMap
  }

  private def extractPaymentDetails(rec: PaymentTableRecord): PaymentDetails =
    PaymentDetails(Currency.fromId(rec._5), Currency.fromId(rec._6), rec._7, rec._8, rec._9, rec._10)

  protected class PaymentsTable(tag: Tag) extends Table[PaymentTableRecord](tag, TableName) {
    //columns
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def paymentId = column[String]("external_id")

    def contractId = column[String]("contract_id")

    def termId = column[String]("term_id")

    def currencyId = column[Byte]("currency_id")

    def targetCurrencyId = column[Byte]("target_currency_id")

    def amount = column[Long]("amount")

    def conversionRate = column[Double]("conversion_rate")

    def targetAmount = column[Double]("target_amount")

    def paymentTime = column[Long]("time")

    def * = (id, paymentId, contractId, termId, currencyId, targetCurrencyId,
      amount, conversionRate, targetAmount, paymentTime)

    //secondary indexes
    def uniq = index("payment_uniq_constraint", (paymentId, contractId, termId), unique = true)
  }
}


object PaymentsStorageProduction extends PaymentsStorageImplementation
  with StorageImplementationProduction[PaymentTableRecord]

object PaymentsStorageTest extends PaymentsStorageImplementation
  with StorageImplementationTest[PaymentTableRecord]