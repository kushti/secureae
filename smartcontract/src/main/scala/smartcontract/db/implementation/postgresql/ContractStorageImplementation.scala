/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.implementation.postgresql

import slick.backend.DatabasePublisher
import slick.driver.PostgresDriver.api._
import smartcontract.db.implementation.postgresql.PostgresTableRecords.ContractsTableRecord
import smartcontract.db.interfaces.ContractStorage
import smartcontract.model.{SmartContractTrackingState, SmartContract, ContractPrivacyOptions}
import smartcontract.utils.Logging

import scala.util.Try

abstract class ContractStorageImplementation extends ContractStorage
  with StorageImplementation[ContractsTableRecord]
  with Logging {

  override protected val TableName = "contract"
  override protected val table = TableQuery[ContractsTable]

  override def updateContract(contract: SmartContract): Unit = addContract(contract)

  override def addContract(contract: SmartContract): Unit = Try {
    require((contract.contractParams.privacy == ContractPrivacyOptions.Private &&
      contract.contractParams.passwordOpt.isDefined) ||
      (contract.contractParams.privacy == ContractPrivacyOptions.Public))

    val contractId = contract.id
    val a = table.filter(_.contractId === contractId).map(_.latest).update(false)
    await(db.run(a))

    val timeStamp = contract.lastUpdate
    val json = SmartContract.serialize(contract).toString()
    val isFinished = contract.finished

    val privacy = contract.contractParams.privacy.toString
    val passwordOpt = contract.contractParams.passwordOpt
    val text = contract.contractParams.contractText
    val hash = contract.contractParams.hashHex
    val trackingState = contract.trackingState.id.toByte

    val a2 = table.+=(None, contractId, timeStamp, isFinished, json, privacy, passwordOpt, text, hash, trackingState, true)
    await(db.run(a2))
  }.recover {
    case t: Throwable => log.error("Error while inserting contract", t)
  }

  override def getContract(id: String): Option[SmartContract] = {
    val q = table.filter(_.contractId === id)
      .filter(_.latest === true)
      .take(1)
      .map(_.json)

    await(db.run(q.result.headOption)).map(SmartContract.deserialize)
  }

  override def getAllVersions(id: String): Map[TimeStamp, SmartContract] = {
    val q = table.filter(_.contractId === id)
      .sortBy(_.timestamp.desc)
      .map(t => t.timestamp -> t.json)

    await(db.run(q.result)).map(t => t._1 -> SmartContract.deserialize(t._2)).toMap
  }

  override def iterator(): DatabasePublisher[SmartContract] =
    db.stream(table.map(_.json).result).mapResult(SmartContract.deserialize)

  protected lazy val activeFilter = table
    .filter(_.latest === true)
    .filter(_.finished === false)
    .filter(_.trackingState === SmartContractTrackingState.TRACKING.id.toByte)

  override def activeContracts(): DatabasePublisher[SmartContract] =
    db.stream(activeFilter.map(_.json).result).mapResult(SmartContract.deserialize)

  override def activeSize(): Int = await(db.run(activeFilter.length.result))

  private def pendingFilter = table
    .filter(_.latest === true)
    .filter(_.finished === false)
    .filter(_.trackingState === SmartContractTrackingState.PENDING.id.toByte)
  
  def pendingContracts(): DatabasePublisher[SmartContract] =
    db.stream(pendingFilter.map(_.json).result).mapResult(SmartContract.deserialize)
  
  def pendingContractIds(): Seq[String] = await(db.run(pendingFilter.map(_.contractId).result))

  protected class ContractsTable(tag: Tag) extends Table[ContractsTableRecord](tag, TableName) {
    //columns
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def contractId = column[String]("contract_id")

    def timestamp = column[Long]("time")

    def finished = column[Boolean]("finished")

    def json = column[String]("contract_json")

    def privacy = column[String]("privacy")

    def passwordOpt = column[Option[String]]("password")

    def text = column[String]("contract_text")

    def hash = column[String]("contract_hash")

    def trackingState = column[Byte]("tracking_state")

    def latest = column[Boolean]("latest")

    def * = (id.?, contractId, timestamp, finished, json, privacy, passwordOpt, text, hash, trackingState, latest)

    //secondary indexes
    def idx = index("idx_contract_id", contractId, unique = false)
  }
}


object ContractStorageProduction extends ContractStorageImplementation
  with StorageImplementationProduction[ContractsTableRecord]

object ContractStorageTest extends ContractStorageImplementation
  with StorageImplementationTest[ContractsTableRecord]