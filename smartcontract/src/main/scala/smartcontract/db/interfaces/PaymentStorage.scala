/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.interfaces

import smartcontract.model.Currency


case class PaymentDetails(currency: Currency,
                          targetCurrency: Currency,
                          amount: Long,
                          conversionRate: Double,
                          targetAmount: Double,
                          paymentTime: Long)

trait PaymentsStorage extends Storage {
  type PaymentId = String

  def addPayment(contractId: String, termId: String, paymentId: String, paymentDetails: PaymentDetails): Unit

  def addPayment(globalTermId: GlobalTermId, paymentId: String, paymentDetails: PaymentDetails): Unit =
    addPayment(globalTermId.contractId, globalTermId.termId, paymentId, paymentDetails)

  def getPayments(contractId: String, termId: String): Map[PaymentId, PaymentDetails]

  def getPayments(globalTermId: GlobalTermId): Map[PaymentId, PaymentDetails] =
    getPayments(globalTermId.contractId, globalTermId.termId)

  def getContractPayments(contractId: String): Map[(TermId, PaymentId), PaymentDetails]
}
