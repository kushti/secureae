/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.interfaces

import slick.backend.DatabasePublisher
import smartcontract.model.SmartContract

trait ContractStorage extends Storage {

  def addContract(contract: SmartContract)

  def updateContract(contract: SmartContract)

  def getContract(id: String): Option[SmartContract]

  def getAllVersions(id: String): Map[TimeStamp, SmartContract]

  def iterator(): DatabasePublisher[SmartContract]

  def activeContracts(): DatabasePublisher[SmartContract]

  def pendingContracts(): DatabasePublisher[SmartContract]

  def pendingContractIds(): Seq[String]

  //todo: queries to get pending/aborted contracts

  def activeSize(): Int
}
