/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.interfaces

import smartcontract.actors.p2p.ContractDistributionEvent
import smartcontract.model.SmartContract
import SmartContract.ContractId


trait ContractDistributionEventStorage extends Storage {
  def addEvent(event: ContractDistributionEvent)

  def eventsByContract(contractId: ContractId): Seq[ContractDistributionEvent]

  def eventsByContractAndEventType(contractId: ContractId, eventTypeId:Byte): Seq[ContractDistributionEvent]
}
