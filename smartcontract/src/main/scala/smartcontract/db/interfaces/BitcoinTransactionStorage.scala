/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.interfaces

import org.bitcoinj.core.Transaction

case class ActionId(termId: GlobalTermId, success: Boolean)


trait BitcoinTransactionsStorage extends Storage {

  def putTransaction(transactionId: ActionId, transaction: Transaction)

  def getTransaction(transactionId: ActionId): Option[Transaction]

  def getAllVersions(transactionId: ActionId): Map[TimeStamp, Transaction]

  def getContractTransactions(contractId: String): Map[ActionId, Transaction]
}
