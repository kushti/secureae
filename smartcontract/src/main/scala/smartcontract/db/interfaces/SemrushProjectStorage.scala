/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.db.interfaces

import slick.backend.DatabasePublisher

case class SemrushProject(projectId: String, contractId: String, key: String)

trait SemrushProjectStorage extends Storage {

  def addSemrushProject(project: SemrushProject): Unit

  def removeSemrushProject(project: SemrushProject): Unit

  def semrushProjects(): DatabasePublisher[SemrushProject]

  /**
   * As contract could have multiple terms, it's possible contract could be associated with multiple projects
   * @param contractId - id of a contract
   * @return Sequence(maybe empty) of projects associated with a contract
   */
  def projectsByContractId(contractId: String): Seq[SemrushProject]
}
