package smartcontract.bootstrap

import smartcontract.AppRegistry
import smartcontract.actors.{SmartContractSlaveListener, CheckContracts}
import smartcontract.api.http.{ConsensusRoute, SmartContractHttpRoute}
import smartcontract.api.seo.SemrushClient
import smartcontract.model.blockchain.ContractsBlockchainDb
import smartcontract.utils.db.RefTxsHashIndexCreator
import spray.routing.RouteConcatenation

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import ExecutionContext.Implicits.global


object SmartContractBootstrap extends BootstrapFunctions
  with SmartContractHttpRoute with ConsensusRoute with RouteConcatenation {

  override val concreteBootstrap = () => {
    implicit val asys = AppRegistry.system

    //apply index for transaction referencing
    RefTxsHashIndexCreator.applyUpdate()

    val m = AppRegistry.bitcoinSigner.mode
    log.debug(s"Bitcoin signing utility is started in mode $m")

    val nodeId = VerificationServerConfig.nodeId
    require(!nodeId.isEmpty && nodeId.length < 50, "Node id is empty or more than 50 chars")

    val ordinary = (('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')).toSet
    require(nodeId.forall(ordinary.contains), "Node id could be made only of english chars & numbers")

    //Start listening for contracts written into chain by master
    //todo: can some blocks be downloaded w/out listening?
    SmartContractSlaveListener.start()

    //todo: make delays configurable
    asys.scheduler.schedule(0.minute, 1.minute)(AppRegistry.contractsTracker ! CheckContracts)

    //contracts republishing scheduling
    //todo: make timing params configurable
    asys.scheduler.schedule(10.minutes, 30.minutes)(ContractsBlockchainDb.checkContractsBeingWritten())

    //semrush projects deletion for finished projects
    asys.scheduler.schedule(2.hours, 2.hours)(SemrushClient.removeOutdatedProjects())

    log.debug(s"Peer networking is launched ${AppRegistry.networkController}")
  }

  override lazy val specificRouting = smartContractRoute ~ consensusRoute
}

