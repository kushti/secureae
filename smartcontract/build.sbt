import com.typesafe.sbt.SbtStartScript

name := "smartcontract"

version := "0.4.0"

libraryDependencies ++= (Seq(
  "org.bitcoinj" % "bitcoinj-core" % "0.13.1",

  "org.jsoup" % "jsoup" % "1.8.2",

  "org.apache.commons" % "commons-lang3" % "3.4",
  "joda-time" % "joda-time" % "2.7",
  "com.github.nscala-time" %% "nscala-time" % "2.0.0",
  "org.joda" % "joda-convert" % "1.7",
  "com.google.guava" % "guava" % "18.0",

  //test dependencies below
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % "test",
  "org.scalatest" %% "scalatest" % "2.2.5" % "test",
  "org.scalactic" % "scalactic_2.11" % "2.2.1" % "test",
  "io.spray" %% "spray-testkit" % "1.3.3" % "test"
) ++ Dependencies.db ++ Dependencies.spray).map(_.force())

libraryDependencies ~= { _.map(_.exclude("org.slf4j", "slf4j-jdk14")) }



javaOptions += "-Xms1G"

javaOptions += "-Xmx4G"

javaOptions += "-server"

Seq(SbtStartScript.startScriptForClassesSettings: _*)