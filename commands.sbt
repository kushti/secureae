val startSaeMainnet = taskKey[Unit]("Start SAE over Nxt Mainnet")
val startSaeTestnet = taskKey[Unit]("Start SAE over Nxt Testnet")
val startMainnet = taskKey[Unit]("Start Sc over Nxt Mainnet")
val startTestnet = taskKey[Unit]("Start Sc over Nxt Testnet")


def runClass(params: Seq[String]): Unit = {
  s"./run-class.sh ${params.mkString(" ")}".!
}

startSaeMainnet := runClass(Seq("sae", "sae.MainnetLauncher"))


startSaeTestnet := runClass(Seq("sae", "sae.TestnetLauncher"))


startMainnet := runClass(Seq("smartcontract", "smartcontract.MainnetLauncher"))


startTestnet := runClass(Seq("smartcontract", "smartcontract.TestnetLauncher"))
