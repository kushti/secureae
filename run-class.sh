#!/bin/sh

rm nohup.out

rm -rf target/

sbt "project $1" clean compile start-script

# e.g. to run data importing, smartcontract.utils.db.FrontendDataImporter
$1/target/start $2