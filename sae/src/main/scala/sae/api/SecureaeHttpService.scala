/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.api

import java.util.concurrent.TimeUnit

import nxt._
import org.joda.time.DateTimeZone
import play.api.libs.json.{JsArray, JsValue, Json}
import sae.NxtUtils._
import sae.api.JsonUtils._
import sae.logic.JsonExtractors
import sae.model.{AssetOwners, _}
import smartcontract.utils.Logging
import spray.caching.{Cache, LruCache}
import spray.http.StatusCodes._
import spray.http._
import spray.routing.HttpService._

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

trait SecureaeHttpService extends Logging {

  val cache: Cache[JsValue] = LruCache(
    maxCapacity = 100,
    initialCapacity = 16,
    timeToLive = Duration.create(30, TimeUnit.MINUTES),
    timeToIdle = Duration.create(10, TimeUnit.MINUTES)
  )

  //clear cache on block coming
  lazy val cacheCleaner = new BlockchainListener {
    override def notify(b: Block): Unit = {
      val h = b.getHeight
      val wv = computeWeeklyVol()
      log.info("Invalidating HTTP API cache on coming block @ height " + h)
      cache.clear()
      cache("wvall")(wv)
    }
  }

  lazy val secureAeRoute =
    path("trades" / Segment / IntNumber) {
      case (assetIdUnsigned, num) =>
        get {
          log.debug(s"Got /trades request: $assetIdUnsigned $num")

          completeFutureJsValue(cache("t" + assetIdUnsigned) {
            val assetId = fromUnsigned(assetIdUnsigned)
            implicit val tradesWriter = tradeWrites(asset(assetId))
            val trades = Trade.getAssetTrades(assetId, 0, num - 1).iterator().toList
            Json.toJson(trades)
          })
        }
    } ~
      path("asks" / Segment) {
        case assetIdUnsigned =>
          get {
            log.debug(s"Got /asks request: $assetIdUnsigned")
            orders(assetIdUnsigned)
          }
      } ~
      path("bids" / Segment) {
        case assetIdUnsigned =>
          get {
            log.debug(s"Got /bids request: $assetIdUnsigned")
            orders(assetIdUnsigned)
          }
      } ~
      path("assets") {
        get {
          log.debug(s"Got /assets request")
          val assets = allAssets()
          respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
            _.complete(Json.toJson(assets).toString())
          }
        }
      } ~
      path("assetOwners" / Segment) {
        case assetIdUnsigned =>
          get {
            log.debug(s"Got /assetOwners request")

            val assetId = fromUnsigned(assetIdUnsigned)
            val aoi = AssetOwners(assetId)

            respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
              _.complete(Json.toJson(aoi).toString())
            }
          }
      } ~
      path("assetOwners" / Segment / IntNumber) {
        case (assetIdUnsigned, height) =>
          get {
            log.debug(s"Got /assetOwners request: $assetIdUnsigned $height")

            val aid = fromUnsigned(assetIdUnsigned)
            val aoi = DistributionDatabase.getOrExtractDistribution(aid, height)

            respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
              _.complete(Json.toJson(aoi).toString())
            }
          }
      } ~
      path("dividendsToPay" / Segment / LongNumber) {
        case (assetIdUnsigned: String, amount: Long) =>
          get {
            log.debug(s"Got /dividendsToPay request: $assetIdUnsigned $amount")

            val assetId = fromUnsigned(assetIdUnsigned)
            val aoi = AssetOwners(assetId)
            DistributionDatabase.saveDistribution(aoi)
            val divs = aoi.spreadDividends(amount, payToIssuer = false)

            respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
              _.complete(Json.toJson(divs).toString())
            }
          }
      } ~
      path("lastcandles" / Segment / Segment / IntNumber) {
        case (assetIdUnsigned: String, stepStr: String, howMany: Int) =>
          get {
            log.debug(s"Got /lastcandles request: $assetIdUnsigned $stepStr $howMany")

            completeFutureJsValue(cache("lc" + assetIdUnsigned + stepStr + howMany) {
              val assetId = fromUnsigned(assetIdUnsigned)
              implicit val candlesWriter = candleWriters(asset(assetId))

              val step = CandleStep.withName(stepStr.toUpperCase)

              val candles = Candle.last(assetId, step, howMany)
              Json.toJson(candles)
            })
          }
      } ~
      path("weeklyvol" / Segment) {
        case assetIdUnsigned: String =>
          get {
            log.debug(s"Got /weeklyvol request: $assetIdUnsigned")

            completeFutureJsValue(cache("wv" + assetIdUnsigned) {
              val assetId = fromUnsigned(assetIdUnsigned)
              val a = asset(assetId)
              val (qnts, nqts) = weeklyVolsQntNqt(assetId)
              Json.obj(
                "volumeQNT" -> decDiv(qnts, a),
                "volumeNQT" -> nqts
              )
            })
          }
      } ~
      path("weeklyvol") {
        get {
          dynamic {
            log.debug(s"Got /weeklyvol request for all the assets")
            completeFutureJsValue(cache("wvall")(computeWeeklyVol()))
          }
        }
      } ~
      path("contract" / Segment) {
        case assetIdUnsigned: String =>
          get {
            log.debug(s"Got /contract request: $assetIdUnsigned")

            val assetId = fromUnsigned(assetIdUnsigned)
            val cpOpt = ClauseParams.byAssetId(assetId)

            val js = cpOpt match {
              case Some(cp) =>
                val now = org.joda.time.DateTime.now
                Json.obj(
                  "status" -> "ok",
                  "asset_id" -> toUnsigned(cp.assetId),
                  "dividends_payment_account" -> nxt.util.Convert.rsAccount(cp.dividendsPayerId),
                  "start_time" -> cp.startTime.withZone(DateTimeZone.UTC).toString,
                  "end_time" -> cp.endTime.withZone(DateTimeZone.UTC).toString,
                  "promised" -> cp.dividendsPromised.toString,
                  "paid" -> cp.dividendsPaid.toString,
                  "finished" -> cp.finished(now),
                  "completed" -> cp.completed
                )
              case None =>
                Json.obj("status" -> "error", "details" -> "No contract found")
            }

            respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
              _.complete(js.toString())
            }
          }
      } ~
      path("contracts") {
        get {
          log.debug(s"Got /contracts request")

          val cs = ClauseParams.all.map(_.assetId).map(java.lang.Long.toUnsignedString).mkString("[", ",", "]")
          respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
            _.complete(cs)
          }
        }
      } ~
      path("txmessage" / Segment) {
        case assetIdUnsigned: String =>
          get {
            log.debug(s"Got /txmessage request: $assetIdUnsigned")

            val assetId = fromUnsigned(assetIdUnsigned)
            NxtFunctions.transactionById(assetId) match {
              case None => complete("No tx!")
              case Some(tx) => complete(new String(tx.getMessage.getMessage))
            }
          }
      } ~
      path("extractClause" / Segment) {
        case assetIdUnsigned: String =>
          get {
            val assetId = fromUnsigned(assetIdUnsigned)
            val txs = NxtFunctions.transactionById(assetId).toSeq

            val clause = txsContaining(txs, "clause")
              .filter(_._1.getAttachment.getTransactionType == TransactionType.ColoredCoins.ASSET_ISSUANCE)
              .flatMap(t => JsonExtractors.parseClauseJson(t._2).map(pp => t._1 -> pp))
              .map(t => ClauseParams(t._2, t._1))
              .headOption
            complete(clause.toString)
          }
      } ~
      path("extractClause" / Segment / IntNumber) {
        case (assetIdUnsigned: String, height: Int) =>
          get {
            val assetId = fromUnsigned(assetIdUnsigned)
            val distribOpt = DistributionDatabase.getDistribution(assetId, height)

            val jsonResult = distribOpt match {
              case Some(distrib) => Json.toJson(distrib)
              case None => Json.obj("status" -> "error",
                "details" -> s"no database record for $assetIdUnsigned - $height")
            }
            respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
              _.complete(jsonResult.toString())
            }
          }
      }

  private def orders(assetIdUnsigned: String) = {
    val assetId = fromUnsigned(assetIdUnsigned)

    implicit val ordersWriter = orderWriters(asset(assetId))

    val orders = getOrders(assetId)
    respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
      _.complete(Json.toJson(orders).toString())
    }
  }

  private def weeklyVolsQntNqt(assetId: Long): (Long, Long) = {
    val candles = Candle.last(assetId, CandleStep.Hour, 7 * 24)
    (candles.map(_.volumeQNT).sum, candles.map(_.volumeNQT).sum)
  }

  private def completeFutureJsValue(jsFut: => Future[JsValue]) =
    onComplete(jsFut) {
      case Success(value) => respondWithHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
        _.complete(value.toString())
      }
      case Failure(ex) => complete(InternalServerError, s"An error occurred: ${ex.getMessage}")
    }

  private def computeWeeklyVol(): JsArray = {
    JsArray(allAssets().map {
      asset =>
        val (qnts, nqts) = weeklyVolsQntNqt(asset.getId)
        Json.obj(
          "asset" -> toUnsigned(asset.getId),
          "volumeQNT" -> decDiv(qnts, asset),
          "volumeNQT" -> nqts
        )
    })
  }
}
