/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.api

import nxt.{Asset, Order, Trade}
import org.joda.time.DateTime
import play.api.libs.json._
import sae.NxtUtils._
import sae.model.{Share, AssetOwners, Candle}

object JsonUtils {

  implicit val shareWriters = new Writes[Share] {
    def writes(share: Share): JsValue = Json.obj(
      "account" -> toRs(share.accountId),
      "shares" -> share.shares,
      "percent" -> share.percentage,
      "percentNoIssuer" -> (share.percentageNoIssuer.map(d => d.toString).getOrElse("n/a"): String),
      "dividend" -> (share.dividendsToPay.map(_.toString).getOrElse("n/a/"): String)
    )
  }

  implicit val assetOwnersWriters: Writes[AssetOwners] = new Writes[AssetOwners] {
    def writes(assetOwners: AssetOwners): JsValue = {
      val total = assetOwners.totalAssets
      Json.obj(
        "id" -> toUnsigned(assetOwners.assetId),
        "issuer" -> toRs(assetOwners.issuerShare.accountId),
        "total" -> total.toString,
        "totalWithoutIssuer" -> (total - assetOwners.issuerShare.shares).toString,
        "height" -> assetOwners.height,
        "issuer" -> assetOwners.issuerShare,
        "others" -> assetOwners.otherShares
      )
    }
  }

  implicit val assetWrites = new Writes[Asset] {
    def writes(asset: Asset): JsValue = {
      Json.obj(
        "asset" -> toUnsigned(asset.getId),
        "symbol" -> asset.getName,
        "issuer" -> toRs(asset.getAccountId),
        "description" -> asset.getDescription,
        "decimals" -> asset.getDecimals.toInt
      )
    }
  }


  def candleWriters(asset: Asset) = new Writes[Candle] {
    def writes(c: Candle): JsValue =
      Json.obj(
        "time" -> c.startTime / 1000,
        "trades" -> c.tradesCount,
        "open" -> decMult(c.open, asset),
        "high" -> decMult(c.high, asset),
        "low" -> decMult(c.low, asset),
        "close" -> decMult(c.close, asset),
        "volumeQNT" -> decDiv(c.volumeQNT, asset),
        "volumeNQT" -> c.volumeNQT
      )
  }


  def orderWriters(asset: Asset) = new Writes[Order] {
    def writes(o: Order): JsValue =
      Json.obj(
        "id" -> toUnsigned(o.getId),
        "account" -> toRs(o.getAccountId),
        "amount" -> decDiv(o.getQuantityQNT, asset),
        "price" -> o.getPriceNQT
      )
  }

  def tradeWrites(asset: Asset) = new Writes[Trade] {
    def writes(t: Trade): JsValue = {
      val humanTime = new DateTime(fromNxtTime(t.getTimestamp)).toString

      Json.obj(
        "asset" -> toUnsigned(t.getAssetId),
        "quantity" -> decDiv(t.getQuantityQNT, asset),
        "price" -> decMult(t.getPriceNQT, asset),
        "timestamp" -> t.getTimestamp / 1000,
        "height" -> t.getHeight,
        "humantime" -> humanTime,
        "askOrder" -> toUnsigned(t.getAskOrderId),
        "bidOrder" -> toUnsigned(t.getBidOrderId)
      )
    }
  }

  private implicit def intPow(a: Int) = new {
    private def pow(a: Int, b: Int, acc: Int): Int =
      b match {
        case 0 => acc
        case _ => pow(a, b - 1, a * acc)
      }

    def pow(b: Int): Int = {
      require(b >= 0, "Exponent must be a non-negative integer.")
      pow(a, b, 1)
    }

    def ^^(b: Int): Int = pow(b)
  }

  def decMult(num: Long, asset: Asset) = num * (10 ^^ asset.getDecimals)

  def decDiv(num: Long, asset: Asset) = num / (10 ^^ asset.getDecimals)
}