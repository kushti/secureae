/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.bootstrap

import akka.actor.Props
import sae.api.SecureaeHttpService
import sae.logic.{TradesWatcher, BootstrapCandlesBuilding, SecureAeListener}
import sae.model.ClauseParams
import smartcontract.bootstrap.BootstrapFunctions
import smartcontract.utils.Logging

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

object SaeBootstrap extends BootstrapFunctions with SecureaeHttpService with Logging {

  override val concreteBootstrap: () => Unit = () => {
    ClauseParams.bootstrap()
    val tradesWatcher = system.actorOf(Props[TradesWatcher])
    tradesWatcher ! BootstrapCandlesBuilding
    Future(SecureAeListener.start())

    cacheCleaner.start()

    log.info("SAE bootstrap is done")
  }

  override lazy val specificRouting = secureAeRoute
}
