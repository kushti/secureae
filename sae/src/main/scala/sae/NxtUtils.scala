/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae

import nxt.util.Convert
import nxt.utils.TxSeqUtils
import nxt.{Order, Transaction, Constants}

import scala.collection.JavaConversions._

object NxtUtils {
  val OneNxt = Constants.ONE_NXT
  val Deadline: Short = 120

  def allAssets(): Seq[nxt.Asset] =
    nxt.Asset.getAllAssets(0, Integer.MAX_VALUE).iterator().toList

  def asset(id: Long) = nxt.Asset.getAsset(id)

  def txsContaining(txs: Iterable[Transaction], s: String) =
    TxSeqUtils.withTextMessage(txs).map(tx => tx -> new String(tx.getMessage.getMessage)).filter(_._2.contains(s))

  def getOrders(assetId: Long): Set[Order] = getOrders(assetId, ask = true) ++ getOrders(assetId, ask = false)

  def getOrders(assetId: Long, ask: Boolean): Set[Order] =
    if (ask) Order.Ask.getSortedOrders(assetId, 0, -1).iterator().toSet
    else Order.Bid.getSortedOrders(assetId, 0, -1).iterator().toSet

  def fromUnsigned(s: String): Long = Convert.parseUnsignedLong(s).toLong

  def toUnsigned(l: Long) = java.lang.Long.toUnsignedString(l)

  def toRs(l: Long) = Convert.rsAccount(l)

  def fromNxtTime(secondsFromGenesis: Long): Long = Constants.EPOCH_BEGINNING + secondsFromGenesis * 1000
}
