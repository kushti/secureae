/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.logic

import nxt.Constants
import sae.NxtUtils
import NxtUtils.fromUnsigned
import org.joda.time.DateTime
import play.api.libs.json.{JsValue, Json}


object JsonExtractors {
  def parseDividendsJson(jsonString: String): Option[ParsedDividendParams] =
    try {
      val json: JsValue = Json.parse(jsonString)

      val tipe = (json \ "type").as[String]
      if (tipe.toLowerCase.startsWith("dividend")) {
        val height = (json \ "height").as[Int]
        val total = (json \ "total").as[String].toLong
        val assetId = fromUnsigned((json \ "contractId").as[String])
        Some(ParsedDividendParams(assetId, height, total))
      } else None
    } catch {
      case t: Throwable => t.printStackTrace()
        None
    }

  def parseClauseJson(jsonString: String): Option[ParsedClauseParams] =
    try {
      val json: JsValue = Json.parse(jsonString)

      val contents = (json \ "contents").as[String]
      if (contents != "clause") None
      else {

        val startTimeString = (json \ "datetime_start_utc").as[String]
        val startTime = DateTime.parse(startTimeString)

        val endTimeString = (json \ "datetime_end_utc").as[String]
        val endTime = DateTime.parse(endTimeString)

        val sum = (json \ "total_contract_amount").as[String].toLong * Constants.ONE_NXT

        Some(ParsedClauseParams(startTime, endTime, sum))
      }
    } catch {
      case t: Throwable => t.printStackTrace()
        None
    }

}
