/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.logic

import nxt.{Block, BlockchainListener, TransactionType}
import sae.NxtUtils._
import sae.model.ClauseParams
import smartcontract.utils.Logging

import scala.collection.JavaConversions._


object SecureAeListener extends BlockchainListener with Logging {

  import JsonExtractors._

  override def notify(b: Block) {
    updateHeight(b.getHeight)
    checkClauses(b)
    checkDividends(b)
  }

  def updateHeight(height: Int) {
    log.debug("New block: " + height)
  }

  def checkClauses(b: Block) {
    val clauses = txsContaining(b.getTransactions, "clause")
      .filter(_._1.getAttachment.getTransactionType == TransactionType.ColoredCoins.ASSET_ISSUANCE)
      .flatMap(t => parseClauseJson(t._2).map(pp => t._1 -> pp))
      .map(t => ClauseParams(t._2, t._1)).toSeq

    val cs = ClauseParams.all.map(_.assetId).map(java.lang.Long.toUnsignedString).mkString(",")
    log.debug(s"Clauses found at height ${b.getHeight}: ${clauses.length}, all clauses: $cs")
    clauses.foreach(cp => ClauseParams.save(cp))
  }

  def checkDividends(b: Block) {
    DividendsChecker.checkDividends(b.getTransactions)
  }
}
