/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.logic

import sae.NxtUtils
import NxtUtils._
import nxt.util.Convert
import nxt.{Transaction, TransactionQueryBuilder}
import sae.model.{ClauseParams, DistributionDatabase}
import smartcontract.utils.Logging

import scala.util.{Failure, Success, _}


object DividendsChecker extends Logging {
  def checkHistoryDividends(issuerId: Long) = Try {
    new TransactionQueryBuilder().withSender(issuerId).withType(0).withPlainMessage().query() match {
      case Success(txs) => checkDividends(txs.toSeq)
      case Failure(e) => e.printStackTrace()
    }
  }

  def checkDividends(txs: Iterable[Transaction]) {
    import JsonExtractors._

    txsContaining(txs, "dividend").foreach {
      case (tx, msg) =>
        parseDividendsJson(msg).foreach {
          case dp =>
            val assetId = dp.assetId
            val aid = java.lang.Long.toUnsignedString(assetId)

            if (Option(nxt.Asset.getAsset(assetId)).isDefined) {

              val txAmt = tx.getAmountNQT
              val assetOwners = DistributionDatabase.getOrExtractDistribution(dp.assetId, dp.distribHeight)
              val ownerIdOpt = tx.getRecipientId

              val shareInfoOpt = assetOwners.otherShares.find(_.accountId == ownerIdOpt)

              shareInfoOpt match {
                case None => log.debug(s"Contract $aid: ${toRs(ownerIdOpt)} doesn't own any assets})")
                case Some(shareInfo) =>
                  val diff = shareInfo.percentageNoIssuer.get * dp.bunchNqtAmount - txAmt

                  //100 nqt max error
                  if (Math.abs(diff) < 100) {
                    ClauseParams.byAssetId(assetId).foreach {
                      clause =>
                        val timestamp = Convert.fromEpochTime(tx.getTimestamp)

                        if (timestamp <= clause.endTime.getMillis && timestamp >= clause.startTime.getMillis) {

                          val updatedClause = clause.copy(dividendsPaid = clause.dividendsPaid + txAmt)
                          ClauseParams.save(updatedClause)
                          log.debug(s"Contract $aid:  updated")
                        } else {
                          log.debug(s"Contract $aid: payment ${java.lang.Long.toUnsignedString(tx.getId)} is out of time")
                        }
                    }
                  } else {
                    log.warn(s"Contract $aid: too big error. ShareInfo: $shareInfo, " +
                      s"total: ${assetOwners.totalAssets} Bunch: ${dp.bunchNqtAmount}, TxAmp: $txAmt")
                  }
              }
            } else {
              log.warn(s"Contract $aid: no asset found")
            }
        }
    }
  }
}
