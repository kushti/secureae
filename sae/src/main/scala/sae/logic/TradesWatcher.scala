/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.logic

import akka.actor.{Actor, Props}
import akka.routing.RoundRobinPool
import nxt.{Asset, Trade}
import smartcontract.utils.Logging
import scala.collection.JavaConversions._


object BootstrapCandlesBuilding

class TradesWatcher extends Actor with Logging {
  val candleBuilders = context.system.actorOf(Props[CandleBuilder].withRouter(RoundRobinPool(3)))

  lazy val tradeListener = new nxt.util.Listener[Trade] {
    override def notify(t: Trade): Unit = {
      log.debug(s"Got new trade for ${t.getAssetId}")
      println(s"Got new trade for ${t.getAssetId}")
      val diff = TradesDiff(t.getAssetId, Seq(t))
      candleBuilders ! diff
    }
  }

  override def receive = {
    case BootstrapCandlesBuilding =>
      log.info("Building SAE candles")
      val allAssets = Asset.getAllAssets(0, Integer.MAX_VALUE).iterator().toList

      allAssets foreach { asset =>
        val aid = asset.getId
        val newTrades = Trade.getAssetTrades(aid, 0, Integer.MAX_VALUE).iterator().toList
        val diff = TradesDiff(aid, newTrades)
        candleBuilders ! diff
      }

      Trade.addListener(tradeListener, Trade.Event.TRADE)
      log.info("Candles building done")

    case a: Any => log.error("Wrong signal received: " + a)
  }
}