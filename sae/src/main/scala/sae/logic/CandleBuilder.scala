/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.logic

import akka.actor.Actor
import nxt.Trade
import nxt.util.Convert
import org.joda.time.DateTime
import sae.model.{Candle, CandleStep}
import smartcontract.utils.Logging

case class TradesDiff(assetId: Long, trades: Seq[Trade])

class CandleBuilder extends Actor with Logging {
  override def receive = {
    case TradesDiff(assetId, trades) =>
      log.debug(s"Got trades for $assetId, size of collection is: " + trades.size)
      trades.foreach { tr =>
        CandleStep.values.foreach { step =>
          val ct = Candle.candleStart(Convert.fromEpochTime(tr.getTimestamp), step)

          log.debug(s"Going to add or merge a candle for step $step, " +
            s"asset ${java.lang.Long.toUnsignedString(assetId)}, candle time ${new DateTime(ct)}")

          Candle.byTime(assetId, step, ct) match {
            case Some(c) =>
              Candle.put(c + tr)
            case None =>
              Candle.put(Candle(tr, step))
          }
        }
      }

    case a: Any => log.error("Wrong signal received: " + a)
  }
}