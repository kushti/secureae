/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.model

import org.mapdb.BTreeMap
import sae.NxtUtils
import sae.db.SaeMapDbDatabases.memCache
import scala.collection.JavaConversions._

trait CandleOperations {
  def put(c: Candle)

  def range(assetId: Long, step: CandleStep.Value, startTime: Long, endTime: Long): Seq[Candle]

  def since(assetId: Long, step: CandleStep.Value, time: Long): Seq[Candle]

  def last(assetId: Long, step: CandleStep.Value): Option[Candle]

  def last(assetId: Long, step: CandleStep.Value, howMany: Int): Seq[Candle]

  def byTime(assetId: Long, step: CandleStep.Value, time: Long): Option[Candle]

  def all(assetId: Long, step: CandleStep.Value): Seq[Candle]

  def count(assetId: Long, step: CandleStep.Value): Long
}


trait CandleOperationsMapDb extends CandleOperations {

  private def candlesMap(assetId: Long, step: CandleStep.Value) = {
    val assetIdUnsigned = NxtUtils.toUnsigned(assetId)
    memCache.createTreeMap(s"c-$assetIdUnsigned-${step.id}").makeOrGet[Long, Candle]()
  }

  private def withCandlesMap[T](assetId: Long, step: CandleStep.Value)(fn: BTreeMap[Long, Candle] => T): T =
    fn(candlesMap(assetId, step))

  override def put(c: Candle) {
    withCandlesMap(c.assetId, c.step)(_.put(c.startTime, c))
    memCache.commit()
  }

  override def range(assetId: Long, step: CandleStep.Value, startTime: Long, endTime: Long): Seq[Candle] =
    withCandlesMap(assetId, step)(_.subMap(startTime, endTime).values().toSeq)

  override def since(assetId: Long, step: CandleStep.Value, time: Long): Seq[Candle] =
    withCandlesMap(assetId, step)(_.tailMap(time).values().toSeq)

  override def last(assetId: Long, step: CandleStep.Value): Option[Candle] =
    withCandlesMap(assetId, step)(_.lastOption.map(_._2))

  override def last(assetId: Long, step: CandleStep.Value, howMany: Int): Seq[Candle] = {
    val to = Candle.candleStart(System.currentTimeMillis(), step, 1)
    val from = Candle.candleStart(System.currentTimeMillis(), step, -howMany + 1)
    withCandlesMap(assetId, step)(_.subMap(from, to).values().toSeq)
  }

  override def byTime(assetId: Long, step: CandleStep.Value, time: Long): Option[Candle] =
    Option(withCandlesMap(assetId, step)(_.get(time)))

  override def all(assetId: Long, step: CandleStep.Value): Seq[Candle] =
    withCandlesMap(assetId, step)(_.values().toSeq)

  override def count(assetId: Long, step: CandleStep.Value): Long =
    withCandlesMap(assetId, step)(_.size())
}