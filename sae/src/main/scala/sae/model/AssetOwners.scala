/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.model

import nxt.{Account, Nxt, NxtFunctions}

import scala.collection.JavaConversions._


case class AssetOwners(assetId: Long, height: Int, totalAssets: Long, issuerShare: Share, otherShares: Seq[Share]) {
  def spreadDividends(amountNqt: Long, payToIssuer: Boolean): AssetOwners = payToIssuer match {
    case true =>
      val td = totalAssets.toDouble
      val is = issuerShare.copy(dividendsToPay = Some(Math.round(issuerShare.shares / td * amountNqt)))
      val os = otherShares.map(os => os.copy(dividendsToPay = Some(Math.round(os.shares / td * amountNqt))))
      this.copy(issuerShare = is, otherShares = os)
    case false =>
      val td = (totalAssets - issuerShare.shares).toDouble
      val os = otherShares.map(os => os.copy(dividendsToPay = Some(Math.round(os.shares / td * amountNqt))))
      this.copy(otherShares = os)
  }

  def asMap(): Map[Long, Long] =
    otherShares.map(os => os.accountId -> os.shares).toMap.updated(issuerShare.accountId, issuerShare.shares)

  override def toString = {
    s"Asset: ${java.lang.Long.toUnsignedString(assetId)}, total assets: $totalAssets, " +
      s"height: $height, issuer assets: $issuerShare, other owners:" + otherShares.mkString("(", ",", ")")
  }
}

object AssetOwners {
  def apply(assetId: Long): AssetOwners = {
    val height = Nxt.getBlockchain.getHeight

    val distribMap = Account.getAllAccounts(0, Integer.MAX_VALUE).iterator().flatMap { acc =>
      val balance = acc.getAssetBalanceQNT(assetId)
      if (balance > 0) Some(acc.getId.toLong -> balance) else None
    }.toMap

    AssetOwners(assetId, height, distribMap)
  }

  def apply(assetId: Long, height: Int, distrib: Map[Long, Long]): AssetOwners = {
    val height = NxtFunctions.currentHeight
    val asset = nxt.Asset.getAsset(assetId)
    val total = asset.getQuantityQNT

    val totalDouble = total.toDouble

    val issuer = asset.getAccountId
    val issuerBalance = distrib.getOrElse(issuer, 0: Long)
    val issuerShares = Share(issuer, issuerBalance, issuerBalance / totalDouble, None, None)

    val totalNoIssuer = (total - issuerBalance).toDouble

    val otherShares = distrib.flatMap { case (accId, balance) =>
      if (balance > 0 && accId != issuer) {
        Some(Share(accId, balance, balance / totalDouble, Some(balance / totalNoIssuer), None))
      } else None
    }.toSeq

    AssetOwners(assetId, height, total, issuerShares, otherShares)
  }
}