/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.model

import nxt.Trade
import nxt.util.Convert
import org.joda.time.DateTime




case class Candle(assetId: Long, startTime: Long, step: CandleStep.Value,
                  tradesCount: Int, open: Long, close: Long, high: Long, low: Long,
                  volumeQNT: Long, volumeNQT: Long) extends Ordered[Candle] {

  def +(t: Trade): Candle = this.copy(
    tradesCount = this.tradesCount + 1,
    close = t.getPriceNQT,
    high = Math.max(this.high, t.getPriceNQT),
    low = Math.min(this.low, t.getPriceNQT),
    volumeQNT = this.volumeQNT + t.getQuantityQNT,
    volumeNQT = this.volumeNQT + t.getQuantityQNT * t.getPriceNQT)

  def +(that: Candle): Candle = Candle.merge(this, that)

  def ++(those: Seq[Candle]): Candle = those.foldLeft(this) { case (ac, c) => ac + c }

  def ==(that: Candle): Boolean = this.assetId == that.assetId &&
    this.startTime == that.startTime &&
    this.step == that.step &&
    this.tradesCount == that.tradesCount &&
    this.open == that.open &&
    this.close == that.close &&
    this.high == that.high &&
    this.low == that.low &&
    this.volumeQNT == that.volumeQNT &&
    this.volumeNQT == that.volumeNQT

  def !=(that: Candle): Boolean = !(this == that)

  override def toString = s"$assetId ${new DateTime(startTime)}($startTime) " +
    s"($step) Trades Count: $tradesCount Open: $open High: $high Low: $low Close: $close; " +
    s"Vol: $volumeQNT / $volumeNQT NQT"

  //overriding from Ordered[Candle] to order candles by time
  override def compare(that: Candle): Int = {
    require(this.step == that.step, "different steps - unable to compare")
    this.startTime.compareTo(that.startTime)
  }
}


object Candle extends CandleOperations with CandleOperationsMapDb {

  def candleStartAndEnd(t: Long, st: CandleStep.Value) = (candleStart(t, st), nextCandleStart(t, st))

  def candleStart(t: Long, st: CandleStep.Value): Long = (t / (1000l * st.id)) * (1000l * st.id)

  def candleStart(t: Long, st: CandleStep.Value, shift: Int): Long = candleStart(t, st) + 1000l * st.id * shift

  def nextCandleStart(t: Long, st: CandleStep.Value): Long = (t / (1000l * st.id) + 1) * (1000l * st.id)

  def candlesBetween(start: Long, end: Long, st: CandleStep.Value): Long = Math.abs(end / (1000l * st.id) - start / (1000l * st.id) - 1)

  def calculateEndTime(startTime: Long, step: CandleStep.Value): Long = startTime + 1000l * step.id

  def apply(cs: List[Candle], step: CandleStep.Value) = {
    require(cs.nonEmpty, "Empty list is not appropriate argument for Candle.apply()")
    val h = cs.head
    h.copy(step = step, startTime = Candle.candleStart(h.startTime, step)) ++ cs.tail
  }

  def apply(t: Trade, step: CandleStep.Value): Candle = {
    val p = t.getPriceNQT
    val st = Candle.candleStart(Convert.fromEpochTime(t.getTimestamp), step)
    Candle(t.getAssetId, st, step, 1, p, p, p, p, t.getQuantityQNT, t.getQuantityQNT * p)
  }

  def merge(first: Candle, second: Candle): Candle = {
    require(second.startTime < Candle.nextCandleStart(first.startTime, first.step))
    require(first.assetId == second.assetId,
      s"Different assets cannot be merged($first, $second)")
    require(first.startTime <= second.startTime,
      s"First candle's startTime should be not greater than second($first +++ $second)")

    val step = if (first.step >= second.step) first.step else second.step

    Candle(first.assetId,
      first.startTime,
      step,
      first.tradesCount + second.tradesCount,
      first.open,
      second.close,
      Math.max(first.high, second.high),
      Math.min(first.low, second.low),
      first.volumeQNT + second.volumeQNT,
      first.volumeNQT + second.volumeNQT)
  }
}