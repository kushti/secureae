/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.model

object CandleStep extends Enumeration {
  type CandleStep = Value

  // val Minute = Value(60, "M1")
  // val FiveMinutes = Value(300, "M5")
  // val TenMinutes = Value(600, "M10")
  // val FifteenMinutes = Value(900, "M15")
  //  val ThirtyMinutes = Value(1800, "M30")
  val Hour = Value(3600, "H1")
  val FourHours = Value(14400, "H4")
  val Day = Value(86400, "D1")

  val smallestCandle = Hour

  def getScaleFactor(cs1: CandleStep.Value, cs2: CandleStep.Value): Int = cs2.id / cs1.id

  def next(current: CandleStep.Value) = CandleStep.values.find(_.id > current.id)
}
