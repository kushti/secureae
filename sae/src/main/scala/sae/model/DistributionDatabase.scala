/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.model

import nxt.Attachment.ColoredCoinsAssetTransfer
import sae.NxtUtils
import NxtUtils._
import nxt.{Constants, Trade, Transaction, TransactionQueryBuilder}
import smartcontract.utils.Logging

import scala.collection.JavaConversions._
import sae.db.SaeMapDbDatabases.memCache

object DistributionDatabase extends Logging {

  val transfers = new TransactionQueryBuilder()
    .withHeightMoreThan(Constants.ASSET_EXCHANGE_BLOCK)
    .withType(2, 1)
    .query()
    .get
    .groupBy(_.getAttachment.asInstanceOf[ColoredCoinsAssetTransfer].getAssetId)

  def getOrExtractDistribution(assetId: Long, height: Int): AssetOwners =
    getDistribution(assetId, height).getOrElse {
      val startHeightOpt = getLastHeightBefore(assetId, height)
      val startHeight = startHeightOpt.getOrElse(Constants.ASSET_EXCHANGE_BLOCK) + 1

      val asset = nxt.Asset.getAsset(assetId)
      val issuer = asset.getAccountId

      val startDistrib = startHeightOpt match {
        case Some(h) =>
          getDistribution(assetId, h).get.asMap()
        case None =>
          Map(asset.getAccountId.toLong -> asset.getQuantityQNT)
      }

      val distrib = applyTransformations(assetId, startHeight, height, startDistrib)
      val issuerAssets = distrib.getOrElse(issuer, 0L)

      val aoi = AssetOwners(assetId, height, distrib)
      saveDistribution(aoi)
      log.debug(s"Generated distribution: $aoi")
      aoi
    }

  def saveDistribution(ao: AssetOwners) {
    val coll = distribMap(ao.assetId)
    coll.put(ao.height, ao)
  }

  private def distribMap(assetId: Long) = memCache.createTreeMap(toUnsigned(assetId)).makeOrGet[Int, AssetOwners]()

  def getDistribution(assetId: Long, height: Int): Option[AssetOwners] = Option(distribMap(assetId).get(height))

  def getLastHeightBefore(assetId: Long, height: Int): Option[Int] = Option(distribMap(assetId).floorKey(height - 1))

  def applyTransformations(assetId: Long, startHeight: Int, finishHeight: Int, distrib: Map[Long, Long]): Map[Long, Long] = {
    val assetTransfers = transfers.getOrElse(assetId, Seq[Transaction]()).groupBy(_.getHeight)

    val assetTrades = Trade.getAssetTrades(assetId, 0, Int.MaxValue).iterator().map { trade =>
      (trade.getHeight, trade.getQuantityQNT, trade.getSellerId, trade.getBuyerId)
    }.toSeq.groupBy(_._1)

    (startHeight to finishHeight).foldLeft(distrib) {
      case (hDistrib, h) =>

        val transferDistrib = assetTransfers.getOrElse(h, Seq[Transaction]()).foldLeft(hDistrib) {
          case (updDistrib, tx) =>
            val qnt = tx.getAttachment.asInstanceOf[ColoredCoinsAssetTransfer].getQuantityQNT
            val sender = tx.getSenderId
            val recipient = tx.getRecipientId
            val (senderQnt, rcpQnt) = if (sender != recipient) {
              (updDistrib.get(sender).map(_ - qnt).getOrElse(0L), updDistrib.getOrElse(recipient, 0L) + qnt)
            } else (updDistrib.getOrElse(sender, 0L), updDistrib.getOrElse(sender, 0L))

            updDistrib.updated(sender, senderQnt).updated(recipient, rcpQnt)
        }

        assetTrades.getOrElse(h, Seq()).foldLeft(transferDistrib) {
          case (updDistrib, tuple) =>
            val qnt = tuple._2
            val sender = tuple._3
            val recipient = tuple._4
            val (senderQnt, rcpQnt) = if (sender != recipient) {
              (updDistrib.get(sender).map(_ - qnt).getOrElse(0L), updDistrib.getOrElse(recipient, 0L) + qnt)
            } else (updDistrib.getOrElse(sender, 0L), updDistrib.getOrElse(sender, 0L))
            updDistrib.updated(sender, senderQnt).updated(recipient, rcpQnt)
        }
    }
  }
}