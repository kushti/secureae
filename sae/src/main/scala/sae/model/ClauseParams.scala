/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package sae.model

import nxt._
import org.joda.time.DateTime
import sae.logic.{ParsedClauseParams, JsonExtractors, DividendsChecker}
import smartcontract.utils.Logging
import sae.db.SaeMapDbDatabases.memCache

import scala.collection.JavaConversions._
import scala.util.{Failure, Success, Try}


case class ClauseParams(dividendsPayerId: Long, assetId: Long, startTime: DateTime, endTime: DateTime,
                        dividendsPromised: Long, dividendsPaid: Long) {
  def finished(time: DateTime) = time.compareTo(endTime) == 1

  def completed = dividendsPaid >= dividendsPromised
}


object ClauseParams extends Logging {

  private lazy val clauses = memCache.createHashMap(s"clauses").makeOrGet[Long, ClauseParams]()

  def bootstrap() = {
    val txsTry = new TransactionQueryBuilder()
      .withHeightMoreThan(Constants.ASSET_EXCHANGE_BLOCK)
      .withType(2, 0)
      .withPlainMessage()
      .query()

    txsTry match {
      case Success(txs) =>
        log.info("Issued assets with messages found: " + txs.size)
        val clauses = txs.flatMap {
          tx =>
            val msg = new String(tx.getMessage.getMessage, "UTF-8")
            if (msg.contains("clause")) {
              JsonExtractors.parseClauseJson(msg).map {
                pp =>
                  ClauseParams(pp, tx)
              }
            } else None
        }

        log.info("Clauses found: " + clauses.size)
        clauses.foreach(ClauseParams.save)

        val issuers = clauses.flatMap { cl =>
          Try(nxt.Asset.getAsset(cl.assetId).getAccountId).toOption
        }.toSet

        issuers.foreach { issuer =>
          DividendsChecker.checkHistoryDividends(issuer) match {
            case Success(_) => val iid = java.lang.Long.toUnsignedString(issuer)
              log.debug(s"Dividends check for issuer $iid has been finished successfully")
            case Failure(e) => log.error("Error while dividends check: ", new Exception(e));
          }
        }

        log.info("All dividends checks are done")
        val dpReport = ClauseParams.all.filter(_.dividendsPaid > 0).map(_.assetId).map(java.lang.Long.toUnsignedString)
        log.debug("Dividends paid for:" + dpReport)

      case Failure(e) => e.printStackTrace()
    }
  }

  def apply(pp: ParsedClauseParams, tx: Transaction) = {
    val dividendsPayer = tx.getSenderId
    val assetId = tx.getId
    new ClauseParams(dividendsPayer, assetId, pp.startTime, pp.endTime, pp.dividendsPromised, 0)
  }

  def save(clause: ClauseParams) = clauses.put(clause.assetId, clause)

  def all = clauses.values().iterator().toList

  def byAssetId(assetId: Long): Option[ClauseParams] = Option(clauses.get(assetId))
}