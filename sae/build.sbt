import com.typesafe.sbt.SbtStartScript

organization := "com.smartcontract"

name := "sae"

version := "0.4.0"


libraryDependencies ++= Dependencies.akka ++ Dependencies.spray ++ Seq(
  "org.mapdb" % "mapdb" % "1.0.8"
)


javaOptions += "-Xms1G"

javaOptions += "-Xmx4G"

javaOptions += "-server"

Seq(SbtStartScript.startScriptForClassesSettings: _*)