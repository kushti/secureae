/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.api

import akka.actor.{ActorRef, ActorSystem}
import smartcontract.actors.monitoring.ExternalError
import smartcontract.utils.Logging
import spray.can.client.HostConnectorSettings
import spray.client.pipelining._
import spray.http._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class HttpClient(implicit system: ActorSystem, monitoringActor: ActorRef) extends Logging {

  log.debug("Spray-client max redirects: " + HostConnectorSettings(system).maxRedirects)

  private val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

  def get(url: String): Future[Try[String]] = get(url, entity => entity.asString)

  def getBinary(url: String): Future[Try[Array[Byte]]] = get(url, entity => entity.data.toByteArray)

  def get(url: Uri): Future[Try[String]] = get(url, entity => entity.asString)

  def get[T](url: Uri, entityTransformation: HttpEntity => T): Future[Try[T]] =
    transformResponse(url, pipeline(Get(url)), entityTransformation)

  def getBinary(url: Uri): Future[Try[Array[Byte]]] = get(url, entity => entity.data.toByteArray)

  def post(url: String, content: String): Future[Try[String]] = post(url, content, entity => entity.asString)

  def post[T](url: String, content: String, entityTransformation: HttpEntity => T): Future[Try[T]] = {
    log.debug(s"Sending POST req to $url: $content")

    transformResponse(url,
      pipeline(HttpRequest(method = HttpMethods.POST,
        uri = Uri.apply(url),
        entity = HttpEntity(ContentTypes.`application/json`, content)
      )), entityTransformation)
  }

  def put(url: String, content: String): Future[Try[String]] = put(url, content, entity => entity.asString)

  def put[T](url: String, content: String, entityTransformation: HttpEntity => T): Future[Try[T]] = {
    log.debug(s"Sending PUT req to $url: $content")

    transformResponse(url,
      pipeline(HttpRequest(method = HttpMethods.PUT,
        uri = Uri.apply(url),
        entity = HttpEntity(ContentTypes.`application/json`, content)
      )), entityTransformation)
  }

  def delete(url: String): Future[Try[Unit]] = {
    log.debug(s"Sending DELETE req to $url")
    transformResponse(url,
      pipeline(HttpRequest(method = HttpMethods.DELETE, uri = Uri.apply(url))), entity => Unit)
  }

  private def transformResponse[T](url: Uri, fResp: Future[HttpResponse],
                                   entityTransformation: HttpEntity => T): Future[Try[T]] =
    fResp.map { r =>
      r.status.isSuccess match {
        case true => Success(entityTransformation(r.entity))
        case false => Failure(new Exception(s"Got HTTP code ${r.status.intValue} for $url : ${r.entity.asString}"))
      }
    }.recover { case t =>
      monitoringActor ! ExternalError(s"Error happened while requesting $url")
      log.warn(s"Error while requesting url: $url: ", t)
      Failure(t)
    }
}