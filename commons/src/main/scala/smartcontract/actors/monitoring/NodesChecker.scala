/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.monitoring

import akka.actor.{Actor, ActorRef}
import smartcontract.api.HttpClient
import smartcontract.bootstrap.VerificationServerConfig
import spray.http.Uri
import smartcontract.utils.Logging

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext.Implicits.global

case object CheckNodes

case class CheckNodesForList(nodes: Set[Uri])

class NodesChecker(monitoringActor: ActorRef) extends Actor with Logging {

  override def receive = {
    case CheckNodes => self ! CheckNodesForList(VerificationServerConfig.nodes)

    case CheckNodesForList(nodes) =>
      implicit val system = context.system

      val client = new HttpClient()
      val heights = TrieMap[String, Int]()

      nodes.map(_ + "/height").map { url =>
        client.get(url).map { rTry =>
          rTry.map(Integer.parseInt).toOption.orElse {
            monitoringActor ! InternalError(s"Node failed: $url")
            None
          } foreach { height =>
            heights += url -> height
            val maxHeight = heights.values.max
            if (maxHeight - height > 10) {
              monitoringActor ! InternalError(s"Node stuck: $url (at height $height)")
            }
          }
        }
      }

    case nonsense: Any => log.warn(s"Unknown signal arrived @ NodeChecker: $nonsense")
  }
}
