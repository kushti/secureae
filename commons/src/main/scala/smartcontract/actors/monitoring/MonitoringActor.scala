/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.actors.monitoring

import java.net.InetAddress

import akka.actor.Actor
import akka.actor.ActorSystem
import nxt.NxtFunctions._
import nxt.util.Convert
import nxt.{Nxt, NxtFunctions, Transaction}
import org.joda.time.DateTime
import play.api.libs.json.Json
import smartcontract.bootstrap.VerificationServerConfig
import smartcontract.utils.Logging
import smartcontract.api.HttpClient
import scala.collection.JavaConversions._


trait Event extends Logging {

  type Message = String

  val basket: String

  def check(): Option[Message]

  def booleanToMessageOption(bool: Boolean, msg: => Message) = bool match {
    case true => Some(s"$msg (${InetAddress.getLocalHost.getHostName})")
    case false => None
  }

  def blockchainTypePrefix: String = VerificationServerConfig.isTestnet match {
    case true => "testnet-"
    case false => ""
  }
}


object Event {
  val monitoringPanelUrl: String = VerificationServerConfig.monitoringPostUrl
}


trait CriticalEvent extends Event {
  override val basket = blockchainTypePrefix + "critical"
}

object OldUnconfirmedTransaction extends CriticalEvent {
  override def check() = {
    def filterFn(tx: Transaction) = {
      val threshold = new DateTime().minusMinutes(65).getMillis
      tx.getSenderId == VerificationServerConfig.blockchainDbWorker.getId &&
        Convert.fromEpochTime(tx.getTimestamp) < threshold
    }

    val longLiving = Nxt.getTransactionProcessor.getAllUnconfirmedTransactions
      .iterator().toList.filter(filterFn)

    lazy val llJsons = longLiving.foldLeft(new StringBuilder) { case (sb, tx) =>
      sb.append(tx.getJSONObject.toJSONString).append("                \n")
    }.mkString

    booleanToMessageOption(longLiving.nonEmpty,
      s"There are unconfirmed transactions in the pool for more than 65 mins: $llJsons")
  }
}

object NoNewBlock extends CriticalEvent {
  override def check() = {
    val lb = Nxt.getBlockchain.getLastBlock
    val ts = Convert.fromEpochTime(lb.getTimestamp)
    lazy val height = lb.getHeight
    lazy val msg = s"No new block for more than 50 mins, " +
      s"current time is ${new DateTime().toString} " +
      s"last block time is ${new DateTime(ts).toString} " +
      s"last block height is $height"

    booleanToMessageOption(ts < new DateTime().minusMinutes(50).getMillis, msg)
  }
}


object BalanceLow extends CriticalEvent {
  override def check() = {
    val nxtBalance = toNxt(VerificationServerConfig.blockchainDbWorker.getBalanceNQT)
    val errMsg = s"Balance of ${Convert.rsAccount(VerificationServerConfig.blockchainDbWorkerId)} is too low: $nxtBalance"
    booleanToMessageOption(nxtBalance < 1500, errMsg)
  }
}


trait RecoveryEvent extends Event {
  override val basket = blockchainTypePrefix + "recovery"
}

object NoNewBlockRollbackRecovery extends CriticalEvent {
  override def check() = {
    val ts = Convert.fromEpochTime(Nxt.getBlockchain.getLastBlock.getTimestamp)
    val msg = booleanToMessageOption(ts < new DateTime().minusMinutes(65).getMillis,
      "Ver server is going to 5 blocks rollback as there's no new block for more than 65 mins")
    NxtFunctions.forgetLastBlocks(5, removeUnconfirmedTransactions = false)
    msg
  }
}


case class ExternalError(msg: String) extends Event {
  override val basket = blockchainTypePrefix + "external-errors"

  override def check() = Some(msg)
}

case class InternalError(msg: String) extends CriticalEvent {
  override def check() = Some(msg)
}


class MonitoringActor(implicit val system:ActorSystem) extends Actor with Logging {
  import smartcontract.actors.monitoring.Event._

  private def checkAndSend(evt:Event): Unit = {
    evt.check().map { msg =>
      val js = Json.obj("value" -> msg, "basket" -> evt.basket, "timestamp" -> new DateTime().getMillis)
      log.info("Error found, sending notification: "+msg)
      new HttpClient().post(monitoringPanelUrl, js.toString())
    }
  }

  def receive = {
    case InternalCriticalEventsChecks =>
      Array(OldUnconfirmedTransaction, NoNewBlock, BalanceLow).foreach(checkAndSend)

    case RecoveryChecks =>
      Array(NoNewBlockRollbackRecovery).foreach(checkAndSend)

    case e: ExternalError => checkAndSend(e)
  }
}


case object RecoveryChecks

case object InternalCriticalEventsChecks

case object MetricsCheck