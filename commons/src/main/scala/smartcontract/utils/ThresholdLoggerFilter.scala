/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.utils

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.filter.Filter
import ch.qos.logback.core.spi.FilterReply

/**
 * The code is rewritten from Java source
 * found on http://stackoverflow.com/questions/10734025/logback-two-appenders-multiple-loggers-different-levels
 */

class ThresholdLoggerFilter extends Filter[ILoggingEvent] {
  private var level: Level = _
  private var logger: String = _

  override def decide(event: ILoggingEvent): FilterReply =
    if (!isStarted) FilterReply.NEUTRAL
    else if (!event.getLoggerName.startsWith(logger)) FilterReply.DENY
    else if (event.getLevel.isGreaterOrEqual(level)) FilterReply.NEUTRAL
    else FilterReply.DENY

  def setLevel(level: Level): Unit = this.level = level

  def setLogger(logger: String): Unit = this.logger = logger

  override def start(): Unit =
    if (this.level != null && this.logger != null)
      super.start()
    else
      throw new IllegalStateException("Cant' start ThresholdLoggerFilter without level & logger params")
}