/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.utils

import java.util.Base64


object EncodingUtils {
  def hex2bytes(hex: String): Array[Byte] =
    hex.replaceAll("[^0-9A-Fa-f]", "").sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte)

  def bytes2hex(bytes: Array[Byte]): String = bytes2hex(bytes, None)

  def bytes2hex(bytes: Array[Byte], sep: Option[String]): String =
    bytes.map("%02x".format(_)).mkString(sep.getOrElse(""))

  def bytes2base64(bytes: Array[Byte]): String = Base64.getEncoder.encodeToString(bytes)

  def base64bytes(base64:String):Array[Byte] = Base64.getDecoder.decode(base64)

  def xorHexStrings(hexString1: String, hexString2: String) = {
    val iterator1 = hexString1.sliding(2, 2)
    val iterator2 = hexString2.sliding(2, 2)
    val result = new StringBuilder

    if (hexString2.length > hexString1.length) {
      while (iterator1.hasNext) {
        val i = Integer.toString(Integer.parseInt(iterator1.next(), 16) ^
          Integer.parseInt(iterator2.next(), 16), 16)

        if (i.length == 1) result.append("0")
        result.append(i)
      }
      while (iterator2.hasNext) result.append(iterator2.next())
    } else {
      while (iterator2.hasNext) {
        val i = Integer.toString(Integer.parseInt(iterator1.next(), 16) ^
          Integer.parseInt(iterator2.next(), 16), 16)

        if (i.length == 1) result.append("0")
        result.append(i)
      }
      while (iterator1.hasNext) result.append(iterator1.next())
    }

    result.toString()
  }
}