/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bootstrap

import java.io.File
import java.net.{URL, URLClassLoader}

import akka.actor.Props
import akka.io.IO
import nxt._
import org.joda.time.DateTimeZone
import smartcontract.actors.monitoring.{CheckNodes, InternalCriticalEventsChecks, RecoveryChecks}
import smartcontract.api.http.HttpServiceActor
import smartcontract.utils.Logging
import spray.can.Http

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

trait BootstrapFunctions extends GlobalActors with ServerSSLConfig with Logging {

  import VerificationServerConfig._

  val concreteBootstrap: () => Unit
  val specificRouting: spray.routing.Route

  def bootstrap(environment: VerificationServerEnvironment.Value): Unit = {
    loadConfig(environment)
      .ensuring(VerificationServerConfig.isTestnet == Constants.isTestnet)

    DateTimeZone.setDefault(DateTimeZone.UTC)


    LaunchingFunctions.launch()
    LaunchingFunctions.waitForDownloadingDone()
    log.info("Blockchain downloading is done")

    concreteBootstrap()

    //binding
    val hsProps = Props(classOf[HttpServiceActor], specificRouting)
    val httpServiceActor = system.actorOf(hsProps, "http-service")
    val bindAddress = VerificationServerConfig.bindAddress
    IO(Http) ! Http.Bind(httpServiceActor, bindAddress, httpApiPort)

    //scheduling
    system.scheduler.schedule(15.minutes, 25.minutes)(monitoring ! InternalCriticalEventsChecks)
    system.scheduler.schedule(15.minutes, 60.minutes)(monitoring ! RecoveryChecks)
    system.scheduler.schedule(30.minutes, 60.minutes)(nodeChecker ! CheckNodes)
  }

  def loadConfig(environment: VerificationServerEnvironment.Value): Unit = {
    VerificationServerEnvironment.setAndGet(environment)
    VerificationServerEnvironment.configFolder().foreach { configFolder =>
      val absFolder = this.getClass.getResource(configFolder).getFile

      val method = classOf[URLClassLoader].getDeclaredMethod("addURL", classOf[URL])
      method.setAccessible(true)
      method.invoke(ClassLoader.getSystemClassLoader, new File(absFolder).toURI.toURL)
      log.info(s"Config folder set: " + absFolder)
    }
  }

  def environmentFromArgs(args: Array[String]) = args.lastOption match {
    case Some(s: String) if s.equalsIgnoreCase("testnet") => VerificationServerEnvironment.Testnet
    case Some(s: String) if s.equalsIgnoreCase("mainnet") => VerificationServerEnvironment.Mainnet
    case _ =>
      if (System.getProperty("user.dir").toLowerCase.contains("testnet"))
        VerificationServerEnvironment.Testnet
      else
        VerificationServerEnvironment.Mainnet
  }
}