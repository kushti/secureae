/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bootstrap

import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit

import com.typesafe.config.ConfigFactory
import nxt.Account
import nxt.crypto.Crypto
import nxt.util.Convert
import spray.http.Uri

import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.util.Try


object VerificationServerConfig {
  lazy val conf = ConfigFactory.load().withFallback(defaultConfig)
  lazy val nodeId = getString("node-id")

  lazy val acksToStart = getInt("consensus.acks-to-start")

  lazy val blockchainDbWorker = Account.getAccount(Crypto.getPublicKey(blockchainDbWorkerPhrase))
  lazy val blockchainDbWorkerPhrase = getString("blockchain-worker-phrase")
  lazy val blockchainDbWorkerId = blockchainDbWorker.getId
  lazy val isTestnet = getBoolean("isTestnet")
  lazy val frontAppPath = getString("front-app")
  lazy val bindAddress = Try(conf.getString("bind-address")).getOrElse("127.0.0.1")
  lazy val httpApiPort = getInt("http-api-port")
  lazy val peerPort = getInt("peer-port")
  lazy val monitoringPostUrl = conf.getString("monitoring-post-url")
  lazy val nodes: Set[Uri] = conf.getStringList("nodes").toSet.map(Uri.apply)
  lazy val peers: Map[Array[Byte], InetSocketAddress] = {
    conf.getConfigList("peers").map { c =>
      val id = Convert.parseAccountId(c.getString("nxt-id"))
      val pk = Account.getAccount(id).getPublicKey
      val rawAddr = c.getString("ip")
      val raParts = rawAddr.split(":")
      val hostname = raParts(0)
      val port = raParts(1).toInt
      val addr = new InetSocketAddress(hostname, port)
      pk -> addr
    }.toMap
  }
  lazy val isChainComEnabled = conf.getBoolean("chain-dot-com")
  lazy val isToshiEnabled = conf.getBoolean("toshi")
  lazy val bitcoinConfirmations = getInt("bitcoin-confirmations")
  lazy val toshiNodes: Set[String] = conf.getStringList("toshi-nodes").toSet
  lazy val termTrackingTimeout = getDuration("term-tracking-timeout")
  lazy val minHeight = getInt("min-height")
  private lazy val defaultConfig = ConfigFactory.load("application-default")

  def getBoolean(property: String) = conf.getBoolean(property)

  def getInt(property: String) = conf.getInt(property)

  def getString(property: String) = conf.getString(property)

  def getDuration(property: String) = conf.getDuration(property, TimeUnit.SECONDS).seconds
}