/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bootstrap

import javax.net.ssl.SSLContext

import org.apache.camel.util.jsse.{KeyManagersParameters, KeyStoreParameters, SSLContextParameters}


// Camel-core is used, see https://groups.google.com/forum/#!msg/spray-user/Vtgtaf_duB8/zly5DbcW_lsJ
// for SSL support (if enabled in application.conf)
trait ServerSSLConfig {

  implicit def sslContext: SSLContext = {

    val keyStoreFile = "certs/server.keystore"

    val ksp = new KeyStoreParameters()
    ksp.setResource(keyStoreFile)
    ksp.setPassword("")

    val kmp = new KeyManagersParameters()
    kmp.setKeyStore(ksp)
    kmp.setKeyPassword("")

    val scp = new SSLContextParameters()
    scp.setKeyManagers(kmp)

    val context = scp.createSSLContext()

    context
  }
}
