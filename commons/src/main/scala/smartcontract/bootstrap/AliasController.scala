/*
 * Copyright (c) 2014-2016 Alexander Chepurnoy
 *
 * This file is part of former SecureAE/SmartContract backend.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package smartcontract.bootstrap

import nxt.utils.TransactionTemplates
import nxt.{Alias, NxtFunctions}
import smartcontract.utils.Logging

import scala.util.{Failure, Success, Try}


trait AliasController {
  def checkOwnAlias(): Try[Unit]
}


object NxtAliasController extends AliasController with Logging {

  override def checkOwnAlias(): Try[Unit] = Try {
    val nodeId = VerificationServerConfig.nodeId
    val secretPhrase = VerificationServerConfig.blockchainDbWorkerPhrase
    val accId = VerificationServerConfig.blockchainDbWorkerId

    //check alias is registered
    NxtFunctions.aliases(accId).map(_.getAliasName).contains(nodeId) match {
      case false =>
        //check if alias is registered by other party
        Option(Alias.getAlias(nodeId)) match {
          case Some(alias) => throw new IllegalStateException("Node id is already registered by other party")
          case None =>
            TransactionTemplates.registerAlias(secretPhrase, nodeId) match {
              case Success(_) => log.info("Alias registered: " + nodeId)
              case Failure(t) => throw new IllegalStateException("Can't register alias! ", t)
            }

        }
      case true =>
    }
  }
}