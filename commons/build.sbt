import sbt._

import Keys._

name := "commons"

version := "0.1"

resolvers ++= Seq("localrepo" at "file://" + file("repo").getAbsolutePath)

libraryDependencies ++=
  Dependencies.akka ++
  Dependencies.json ++
  Dependencies.spray ++
    Seq(
      "nxt" %% "nxtscala" % "0.2.9",
      "ch.qos.logback"  %  "logback-classic"   % "1.1.3",
      "com.typesafe" % "config" % "1.3.0",
      "org.apache.camel" % "camel-core" % "2.14.1"
    )
